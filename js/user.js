﻿$(document).ready(function(){
	$('.del').click(function(){
		alert('Del!');
		tr_user = $(this).parent().parent();
		$.post('/user/delete/', {id: tr_user.attr('id_usr')}, function(data) {
			alert('Пользователь номер ' + tr_user.attr('id_usr') + ' удален');
			//tr_user.hide();
			$.get('/user/getlist/', function(data) {
				$('#user_div').html(data);
			});
		});
	});
	
	$('.user_table tr td').live('click', function(){
		//alert('edit');
		tr_user = $(this).parent();
		$.getJSON('/user/get/', {id: tr_user.attr('id_usr')}, function(data) {
			//alert('Пользователь номер ' + tr_user.attr('id_usr') + ' удален');
			//tr_user.hide();
			//alert(data.user_id);
			$('select[name="user_type"]').val(data.user_type);
			$('#tr_usr_id td:last').html(data.user_id);
			$('input[name="user_login"]').val(data.user_login);
			$('input[name="user_fname"]').val(data.user_fname);
			$('input[name="user_name"]').val(data.user_name);
			$('input[name="user_lname"]').val(data.user_lname);
			$('input[name="user_number"]').val(data.user_number);
			$('select[name="user_group"]').val(data.user_group);
		});
	});
	
	$('input[name="user_add"]').click(function () {
		
		$('#tr_usr_id td:last').html('');
		$('input[name="user_login"]').val('');
		$('input[name="user_fname"]').val('');
		$('input[name="user_name"]').val('');
		$('input[name="user_lname"]').val('');
		$('input[name="user_passwd"]').val('');
		$('input[name="user_number"]').val('');
		//$('select[name="user_group"]').val(data.user_group);
		//	$('select[name="user_type"]').val(data.user_type);
	});
	
	$('input[name="user_edit"]').click(function () {
		
		id = $('#tr_usr_id td:last').html();
		user_login = $('input[name="user_login"]').val();
		if (!user_login)
		{
			alert('Введите логин');
			exit();
		}
		user_fname = $('input[name="user_fname"]').val();
		if (!user_fname)
		{
			alert('Введите фамилию');
			exit();
		}
		user_name = $('input[name="user_name"]').val();
		if (!user_name)
		{
			alert('Введите имя');
			exit();
		}
		user_lname = $('input[name="user_lname"]').val();
		if (!user_lname)
		{
			alert('Введите отчество');
			exit();
		}
		user_passwd = $('input[name="user_passwd"]').val();
		if (!id && !user_passwd)
		{
			alert('Введите пороль');
			exit();
		}
		user_number = $('input[name="user_number"]').val();
		user_group = $('select[name="user_group"]').val();
		user_type = $('select[name="user_type"]').val();
		
		$.getJSON('/user/add/', 
			{
				id: id, 
				user_login: user_login, 
				user_fname:user_fname, 
				user_name:user_name, 
				user_lname:user_lname, 
				user_passwd:user_passwd, 
				user_number:user_number, 
				user_group:user_group, 
				user_type:user_type 
			}, 
			function(data) {
			
				if (data.error.login == 1)
				{
					alert('Такой логин уже существует!');
					exit();
				}
				if (id)
					alert('Пользователь номер ' + id + ' изменен');
				else
					alert('Пользователь создан');
					
				//tr_user.hide();
				$.get('/user/getlist/', function(data) {
					$('#user_div').html(data);
				});
				//$('input[name="user_add"]').click();
		});
		
	});
	
	$('input[name="user_del"]').click(function () {
		tr_user = $('#tr_usr_id td:last').html();;
		if (!tr_user)
		{
			alert('Нельзя удалить несущствующего пользователя');
			exit();
		}
		alert('Del!');
		$.post('/user/delete/', {id: tr_user}, function(data) {
			alert('Пользователь номер ' + tr_user + ' удален');
			//tr_user.hide();
			$.get('/user/getlist/', function(data) {
				$('#user_div').html(data);
			});
			$('input[name="user_add"]').click();
		});
	});
	
	$.get('/user/getlist/', function(data) {
		$('#user_div').html(data);
	});
});

		