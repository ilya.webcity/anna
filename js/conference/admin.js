// модуль docs - администраторская часть
var redactor_object;

$(document).ready(function () {  

    var cr_cdate = $("#cr_cdate").val();    
    
    // Активируем датаПикеры 
    $.datepicker.setDefaults(
        $.extend($.datepicker.regional["ru"])
    );
    $("#cr_cdate").datepicker();
    $("#cr_cdate").datepicker("option", "dateFormat", "yy-mm-dd");
    // устанавливаем дату
    $("#cr_cdate").datepicker("setDate", cr_cdate);    
    
    //Редактирование соседних тем
    $('#chaptermenu').dcFloater({
                width: 150,
                location: 'top',
                align: 'right',
                offsetLocation: 10,
                offsetAlign: 20,
                speedFloat: 1500,
                speedContent: 600,
                tabText: 'Другие',
                autoClose: true,
                event: 'click'
    });   
});

function ShowSuccess(msg) {
    var console = $("#errmessage"); 
    
    console.removeClass("errmessage");
    console.addClass("okmessage");
    console.html(msg);
    
    console = $("#errmessage2"); 
    
    console.removeClass("errmessage");
    console.addClass("okmessage");
    console.html(msg);
}

function ShowError(msg) {
    var console = $("#errmessage"); 
    
    console.removeClass("okmessage");
    console.addClass("errmessage");
    console.html(msg);
    
    console = $("#errmessage2"); 
    
    console.removeClass("okmessage");
    console.addClass("errmessage");
    console.html(msg);
}

function clearConsoleMess()
{
    var hasOk = $('#errmessage').hasClass('okmessage');
    var hasErr = $('#errmessage').hasClass('errmessage');        
    $("#errmessage").removeClass('okmessage', hasOk);
    $("#errmessage").removeClass('errmessage', hasErr);
    $("#errmessage").html('');
}

function saveConf() {
    
    var cr_id = $('#cr_id').val();
    var cr_admin = $('#cr_admin').val();
    var cr_title = $('#cr_title').val();   
    var cr_open = $('#cr_open').val();    
    var cr_cdate = $('#cr_cdate').val();
    
           
    var errText = '';
    if (cr_title == '') {
        errText += 'ОШИБКА: Не указан заколовок!<br/>';
    }    
    
    if (errText != '') {
        ShowError(errText);
        return;
    }
    
    $.post("/conference/saveConf",
        {
            cr_id: cr_id,
            cr_cdate: cr_cdate,
            cr_title: cr_title,
            cr_admin: cr_admin,
            cr_open: cr_open                     
        },
        function (result)
        {              
            
            if (result >= 0)
            {
                ShowSuccess("Данные успешно сохранены!");    
            }   
            else
            {
                ShowError("В процессе сохранения возникли ошибки!");        
            } 
        }, "json")
}

function newAnn() {    
    
    var cr_admin = $('#cr_admin').val();
    var cr_title = $('#cr_title').val();   
    var cr_open = $('#cr_open').val();    
    var cr_cdate = $('#cr_cdate').val();   
    
           
    var errText = '';
    if (cr_title == '') {
        errText += 'ОШИБКА: Не указан заколовок!<br/>';
    }   
    
    if (errText != '') {
        ShowError(errText);
        return;
    }
    
    $.post("/conference/addConf",
        {            
            cr_cdate: cr_cdate,
            cr_title: cr_title,
            cr_admin: cr_admin,
            cr_open: cr_open 
        },
        function (result)
        {              
            
            if (result >= 0)
            {
                ShowSuccess("Конференция успешно создана!");    
            }   
            else
            {
                ShowError("В процессе создания возникли ошибки!");        
            } 
        }, "json")
}

function delConf() {

    if (confirm("Вы уверены, что хотите удалить эту конференцию?") == false) {
        return;
    }
    var cr_id = $('#cr_id').val(); 
    
    $.post("/conference/deleteConf",
        {
            cr_id: cr_id            
        },
        function (result)
        {              
            if (result >= 0)
            {
                location.href = '/conference/admin';
                ShowSuccess("Конференция успешно удалена!");                 
            }   
            else
            {
                ShowError("В процессе удаления возникли ошибки!");        
            } 
        }, "json")    
    
}