$(document).ready(function () {
    
    $("#pac_form").submit(Send); 
    $("#pac_text").focus(); 
    setInterval("Load();", 2000); });   


function Send() {
    $.post("/conference/send",  
    {           
        name: $("#pac_name").val(), 
        text: $("#pac_text").val() 
    },
    Load ); 

    $("#pac_text").val(""); // очистим поле ввода сообщения
    $("#pac_text").focus(); // и поставим на него фокус
    
    return false; 
}

var last_message_id = 0; 
var load_in_process = false; 


function Load() {
   if(!load_in_process)
   {
        load_in_process = true;         
        $.post("/conference/load", 
        {
            last: last_message_id 
        },
            function (result) { 
                    eval(result); 
                    $(".chat").scrollTop($(".chat").get(0).scrollHeight); 
                    load_in_process = false; 
        });
    }
}