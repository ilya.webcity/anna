$(document).ready(function () {
    
    $("#pac_form").submit(Send); 
    $("#pac_text").focus(); 
    setInterval("Load();", 4000); });   

function sayTo(name)
{
    var text = $("#pac_text").val();
    $("#pac_text").val(text + name + ", ");
    $("#pac_text").focus();
}

function Send() {
    
    $.post("/conference/send",  
    {           
        name: $("#pac_name").val(), 
        text: $("#pac_text").val() 
    },
    Load ); 

    $("#pac_text").val(""); // очистим поле ввода сообщения
    $("#pac_text").focus(); // и поставим на него фокус
    
    return false; 
}

var last_message_id = 0;
var load_in_process = false;
//var load_mess = false;
//var load_users = false; 

function Load() {   
    
   if(!load_in_process)
   {
        load_in_process = true;
        //load_mess = true;
        //load_users = true;
        
        // получаем новые сообщения         
        $.post("/conference/load", 
        {
            last: last_message_id 
        },
            function (result) { 
                    if (result != null)
                    {
                        last_message_id = result.last_id;
                        //alert('last id = ' + last_message_id);
                    
                        // заполняем сообщения
                        var mess = {};
                        for (i = 0; i < result.mess.length; i++)
                        {
                            mess = result.mess[i];
                            $(".chat").append("<span>[" + mess.cl_ts + "] "+ mess.cl_name +": "+ mess.cl_text +"</span>");
                            //alert(mess.cl_text);
                        }
                        $(".chat").scrollTop($(".chat").get(0).scrollHeight);
                        
                        // заполняем список пользователей
                        var user = {};
                        $(".userlist").html("");                        
                        for (i = 0; i < result.users.length; i++)
                        {
                            user = result.users[i];                            
                            $(".userlist").append("<span><a href=\"javascript:void(0)\" onClick=\"sayTo('" + user.cu_name + "');\">" + user.cu_name + "</a></span>");                            
                        }                     

                        $(".userlist").scrollTop($(".userlist").get(0).scrollHeight);
                         
                    }
                    load_in_process = false; 
        }, "json");
        
       /* // обновляем список активных юзеров
        $.post("/conference/getuserlist", 
        {
            //last_u_id: last_user_id    
        },
            function (result) {            
                    
                    if (result != null)
                    {  
                        $(".userlist").html("");                        
                        for (i = 0; i < result.length; i++)
                        {
                            user = result[i];                            
                            $(".userlist").append("<span><a href=\"javascript:void(0)\" onClick=\"sayTo('" + user.cu_name + "');\">" + user.cu_name + "</a></span>");                            
                        }                     

                        $(".userlist").scrollTop($(".userlist").get(0).scrollHeight); 
                    }
                    load_in_process = false; 
        }, "json");  */
        
       // if ((load_mess == false) && (load_users == false))
        //{
        //    load_in_process = false;
        //}
        
    }
}