// модуль test - администраторская часть

$(document).ready(function () {    
    
    
    //Редактирование соседних тем
    $('#chaptermenu').dcFloater({
                width: 150,
                location: 'top',
                align: 'right',
                offsetLocation: 10,
                offsetAlign: 20,
                speedFloat: 1500,
                speedContent: 600,
                tabText: 'Другие',
                autoClose: true,
                event: 'click'
    });
    
    // ПОДКЛЮЧАЕМ ВИзУАЛЬНЫЙ РЕДАКТОР  
	
	$('#test_about').tinymce({
			// Location of TinyMCE script
			script_url : '/js/tiny_mce/tiny_mce.js',
			
			// НОВОЕ
			file_browser_callback : "tinyBrowser",
			// НОВОЕ
			
			// General options
			width: "100%",
			height: "250px",
			theme : "advanced",
			plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",
			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			// Example content CSS (should be your site CSS)

			content_css : "/css/style.css",

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "/js/tiny_mce/plugins/lists/template_list.js",
			external_link_list_url : "/js/tiny_mce/plugins/lists/link_list.js",
			external_image_list_url : "/js/tiny_mce/plugins/lists/image_list.js",
			media_external_list_url : "/js/tiny_mce/plugins/lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
                
                
            $('#testask_text').tinymce({
			// Location of TinyMCE script
			script_url : '/js/tiny_mce/tiny_mce.js',
			
			// НОВОЕ
			file_browser_callback : "tinyBrowser",
			// НОВОЕ
			
			// General options
			width: "100%",
			height: "250px",
			theme : "advanced",
			plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",
			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			// Example content CSS (should be your site CSS)

			content_css : "/css/style.css",

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "/js/tiny_mce/plugins/lists/template_list.js",
			external_link_list_url : "/js/tiny_mce/plugins/lists/link_list.js",
			external_image_list_url : "/js/tiny_mce/plugins/lists/image_list.js",
			media_external_list_url : "/js/tiny_mce/plugins/lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
		
		
		$('#testans_text').tinymce({
			// Location of TinyMCE script
			script_url : '/js/tiny_mce/tiny_mce.js',
			
			// НОВОЕ
			file_browser_callback : "tinyBrowser",
			// НОВОЕ
			
			// General options
			width: "100%",
			height: "250px",
			theme : "advanced",
			plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",
			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			// Example content CSS (should be your site CSS)

			content_css : "/css/style.css",

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "/js/tiny_mce/plugins/lists/template_list.js",
			external_link_list_url : "/js/tiny_mce/plugins/lists/link_list.js",
			external_image_list_url : "/js/tiny_mce/plugins/lists/image_list.js",
			media_external_list_url : "/js/tiny_mce/plugins/lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
		
		$('#left_testans_text').tinymce({
			// Location of TinyMCE script
			script_url : '/js/tiny_mce/tiny_mce.js',
			
			// НОВОЕ
			file_browser_callback : "tinyBrowser",
			// НОВОЕ
			
			// General options
			width: "100%",
			height: "250px",
			theme : "advanced",
			plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",
			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			// Example content CSS (should be your site CSS)

			content_css : "/css/style.css",

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "/js/tiny_mce/plugins/lists/template_list.js",
			external_link_list_url : "/js/tiny_mce/plugins/lists/link_list.js",
			external_image_list_url : "/js/tiny_mce/plugins/lists/image_list.js",
			media_external_list_url : "/js/tiny_mce/plugins/lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
		
		$('#right_testans_text').tinymce({
			// Location of TinyMCE script
			script_url : '/js/tiny_mce/tiny_mce.js',
			
			// НОВОЕ
			file_browser_callback : "tinyBrowser",
			// НОВОЕ
			
			// General options
			width: "100%",
			height: "250px",
			theme : "advanced",
			plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",
			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			// Example content CSS (should be your site CSS)

			content_css : "/css/style.css",

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "/js/tiny_mce/plugins/lists/template_list.js",
			external_link_list_url : "/js/tiny_mce/plugins/lists/link_list.js",
			external_image_list_url : "/js/tiny_mce/plugins/lists/image_list.js",
			media_external_list_url : "/js/tiny_mce/plugins/lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
                
                
   LoadAsks();             

});

function ShowSuccess(msg) {
    var console = $("#errmessage"); 
    
    console.removeClass("errmessage");
    console.addClass("okmessage");
    console.html(msg);
    
    console = $("#errmessage2"); 
    
    console.removeClass("errmessage");
    console.addClass("okmessage");
    console.html(msg);
}

function ShowError(msg) {
    var console = $("#errmessage"); 
    
    console.removeClass("okmessage");
    console.addClass("errmessage");
    console.html(msg);
    
    console = $("#errmessage2"); 
    
    console.removeClass("okmessage");
    console.addClass("errmessage");
    console.html(msg);
}

function clearConsoleMess()
{
    var hasOk = $('#errmessage').hasClass('okmessage');
    var hasErr = $('#errmessage').hasClass('errmessage');        
    $("#errmessage").removeClass('okmessage', hasOk);
    $("#errmessage").removeClass('errmessage', hasErr);
    $("#errmessage").html('');
}

// Тесты

function saveTest() {
    
    var test_id = $('#test_id').val();
    var test_title = $('#test_title').val();
    var test_about = $('#test_about').val();   
    var test_ctrue = $('#test_ctrue').val();
    var test_cask = $('#test_cask').val();   
    
    
    var errText = '';
    if (test_title == '') {
        errText += 'ОШИБКА: Не указан заколовок!<br/>';
    }
    if (test_about == '') {
        errText += 'ОШИБКА: Не указано описание!<br/>';
    }
    if (test_ctrue == '') {
        errText += 'ОШИБКА: Не указано количество ответов, по достижении которого тест считается успешно пройденным!<br/>';
    }
    if (test_cask == '') {
        errText += 'ОШИБКА: Не указано количество ответов!<br/>';
    }
    
    if (errText != '') {
        ShowError(errText);
        return;
    }
    
    $.post("/test/saveTest",
        {
            test_id: test_id,
            test_title: test_title,
            test_about: test_about,
            test_ctrue: test_ctrue,
            test_cask: test_cask           
        },
        function (result)
        {              
            
            if (result >= 0)
            {
                ShowSuccess("Данные успешно сохранены!");    
            }   
            else
            {
                ShowError("В процессе сохранения возникли ошибки!");        
            } 
        }, "json")
}

function newTest() {    

    var test_title = $('#test_title').val();
    var test_about = $('#test_about').val();   
    var test_ctrue = $('#test_ctrue').val();
    var test_cask = $('#test_cask').val();    
    
           
    var errText = '';
    if (test_title == '') {
        errText += 'ОШИБКА: Не указан заколовок!<br/>';
    }
    if (test_about == '') {
        errText += 'ОШИБКА: Не указано описание!<br/>';
    }
    if (test_ctrue == '') {
        errText += 'ОШИБКА: Не указано количество ответов, по достижении которого тест считается успешно пройденным!<br/>';
    }
    if (test_cask == '') {
        errText += 'ОШИБКА: Не указано количество ответов!<br/>';
    }
    
    if (errText != '') {
        ShowError(errText);
        return;
    }
    
    $.post("/test/saveNewTest",
        {            
            test_title: test_title,
            test_about: test_about,
            test_ctrue: test_ctrue,
            test_cask: test_cask
        },
        function (result)
        {              
            
            if (result >= 0)
            {
                ShowSuccess("Тест успешно создан!");    
            }   
            else
            {
                ShowError("В процессе создания возникли ошибки!");        
            } 
        }, "json")
}

function delTest() {

    if (confirm("Вы уверены, что хотите удалить этот тест и все входящие в него вопросы?") == false) {
        return;
    }
    var test_id = $('#test_id').val(); 
    
    $.post("/test/deleteTest",
        {
            test_id: test_id            
        },
        function (result)
        {              
            if (result >= 0)
            {
                location.href = '/test/admin';
                ShowSuccess("Тест успешно удален!");                 
            }   
            else
            {
                ShowError("В процессе удаления возникли ошибки!");        
            } 
        }, "json")    
    
}

// Вопросы

function saveAsk() {
    
    var testask_id = $('#testask_id').val();
    var testask_type = $('#testask_type').val();
    var testask_test = $('#testask_test').val();   
    var testask_text = $('#testask_text').val();    
    
    var errText = '';
    if (testask_text == '') {
        errText += 'ОШИБКА: Не указан текст вопроса!<br/>';
    }    
    
    if (errText != '') {
        ShowError(errText);
        return;
    }
    
    $.post("/test/saveAsk",
        {
            testask_id: testask_id,
            testask_type: testask_type,
            testask_test: testask_test,
            testask_text: testask_text     
        },
        function (result)
        {              
            
            if (result >= 0)
            {
                ShowSuccess("Данные успешно сохранены!");    
            }   
            else
            {
                ShowError("В процессе сохранения возникли ошибки!");        
            } 
        }, "json")
}

function newAsk() {
    
    var testask_type = $('#testask_type').val();
    var testask_test = $('#testask_test').val();   
    var testask_text = $('#testask_text').val();     
   
    
    var errText = '';
    if (testask_text == '') {
        errText += 'ОШИБКА: Не указан текст вопроса!<br/>';
    }    
    
    if (errText != '') {
        ShowError(errText);
        return;
    }
    
    $.post("/test/newAsk",
        {            
            testask_type: testask_type,
            testask_test: testask_test,
            testask_text: testask_text     
        },
        function (result)
        {              
            
            if (result >= 0)
            {
                ShowSuccess("Вопрос успешно добавлен!");
                history.back();
            }   
            else
            {
                ShowError("В процессе добавления возникли ошибки!");        
            } 
        }, "json")

}

function delAsk() {

    if (confirm("Вы уверены, что хотите удалить этот вопрос?") == false) {
        return;
    }
    var testask_id = $('#testask_id').val(); 
    
    $.post("/test/delAsk",
        {
            testask_id: testask_id            
        },
        function (result)
        {              
            if (result >= 0)
            {
                history.back();
                ShowSuccess("Вопрос успешно удален!");                 
            }   
            else
            {
                ShowError("В процессе удаления возникли ошибки!");        
            } 
        }, "json")    
    
}



// Простые ответы

function saveSimpleAns() {

    var testans_id = $('#testans_id').val();
    var testans_text = $('#testans_text').val();
    var testans_right = $('#testans_right').val(); 
    
    
    var errText = '';
    if (testans_text == '') {
        errText += 'ОШИБКА: Не указан текст ответа!<br/>';
    }
    
    if (errText != '') {
        ShowError(errText);
        return;
    }
    
    $.post("/test/saveSimpleAns",
        {
            testans_id: testans_id,
            testans_text: testans_text,
            testans_right: testans_right                      
        },
        function (result)
        {              
            
            if (result >= 0)
            {
                history.back();
                ShowSuccess("Данные успешно сохранены!");    
            }   
            else
            {
                ShowError("В процессе сохранения возникли ошибки!");        
            } 
        }, "json");
    
    
}

function newSimpleAns() {
   
    var testans_ask = $('#testans_ask').val();
    var testans_text = $('#testans_text').val();
    var testans_right = $('#testans_right').val();   
    
    var errText = '';
    if (testans_text == '') {
        errText += 'ОШИБКА: Не указан текст ответа!<br/>';
    }
    
    if (errText != '') {
        ShowError(errText);
        return;
    }
    
    $.post("/test/newSimpleAns",
        {
            testans_ask: testans_ask,
            testans_text: testans_text,
            testans_right: testans_right                      
        },
        function (result)
        {              
            
            if (result >= 0)
            {
                history.back();
                ShowSuccess("Ответ успешно добавлен!");                
            }   
            else
            {
                ShowError("В процессе добавления возникли ошибки!");        
            } 
        }, "json");
    
    
}

function delSimpleAns() {

    if (confirm("Вы уверены, что хотите удалить этот ответ?") == false) {
        return;
    }
    var testans_id = $('#testans_id').val(); 
    
    $.post("/test/delSimpleAns",
        {
            testans_id: testans_id            
        },
        function (result)
        {              
            if (result >= 0)
            {
                history.back();
                ShowSuccess("Ответ успешно удален!");                 
            }   
            else
            {
                ShowError("В процессе удаления возникли ошибки!");        
            } 
        }, "json")    
    
}

// Ответы - пары

function savePairAns() {

    var left_testans_id = $('#left_testans_id').val();
    var left_testans_text = $('#left_testans_text').val();
    
    var right_testans_id = $('#right_testans_id').val();
    var right_testans_text = $('#right_testans_text').val(); 
    
    alert(left_testans_id);
    alert(left_testans_text);
    alert(right_testans_id);
    alert(right_testans_text);
    
    var errText = '';
    if (left_testans_text == '') {
        errText += 'ОШИБКА: Не указан текст ЛЕВОЙ части!<br/>';
    }
    
    if (right_testans_text == '') {
        errText += 'ОШИБКА: Не указан текст ЛЕВОЙ части!<br/>';
    }
    
    if (errText != '') {
        ShowError(errText);
        return;
    }
    
    $.post("/test/savePairAns",
        {
            left_testans_id: left_testans_id,
            left_testans_text: left_testans_text,
            right_testans_id: right_testans_id,
            right_testans_text: right_testans_text                      
        },
        function (result)
        {              
            
            if (result >= 0)
            {
                history.back();
                ShowSuccess("Данные успешно сохранены!");    
            }   
            else
            {
                ShowError("В процессе сохранения возникли ошибки!");        
            } 
        }, "json");
    
    
}


function newPairAns() {   
    
    var testans_ask = $('#testans_ask').val();
    var left_testans_text = $('#left_testans_text').val();
    var right_testans_text = $('#right_testans_text').val();

      
    
    var errText = '';
    if (left_testans_text == '') {
        errText += 'ОШИБКА: Не указан текст ЛЕВОЙ части!<br/>';
    }
    
    if (right_testans_text == '') {
        errText += 'ОШИБКА: Не указан текст ЛЕВОЙ части!<br/>';
    }
    
    if (errText != '') {
        ShowError(errText);
        return;
    }
    
    $.post("/test/newPairAns",
        {            
            testans_ask: testans_ask,
            left_testans_text: left_testans_text,
            right_testans_text: right_testans_text                      
        },
        function (result)
        {              
            
            if (result >= 0)
            {
                history.back();
                ShowSuccess("Пара успешно добавлена!");    
            }   
            else
            {
                ShowError("В процессе сохранения возникли ошибки!");        
            } 
        }, "json");
    
    
}

function delPairAns() {   
    
    if (confirm("Вы уверены, что хотите удалить этот ответ?") == false) {
        return;
    }
    
    var left_testans_id = $('#left_testans_id').val();    
    var right_testans_id = $('#right_testans_id').val();     
    
    
    $.post("/test/delPairAns",
        {            
            right_testans_id: right_testans_id,
            left_testans_id: left_testans_id                      
        },
        function (result)
        {              
            
            if (result >= 0)
            {
                history.back();
                ShowSuccess("Пара успешно удалена!");    
            }   
            else
            {
                ShowError("В процессе удаления возникли ошибки!");        
            } 
        }, "json");
    
    
}



