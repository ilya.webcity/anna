﻿$(document).ready(function(){
	$('.del').live('click',function(){
		alert('Del!');
		tr_user = $(this).parent().parent();
		$.post('/menu/delete/', {id: tr_user.attr('id_itm')}, function(data) {
			alert('Пункт меню номер ' + tr_user.attr('id_itm') + ' удален');
			//tr_user.hide();
			$.get('/menu/getlist/', function(data) {
				$('#items_div').html(data);
			});
			$.get('/menu/getselect/', function(data) {
				$('select[name="menu_pid"]').parent().html(data);
			});
			$('input[name="user_add"]').click();
		});
	});
	
	$('.item_table tr td').live('click', function(){
		//alert('edit');
		tr_user = $(this).parent();
		$.getJSON('/menu/get/', {id: tr_user.attr('id_itm')}, function(data) {
			//alert('Пользователь номер ' + tr_user.attr('id_usr') + ' удален');
			//tr_user.hide();;
			//alert(data.group_id);
			$('#tr_id td:last').html(data.menu_id);
			$('input[name="menu_title"]').val(data.menu_title);
			$('input[name="menu_url"]').val(data.menu_url);
			$('input[name="menu_number"]').val(data.menu_number);
			$('select[name="menu_pid"]').val(data.menu_pid);
			$('select[name="menu_type"]').val(data.menu_type);
			//$('select[name="color"]').val(data.color);
			$('img[id="'+data.color+'"]').click();
		});
	});
	
	$('input[name="user_add"]').click(function () {
		
		$('#tr_id td:last').html('');
		$('input[name="menu_title"]').val('');
		$('input[name="menu_url"]').val('');
		$('input[name="menu_number"]').val('');
		$('select[name="menu_pid"]').val('');
		$('select[name="menu_type"]').val('');
		$('select[name="color"]').val('');
	});
	
	$('input[name="user_edit"]').click(function () {
		
		id = $('#tr_id td:last').html();
		if (id == null)
			id = 0;
			//alert(id);
		menu_title = $('input[name="menu_title"]').val();
		if (!menu_title)
		{
			alert('Введите название');
			exit();
		}
		menu_number = $('input[name="menu_number"]').val();
		if (!menu_number)
		{
			alert('Введите номер');
			exit();
		}
		menu_url = $('input[name="menu_url"]').val();
		if (!menu_url)
		{
			alert('Введите URL');
			exit();
		}
		menu_pid = $('select[name="menu_pid"]').val();
		menu_type = $('select[name="menu_type"]').val();
		//color = $('select[name="color"]').val();
		color = $('#color_td img.active').attr('id');
		$.get('/menu/add/', 
			{
				menu_id: id, 
				menu_pid: menu_pid, 
				menu_number: menu_number, 
				color: color, 
				menu_title: menu_title, 
				menu_url: menu_url, 
				menu_type:menu_type 
			}, 
			function(data) {
				if (id)
					alert('Пункт меню ' + menu_title + ' изменен');
				else
					alert('Пункт меню создан');
				//alert(data);
				//tr_user.hide();
				$.get('/menu/getlist/', function(data) {
					$('#items_div').html(data);
				});
				$.get('/menu/getselect/', function(data) {
					$('select[name="menu_pid"]').parent().html(data);
				});
				$('input[name="user_add"]').click();
		});
		
	});
	
	$('input[name="user_del"]').click(function () {
		tr_item = $('#tr_id td:last').html();;
		if (!tr_item)
		{
			alert('Нельзя удалить');
			exit();
		}
		alert('Del!');
		$.post('/menu/delete/', {id: tr_item}, function(data) {
			alert('удален');
			//tr_user.hide();
				$.get('/menu/getlist/', function(data) {
					$('#items_div').html(data);
				});
				$.get('/menu/getselect/', function(data) {
					$('select[name="menu_pid"]').parent().html(data);
				});
			$('input[name="user_add"]').click();
		});
	});
	
	$.get('/menu/getlist/', function(data) {
		$('#items_div').html(data);
	});
	$('#color_td img').click(function() {
		$('#color_td img').removeClass('active');
		$(this).addClass('active');
	});
});

		