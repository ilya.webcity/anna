// модуль docs - администраторская часть
var redactor_object;

$(document).ready(function () {  

    var ann_cdate = $("#ann_cdate").val();
    var ann_edate = $("#ann_edate").val();
    
    // Активируем датаПикеры 
    $.datepicker.setDefaults(
        $.extend($.datepicker.regional["ru"])
    );
    $("#ann_cdate").datepicker();
    $("#ann_cdate").datepicker("option", "dateFormat", "yy-mm-dd");
    // устанавливаем дату
    $("#ann_cdate").datepicker("setDate", ann_cdate);

    // Дата окончания
    $("#ann_edate").datepicker();
    $("#ann_edate").datepicker("option", "dateFormat", "yy-mm-dd");
    // устанавливаем дату
    $("#ann_edate").datepicker("setDate", ann_edate);
    
    //Редактирование соседних тем
    $('#chaptermenu').dcFloater({
                width: 150,
                location: 'top',
                align: 'right',
                offsetLocation: 10,
                offsetAlign: 20,
                speedFloat: 1500,
                speedContent: 600,
                tabText: 'Другие',
                autoClose: true,
                event: 'click'
    });
    
    // ПОДКЛЮЧАЕМ ВИзУАЛЬНЫЙ РЕДАКТОР  
	
	$('#ann_text').tinymce({
			// Location of TinyMCE script
			script_url : '/js/tiny_mce/tiny_mce.js',
			
			// НОВОЕ
			file_browser_callback : "tinyBrowser",
			// НОВОЕ
			
			// General options
			width: "100%",
			height: "500px",
			theme : "advanced",
			plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",
			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			// Example content CSS (should be your site CSS)

			content_css : "/css/style.css",

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "/js/tiny_mce/plugins/lists/template_list.js",
			external_link_list_url : "/js/tiny_mce/plugins/lists/link_list.js",
			external_image_list_url : "/js/tiny_mce/plugins/lists/image_list.js",
			media_external_list_url : "/js/tiny_mce/plugins/lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});

    
});

function ShowSuccess(msg) {
    var console = $("#errmessage"); 
    
    console.removeClass("errmessage");
    console.addClass("okmessage");
    console.html(msg);
    
    console = $("#errmessage2"); 
    
    console.removeClass("errmessage");
    console.addClass("okmessage");
    console.html(msg);
}

function ShowError(msg) {
    var console = $("#errmessage"); 
    
    console.removeClass("okmessage");
    console.addClass("errmessage");
    console.html(msg);
    
    console = $("#errmessage2"); 
    
    console.removeClass("okmessage");
    console.addClass("errmessage");
    console.html(msg);
}

function clearConsoleMess()
{
    var hasOk = $('#errmessage').hasClass('okmessage');
    var hasErr = $('#errmessage').hasClass('errmessage');        
    $("#errmessage").removeClass('okmessage', hasOk);
    $("#errmessage").removeClass('errmessage', hasErr);
    $("#errmessage").html('');
}

function saveAnn() {
    
    var ann_id = $('#ann_id').val();
    var ann_type = $('#ann_type').val();
    var ann_title = $('#ann_title').val();
    //var ann_text = redactor_object.getCodeEditor();
	var ann_text = $('#ann_text').val();
    var ann_user = $('#ann_user').val();    
    var ann_cdate = $('#ann_cdate').val();
    var ann_edate = $('#ann_edate').val();    
    
           
    var errText = '';
    if (ann_title == '') {
        errText += 'ОШИБКА: Не указан заколовок!<br/>';
    }
    if (ann_cdate == '') {
        errText += 'ОШИБКА: Не указана дата создания!<br/>';
    }
    if (ann_edate == '') {
        errText += 'ОШИБКА: Не указана дата окончания!<br/>';
    }
    if (ann_text == '') {
        errText += 'ОШИБКА: Не указан текст объявления!<br/>';
    }
    
    if (errText != '') {
        ShowError(errText);
        return;
    }
    
    $.post("/ann/saveAnn",
        {
            ann_id: ann_id,
            ann_cdate: ann_cdate,
            ann_title: ann_title,
            ann_text: ann_text,
            ann_type: ann_type,
            ann_edate: ann_edate,
            ann_user: ann_user            
        },
        function (result)
        {              
            
            if (result >= 0)
            {
                ShowSuccess("Данные успешно сохранены!");    
            }   
            else
            {
                ShowError("В процессе сохранения возникли ошибки!");        
            } 
        }, "json")
}

function newAnn() {
    
    var ann_type = $('#ann_type').val();
    var ann_title = $('#ann_title').val();
    //var ann_text = redactor_object.getCodeEditor();
	var ann_text = $('#ann_text').val();
    var ann_user = $('#ann_user').val();    
    var ann_cdate = $('#ann_cdate').val();
    var ann_edate = $('#ann_edate').val();   
    
           
    var errText = '';
    if (ann_title == '') {
        errText += 'ОШИБКА: Не указан заколовок!<br/>';
    }
    if (ann_cdate == '') {
        errText += 'ОШИБКА: Не указана дата создания!<br/>';
    }
    if (ann_edate == '') {
        errText += 'ОШИБКА: Не указана дата окончания!<br/>';
    }
    if (ann_text == '') {
        errText += 'ОШИБКА: Не указан текст объявления!<br/>';
    }
    
    if (errText != '') {
        ShowError(errText);
        return;
    }
    
    $.post("/ann/addAnn",
        {            
            ann_cdate: ann_cdate,
            ann_title: ann_title,
            ann_text: ann_text,
            ann_type: ann_type,
            ann_edate: ann_edate,
            ann_user: ann_user
        },
        function (result)
        {              
            
            if (result >= 0)
            {
                ShowSuccess("Объявление успешно создано!");    
            }   
            else
            {
                ShowError("В процессе создания возникли ошибки!");        
            } 
        }, "json")
}

function delAnn() {

    if (confirm("Вы уверены, что хотите удалить это объявление?") == false) {
        return;
    }
    var ann_id = $('#ann_id').val(); 
    
    $.post("/ann/deleteAnn",
        {
            ann_id: ann_id            
        },
        function (result)
        {              
            if (result >= 0)
            {
                location.href = '/ann/admin';
                ShowSuccess("Объявление успешно удалено!");                 
            }   
            else
            {
                ShowError("В процессе удаления возникли ошибки!");        
            } 
        }, "json")    
    
}