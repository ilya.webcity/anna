// модуль accessmanager - администраторская часть
$(document).ready(function () {  
    $("#table_mess tr td").live("click", OpenRight); 
});

function ShowSuccess(msg) {
    var console = $("#errmessage"); 
    
    console.removeClass("errmessage");
    console.addClass("okmessage");
    console.html(msg);
}

function ShowError(msg) {
    var console = $("#errmessage"); 
    
    console.removeClass("okmessage");
    console.addClass("errmessage");
    console.html(msg);
}

function clearConsoleMess()
{
    var hasOk = $('#errmessage').hasClass('okmessage');
    var hasErr = $('#errmessage').hasClass('errmessage');        
    $("#errmessage").removeClass('okmessage', hasOk);
    $("#errmessage").removeClass('errmessage', hasErr);
    $("#errmessage").html('');
}

function LoaderProcess() {  
    $(".accessConsole").empty();
    $(".accessConsole").append("<img src=\"/img/ajax_loader.gif\" class=\"loaderimg\"></img>");
}

// ===========================================
// ФУНКЦИИ ОБРАБОТКИ ПРАВ ДОСТУПА К ДОКУМЕНТАМ
// ===========================================

// Отобразить данные по конкретному ДОКУ
function DocPanel(DocData, rId, mtype)
{        
    if (DocData != null)
    {               
        // формируем поле ошибок          
        var out = '<hr><div class="wrapper">';       
        out += '<form id="form" class="blocks">';
        out += '<input type="hidden" id="docId" value="' + DocData.doc_id + '"/>';
        out += '<p><label>Документ:</label><textarea class="textarea" disabled>'+ DocData.doc_title +'</textarea></p>';   
        
        // доступен для
        out = out + '<p><label><b>Доступен для: </b></label>';
        out += '<select name="accessok" id="accessok" class="select" style="width: 300px;">';
        for (i = 0; i < DocData.accessok.length; i++)
        {
            var line = DocData.accessok[i];
            if (i == 0)
            {
                var select = " selected";
            }
            else
            {
                var select = "";
            }                      
            out += '<option value="' + line.spec_id + '" class="line"' + select + '>' + line.spec_name + '</option>';
        }
        out += '</select>&nbsp;&nbsp;&nbsp;<input type="button" value="Удалить" onClick="DeleteSpec(' + rId + ', \'' + mtype + '\');"/></p>';
        // 
        
        // НЕ доступен для
        out = out + '<p><label><b>Недоступен для: </b></label>';
        out += '<select name="accesserr" id="accesserr" class="select" style="width: 300px;">';
        for (i = 0; i < DocData.accesserr.length; i++)
        {
            var line = DocData.accesserr[i];
            if (i == 0)
            {
                var select = " selected";
            }
            else
            {
                var select = "";
            }                      
            out += '<option value="' + line.spec_id + '" class="line"' + select + '>' + line.spec_name + '</option>';
        }
        out += '</select>&nbsp;&nbsp;&nbsp;<input type="button" value="Добавить" onClick="AddSpec(' + rId + ', \'' + mtype + '\');"/></p></form>';                                                  
        out = out + "</div>"; 
              
        $(".accessConsole").html(out);                
    } 
    else
    {
        $(".accessConsole").html('<div calss="error">Упс, что-то пошло не так :(</div>');    
    }  
}

// Отобразить данные по конкретной спецухе
function SpecPanel(SpecData, rId, mtype)
{        
   
    if (SpecData != null)
    {               
        // формируем поле ошибок          
        var out = '<hr><div class="wrapper">';       
        out += '<form id="form" class="blocks">';
        out += '<input type="hidden" id="specId" value="' + SpecData.spec_id + '"/>';
        out += '<p><label>Специальность:</label><input type="text" class="text" value="'+ SpecData.spec_name +'" disabled/>';   
        
        // доступен для
        out = out + '<p><label><b>Имеет доступ к: </b></label>';
        out += '<select name="accessok" id="accessok" class="select" style="width: 500px;">';
        for (i = 0; i < SpecData.accessok.length; i++)
        {
            var line = SpecData.accessok[i];
            if (i == 0)
            {
                var select = " selected";
            }
            else
            {
                var select = "";
            }                      
            out += '<option value="' + line.doc_id + '" class="line"' + select + '>' + line.doc_title + '</option>';
        }
        out += '</select>&nbsp;&nbsp;&nbsp;<input type="button" value="Удалить" onClick="DeleteDoc(' + rId + ', \'' + mtype + '\');"/></p>';
        // 
        
        // НЕ доступен для
        out = out + '<p><label><b>Нет доступа к:</b></label>';
        out += '<select name="accesserr" id="accesserr" class="select" style="width: 500px;">';
        for (i = 0; i < SpecData.accesserr.length; i++)
        {
            var line = SpecData.accesserr[i];
            if (i == 0)
            {
                var select = " selected";
            }
            else
            {
                var select = "";
            }                      
            out += '<option value="' + line.doc_id + '" class="line"' + select + '>' + line.doc_title + '</option>';
        }
        out += '</select>&nbsp;&nbsp;&nbsp;<input type="button" value="Добавить" onClick="AddDoc(' + rId + ', \'' + mtype + '\');"/></p></form>';                                                  
        out = out + "</div>"; 
              
        $(".accessConsole").html(out);                
    } 
    else
    {
        $(".accessConsole").html('<div calss="error">Упс, что-то пошло не так :(</div>');    
    }  
}

function DeleteDoc(rId, mtype)
{   
    var spec_id = $("#specId").val();
    var doc_id = $("#accessok").val();
    
    if (doc_id == null)
    {
        ShowError("Не указан документ для удаления!");
        return;
    }
    
    $.post("/accessmanager/deleteSpec", { doc_id: doc_id, spec_id: spec_id }, function (result) {
        
        if (result) 
        {
            ShowSuccess("Данные успешно сохранены!");
            OpenRight(-1, rId, mtype);       
        }
        else
        {
            ShowError("Не удалось удалить доступ для специальности :(!");
        }
        
    }, "json");
    
}

function AddDoc(rId, mtype)
{
    var spec_id = $("#specId").val();
    var doc_id = $("#accesserr").val();
    
    if (doc_id == null)
    {
        ShowError("Не указан документ для добавления!");
        return;
    }
    
    $.post("/accessmanager/addSpec", { doc_id: doc_id, spec_id: spec_id }, function (result) {
        
        if (result) 
        {
            ShowSuccess("Данные успешно сохранены!");
            OpenRight(-1, rId, mtype);       
        }
        else
        {
            ShowError("Не удалось добавить доступ для специальности :(!");
        }
        
    }, "json");
}

function DeleteSpec(rId, mtype)
{   
    var doc_id = $("#docId").val();
    var spec_id = $("#accessok").val();    
    
    if (spec_id == null)
    {
        ShowError("Не указана специальность для удаления!");
        return;
    }
    
    $.post("/accessmanager/deleteSpec", { doc_id: doc_id, spec_id: spec_id }, function (result) {
        
        if (result) 
        {  
            ShowSuccess("Данные успешно сохранены!");
            OpenRight(-1, rId, mtype);       
        }
        else
        {
            ShowError("Не удалось удалить доступ для специальности :(!");
        }
        
    }, "json");
}

function AddSpec(rId, mtype)
{
    var doc_id = $("#docId").val();
    var spec_id = $("#accesserr").val();
    
    if (spec_id == null)
    {
        ShowError("Не указана специальность для добавления!");
        return;
    }
    
    $.post("/accessmanager/addSpec", { doc_id: doc_id, spec_id: spec_id }, function (result) {
        
        if (result) 
        {
            ShowSuccess("Данные успешно сохранены!");
            OpenRight(-1, rId, mtype);       
        }
        else
        {
            ShowError("Не удалось добавить доступ для специальности :(!");
        }
        
    }, "json");
}


// =============================================
// ФУНКЦИИ ОБРАБОТКИ ПРАВ ДОСТУПА К КОНФЕРЕНЦИЯМ
// =============================================

// Отобразить данные по конкретной группе
function GroupPanel(Data, rId, mtype)
{        
    if (Data != null)
    {               
        // формируем поле ошибок          
        var out = '<hr><div class="wrapper">';       
        out += '<form id="form" class="blocks">';
        out += '<input type="hidden" id="groupId" value="' + Data.group_id + '"/>';
        out += '<p><label>Группа:</label><input type="text" class="text" value="' + Data.group_title + '" disabled/>';   
        
        // доступен для
        out = out + '<p><label><b>Имеет доступ к: </b></label>';
        out += '<select name="accessok" id="accessok" class="select" style="width: 300px;">';
        for (i = 0; i < Data.accessok.length; i++)
        {
            var line = Data.accessok[i];
            if (i == 0)
            {
                var select = " selected";
            }
            else
            {
                var select = "";
            }                      
            out += '<option value="' + line.conf_id + '" class="line"' + select + '>' + line.conf_name + '</option>';
        }
        out += '</select>&nbsp;&nbsp;&nbsp;<input type="button" value="Удалить" onClick="DeleteConf(' + rId + ', \'' + mtype + '\');"/></p>';
        // 
        
        // НЕ доступен для
        out = out + '<p><label><b>Нет доступа к: </b></label>';
        out += '<select name="accesserr" id="accesserr" class="select" style="width: 300px;">';
        for (i = 0; i < Data.accesserr.length; i++)
        {
            var line = Data.accesserr[i];
            if (i == 0)
            {
                var select = " selected";
            }
            else
            {
                var select = "";
            }                      
            out += '<option value="' + line.conf_id + '" class="line"' + select + '>' + line.conf_name + '</option>';
        }
        out += '</select>&nbsp;&nbsp;&nbsp;<input type="button" value="Добавить" onClick="AddConf(' + rId + ', \'' + mtype + '\');"/></p></form>';                                                  
        out = out + "</div>"; 
              
        $(".accessConsole").html(out);                
    } 
    else
    {
        $(".accessConsole").html('<div calss="error">Упс, что-то пошло не так :(</div>');    
    }  
}
// Отобразить данные по конкретной спецухе
function ConfPanel(Data, rId, mtype)
{    
   
    if (Data != null)
    {               
        // формируем поле ошибок          
        var out = '<hr><div class="wrapper">';       
        out += '<form id="form" class="blocks">';
        out += '<input type="hidden" id="confId" value="' + Data.conf_id + '"/>';
        out += '<p><label>Конференция:</label><input type="text" class="text" value="'+ Data.conf_title +'" disabled/>';   
        
        // доступен для
        out = out + '<p><label><b>Доступна для: </b></label>';
        out += '<select name="accessok" id="accessok" class="select" style="width: 500px;">';
        for (i = 0; i < Data.accessok.length; i++)
        {
            var line = Data.accessok[i];
            if (i == 0)
            {
                var select = " selected";
            }
            else
            {
                var select = "";
            }                      
            out += '<option value="' + line.group_id + '" class="line"' + select + '>' + line.group_name + '</option>';
        }
        out += '</select>&nbsp;&nbsp;&nbsp;<input type="button" value="Удалить" onClick="DeleteCGroup(' + rId + ', \'' + mtype + '\');"/></p>';
        // 
        
        // НЕ доступен для
        out = out + '<p><label><b>Не доступна для:</b></label>';
        out += '<select name="accesserr" id="accesserr" class="select" style="width: 500px;">';
        for (i = 0; i < Data.accesserr.length; i++)
        {
            var line = Data.accesserr[i];
            if (i == 0)
            {
                var select = " selected";
            }
            else
            {
                var select = "";
            }                      
            out += '<option value="' + line.group_id + '" class="line"' + select + '>' + line.group_name + '</option>';
        }
        out += '</select>&nbsp;&nbsp;&nbsp;<input type="button" value="Добавить" onClick="AddCGroup(' + rId + ', \'' + mtype + '\');"/></p></form>';                                                  
        out = out + "</div>"; 
              
        $(".accessConsole").html(out);                
    } 
    else
    {
        $(".accessConsole").html('<div calss="error">Упс, что-то пошло не так :(</div>');    
    }  
}
function DeleteCGroup(rId, mtype)
{   
    var conf_id = $("#confId").val();
    var group_id = $("#accessok").val();    
    
    if (group_id == null)
    {
        ShowError("Не указана группа для удаления!");
        return;
    }
    
    $.post("/accessmanager/deleteCR", { conf_id: conf_id, group_id: group_id }, function (result) {
        
        if (result) 
        {  
            ShowSuccess("Данные успешно сохранены!");
            OpenRight(-1, rId, mtype);       
        }
        else
        {
            ShowError("Не удалось удалить доступ :(!");
        }
        
    }, "json");
}
function AddCGroup(rId, mtype)
{
    var conf_id = $("#confId").val();
    var group_id = $("#accesserr").val(); 
    
    if (group_id == null)
    {
        ShowError("Не указана группа для добавления!");
        return;
    }
    
    $.post("/accessmanager/addCR", { conf_id: conf_id, group_id: group_id }, function (result) {
        
        if (result) 
        {
            ShowSuccess("Данные успешно сохранены!");
            OpenRight(-1, rId, mtype);       
        }
        else
        {
            ShowError("Не удалось добавить доступ для специальности :(!");
        }
        
    }, "json");
}
function DeleteConf(rId, mtype)
{   
    var group_id = $("#groupId").val();
    var conf_id = $("#accessok").val();
    
    if (conf_id == null)
    {
        ShowError("Не указана конференция!");
        return;
    }
    
    $.post("/accessmanager/deleteCR", { conf_id: conf_id, group_id: group_id }, function (result) {
        
        if (result) 
        {
            ShowSuccess("Данные успешно сохранены!");
            OpenRight(-1, rId, mtype);       
        }
        else
        {
            ShowError("Не удалось удалить доступ к конференции :(!");
        }
        
    }, "json");
    
}
function AddConf(rId, mtype)
{
    var group_id = $("#groupId").val();
    var conf_id = $("#accesserr").val();
    
    if (conf_id == null)
    {
        ShowError("Не указан документ для добавления!");
        return;
    }
    
    $.post("/accessmanager/addCR", { conf_id: conf_id, group_id: group_id }, function (result) {
        
        if (result) 
        {
            ShowSuccess("Данные успешно сохранены!");
            OpenRight(-1, rId, mtype);       
        }
        else
        {
            ShowError("Не удалось добавить доступ к конференции :(!");
        }
        
    }, "json");
}



// =============================================
// ФУНКЦИИ ОБРАБОТКИ ПРАВ ДОСТУПА К ОБЪЯВЛЕНИЯМ
// =============================================

// Отобразить данные по конкретной группе
function A_GroupPanel(Data, rId, mtype)
{        
    if (Data != null)
    {               
        // формируем поле ошибок          
        var out = '<hr><div class="wrapper">';       
        out += '<form id="form" class="blocks">';
        out += '<input type="hidden" id="groupId" value="' + Data.group_id + '"/>';
        out += '<p><label>Группа:</label><input type="text" class="text" value="' + Data.group_title + '" disabled/>';   
        
        // доступен для
        out = out + '<p><label><b>Имеет доступ к: </b></label>';
        out += '<select name="accessok" id="accessok" class="select" style="width: 300px;">';
        for (i = 0; i < Data.accessok.length; i++)
        {
            var line = Data.accessok[i];
            if (i == 0)
            {
                var select = " selected";
            }
            else
            {
                var select = "";
            }                      
            out += '<option value="' + line.ann_id + '" class="line"' + select + '>' + line.ann_name + '</option>';
        }
        out += '</select>&nbsp;&nbsp;&nbsp;<input type="button" value="Удалить" onClick="A_DeleteAnn(' + rId + ', \'' + mtype + '\');"/></p>';
        // 
        
        // НЕ доступен для
        out = out + '<p><label><b>Нет доступа к: </b></label>';
        out += '<select name="accesserr" id="accesserr" class="select" style="width: 300px;">';
        for (i = 0; i < Data.accesserr.length; i++)
        {
            var line = Data.accesserr[i];
            if (i == 0)
            {
                var select = " selected";
            }
            else
            {
                var select = "";
            }                      
            out += '<option value="' + line.ann_id + '" class="line"' + select + '>' + line.ann_name + '</option>';
        }
        out += '</select>&nbsp;&nbsp;&nbsp;<input type="button" value="Добавить" onClick="A_AddAnn(' + rId + ', \'' + mtype + '\');"/></p></form>';                                                  
        out = out + "</div>"; 
              
        $(".accessConsole").html(out);                
    } 
    else
    {
        $(".accessConsole").html('<div calss="error">Упс, что-то пошло не так :(</div>');    
    }  
}
// Отобразить данные по конкретной объяве
function A_AnnPanel(Data, rId, mtype)
{      
    if (Data != null)
    {               
        // формируем поле ошибок          
        var out = '<hr><div class="wrapper">';       
        out += '<form id="form" class="blocks">';
        out += '<input type="hidden" id="annId" value="' + Data.ann_id + '"/>';
        out += '<p><label>Объявление:</label><input type="text" class="text" value="'+ Data.ann_title +'" disabled/>';   
        
        // доступен для
        out = out + '<p><label><b>Доступно для: </b></label>';
        out += '<select name="accessok" id="accessok" class="select" style="width: 500px;">';
        for (i = 0; i < Data.accessok.length; i++)
        {
            var line = Data.accessok[i];
            if (i == 0)
            {
                var select = " selected";
            }
            else
            {
                var select = "";
            }                      
            out += '<option value="' + line.group_id + '" class="line"' + select + '>' + line.group_name + '</option>';
        }
        out += '</select>&nbsp;&nbsp;&nbsp;<input type="button" value="Удалить" onClick="A_DeleteGroup(' + rId + ', \'' + mtype + '\');"/></p>';
        // 
        
        // НЕ доступен для
        out = out + '<p><label><b>Не доступно для:</b></label>';
        out += '<select name="accesserr" id="accesserr" class="select" style="width: 500px;">';
        for (i = 0; i < Data.accesserr.length; i++)
        {
            var line = Data.accesserr[i];
            if (i == 0)
            {
                var select = " selected";
            }
            else
            {
                var select = "";
            }                      
            out += '<option value="' + line.group_id + '" class="line"' + select + '>' + line.group_name + '</option>';
        }
        out += '</select>&nbsp;&nbsp;&nbsp;<input type="button" value="Добавить" onClick="A_AddGroup(' + rId + ', \'' + mtype + '\');"/></p></form>';                                                  
        out = out + "</div>"; 
              
        $(".accessConsole").html(out);                
    } 
    else
    {
        $(".accessConsole").html('<div calss="error">Упс, что-то пошло не так :(</div>');    
    }  
}

function A_DeleteGroup(rId, mtype)
{   
    var ann_id = $("#annId").val();
    var group_id = $("#accessok").val();    
    
    if (group_id == null)
    {
        ShowError("Не указана группа для удаления!");
        return;
    }
    
    $.post("/accessmanager/deleteAR", { ann_id: ann_id, group_id: group_id }, function (result) {
        
        if (result) 
        {  
            ShowSuccess("Данные успешно сохранены!");
            OpenRight(-1, rId, mtype);       
        }
        else
        {
            ShowError("Не удалось удалить доступ :(!");
        }
        
    }, "json");
}
function A_AddGroup(rId, mtype)
{
    var ann_id = $("#annId").val();
    var group_id = $("#accesserr").val(); 
    
    if (group_id == null)
    {
        ShowError("Не указана группа для добавления!");
        return;
    }
    
    $.post("/accessmanager/addAR", { ann_id: ann_id, group_id: group_id }, function (result) {
        
        if (result) 
        {
            ShowSuccess("Данные успешно сохранены!");
            OpenRight(-1, rId, mtype);       
        }
        else
        {
            ShowError("Не удалось добавить доступ для специальности :(!");
        }
        
    }, "json");
}

function A_DeleteAnn(rId, mtype)
{   
    var group_id = $("#groupId").val();
    var ann_id = $("#accessok").val();
    
    if (ann_id == null)
    {
        ShowError("Не указано объявление!");
        return;
    }
    
    $.post("/accessmanager/deleteAR", { ann_id: ann_id, group_id: group_id }, function (result) {
        
        if (result) 
        {
            ShowSuccess("Данные успешно сохранены!");
            OpenRight(-1, rId, mtype);       
        }
        else
        {
            ShowError("Не удалось удалить доступ к объявлению :(!");
        }
        
    }, "json");
    
}
function A_AddAnn(rId, mtype)
{
    var group_id = $("#groupId").val();
    var ann_id = $("#accesserr").val();
    
    if (conf_id == null)
    {
        ShowError("Не указано объявление для добавления!");
        return;
    }
    
    $.post("/accessmanager/addAR", { ann_id: ann_id, group_id: group_id }, function (result) {
        
        if (result) 
        {
            ShowSuccess("Данные успешно сохранены!");
            OpenRight(-1, rId, mtype);       
        }
        else
        {
            ShowError("Не удалось добавить доступ к объявлению :(!");
        }
        
    }, "json");
}



// ====================================
// ФУНКЦИИ ОБРАБОТКИ НАЖАТИЙ ПО ССЫЛКАМ
// ====================================

// Получает список документов с описанием всех спец, которые имеют к нему доступ    
function docs()
{
    clearConsoleMess();    
    LoaderProcess();    
    
    $.post("/accessmanager/getDocs", {},
    
        function (result) {
      
            if (result != false)
            {                                               
                DataShow(result, 't_docs');                
            }
            else
            {
                $(".accessConsole").empty();
                $(".accessConsole").append("<h3>Пусто :(</h3>");   
            }
        }, "json") 
}
// Получает список специальостей с описанием всех документов, к которым она имеет доступ
function specDocs()
{
    clearConsoleMess();
    LoaderProcess();
    
    $.post("/accessmanager/getSpecDocs", {},
    
        function (result) {
                  
            if (result != false)
            {                                             
                DataShow(result, 't_docSpec');                
            }
            else
            {
                $(".accessConsole").empty();
                $(".accessConsole").append("<h3>Пусто :(</h3>");   
            }
        }, "json")
}
// Получает список групп с описанием всех конференций, к которым она имеет доступ
function c_groups()
{
    clearConsoleMess();
    LoaderProcess();
    
    $.post("/accessmanager/getCGroups", {},
    
        function (result) {
                  
            if (result != false)
            {                                             
                DataShow(result, 't_confs');                
            }
            else
            {
                $(".accessConsole").empty();
                $(".accessConsole").append("<h3>Пусто :(</h3>");   
            }
        }, "json")
}
// Получает список конференций с группами, для которых они доступны
function groupConfs()
{
    clearConsoleMess();
    LoaderProcess();
    
    $.post("/accessmanager/getGroupConfs", {},
    
        function (result) {
                  
            if (result != false)
            {                                             
                DataShow(result, 't_confGroups');                
            }
            else
            {
                $(".accessConsole").empty();
                $(".accessConsole").append("<h3>Пусто :(</h3>");   
            }
        }, "json")
}
// Получает список групп с объявлениями, которые доступны для нее
function a_groups()
{
    clearConsoleMess();
    LoaderProcess();
    
    $.post("/accessmanager/getAGroups", {},
    
        function (result) {
                  
            if (result != false)
            {                                             
                DataShow(result, 't_announs');                
            }
            else
            {
                $(".accessConsole").empty();
                $(".accessConsole").append("<h3>Пусто :(</h3>");   
            }
        }, "json")
}
// Получает список объявлений и групп, для которых они доступны
function groupAnnouns()
{
    clearConsoleMess();
    LoaderProcess();
    
    $.post("/accessmanager/getGroupAnnouns", {},
    
        function (result) {
                  
            if (result != false)
            {                                             
                DataShow(result, 't_announGroups');                
            }
            else
            {
                $(".accessConsole").empty();
                $(".accessConsole").append("<h3>Пусто :(</h3>");   
            }
        }, "json")
}

// ===============================
// ФУНКЦИИ ВЫВОДА ДАННЫХ НА ФОРМУ
// ===============================

function DataShow(result, mtype)
{
    // добавляем данные в поле accessConsole
        $(".accessConsole").empty();
        var out = ""; 
        
        var widthField1 = ' width: 550px;';
        var widthField2 = '';      
                        
        out = out + '<table id="table_mess">';        
        out = out + '<tr>';
        out = out +   '<th>№ п.п.</th>';
                
        // ДОКУМЕНТЫ
        if (mtype == 't_docs')
        {
            out = out +   '<th>Документ</th>';
            out = out +   '<th>Специальности</th>';
        } 
        else if (mtype == 't_docSpec')
        {
            out = out +   '<th>Специальность</th>';
            out = out +   '<th>Документы</th>';
            
            widthField1 = '';
            widthField2 = ' width: 550px;';    
        }
        
        // КОНФЕРЕНЦИИ
        else if (mtype == 't_confs')
        {
            out = out +   '<th>Конференция</th>';
            out = out +   '<th>Группы</th>';    
        }
        else if (mtype == 't_confGroups')
        {
            out = out +   '<th>Группа</th>';
            out = out +   '<th>Конференции</th>';  
            
            widthField1 = '';
            widthField2 = ' width: 550px;';
        }
        
        // ОБЪЯВЛЕНИЯ
        else if (mtype == 't_announs')
        {
            out = out +   '<th>Объявление</th>';
            out = out +   '<th>Группы</th>';    
        }
        else if (mtype == 't_announGroups')
        {
            out = out +   '<th>Группа</th>';
            out = out +   '<th>Объявления</th>'; 
            
            widthField1 = '';
            widthField2 = ' width: 550px;';
        }        
        out += '<input type="hidden" id="r_type" value="' + mtype + '"/>';                      
        out = out +  '</tr>';             
                                        
        // формируем строки
        for (i = 0; i < result.length; i++)
        {                              
            // выводим, как обычное
            if (i % 2 == 0)
                out = out + '<tr class="alt" r_id="' + result[i].id + '">';
            else
                out = out + '<tr r_id="' + result[i].id + '">';
           
            out = out + '<td style="text-align:center; width: 70px;">' + (i + 1) + '</td>';            
            
            out = out + '<td style="text-align:left;' + widthField1 + '">' + result[i].field1 + '</span></td>';            
            // Формируем список для второго поля
            var field2 = '';
            for (j = 0; j < result[i].field2.length; j++)
            {
                if (j + 1 == result[i].field2.length)
                {
                    field2 += '>> ' + result[i].field2[j].title;
                }  
                else
                {
                    field2 += '>> ' + result[i].field2[j].title + '<br/>';
                }                      
            }
           
            out = out + '<td style="text-align:left;' + widthField2 + '">' + field2 + '</td>';     
                                                          
            out = out + '</tr>';           
        } 
        
        out = out + '</table>'; 
        $(".accessConsole").append(out);
}

function OpenRight(mode, rId, mtype) {
// ф-ия открывает текущую запись о праве; 
    
    if (mode != -1)
    {
        // Вызвали функцию по клику
        var rId = $(this).parent().attr('r_id');
        var mtype = $("#r_type").val();
    }
       
    LoaderProcess();
    
    if (mtype == 't_docs')
    {        
        $.post('/accessmanager/OpenDoc', {doc_id: rId}, function(result) { DocPanel(result, rId, mtype); }, "json");  
    }
    else if (mtype == 't_docSpec')
    {

        $.post('/accessmanager/OpenSpec', {spec_id: rId}, function(result) { SpecPanel(result, rId, mtype); }, "json");
    } 
    
    // КОНФЕРЕНЦИИ!!
    else if (mtype == 't_confs')
    {
        $.post('/accessmanager/OpenConf', {conf_id: rId}, function(result) { ConfPanel(result, rId, mtype); }, "json");
    }
    else if (mtype == 't_confGroups')
    {
        $.post('/accessmanager/OpenCGroup', {group_id: rId}, function(result) { GroupPanel(result, rId, mtype); }, "json");
    }
    
    // ОБЪЯВЛЕНИЯ       
    else if (mtype == 't_announs')
    {
        $.post('/accessmanager/OpenAnn', {ann_id: rId}, function(result) { A_AnnPanel(result, rId, mtype); }, "json");
    }
    else if (mtype == 't_announGroups')
    {
        $.post('/accessmanager/OpenAGroup', {group_id: rId}, function(result) { A_GroupPanel(result, rId, mtype); }, "json");
    }
    
                        
} 
