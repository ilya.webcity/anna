﻿$(document).ready(function(){
	$('.del').click(function(){
		alert('Del!');
		tr_user = $(this).parent().parent();
		$.post('/specialty/delete/', {id: tr_user.attr('id_itm')}, function(data) {
			alert('Специальность номер ' + tr_user.attr('id_itm') + ' удалена');
			//tr_user.hide();
			$.get('/specialty/getlist/', function(data) {
				$('#items_div').html(data);
			});
		});
	});
	
	$('.item_table tr td').live('click', function(){
		//alert('edit');
		tr_user = $(this).parent();
		$.getJSON('/specialty/get/', {id: tr_user.attr('id_itm')}, function(data) {
			//alert('Пользователь номер ' + tr_user.attr('id_usr') + ' удален');
			//tr_user.hide();;
			//alert(data.group_id);
			$('#tr_id td:last').html(data.spec_id);
			$('input[name="spec_name"]').val(data.spec_name);
			$('textarea[name="spec_about"]').val(data.spec_about);
		});
	});
	
	$('input[name="user_add"]').click(function () {
		
		$('#tr_id td:last').html('');
		$('input[name="spec_name"]').val('');
		$('textarea[name="spec_about"]').val('');
	});
	
	$('input[name="user_edit"]').click(function () {
		
		id = $('#tr_id td:last').html();
		if (id == null)
			id = 0;
			//alert(id);
		spec_name = $('input[name="spec_name"]').val();
		if (!spec_name)
		{
			alert('Введите название специальности');
			exit();
		}
		spec_about = $('textarea[name="spec_about"]').val();
		$.get('/specialty/add/', 
			{
				spec_id: id, 
				spec_name: spec_name, 
				spec_about:spec_about 
			}, 
			function(data) {
				if (id)
					alert('Специальность ' + spec_name + ' изменена');
				else
					alert('Специальность создана');
				//alert(data);
				//tr_user.hide();
				$.get('/specialty/getlist/', function(data) {
					$('#items_div').html(data);
				});
				$('input[name="user_add"]').click();
		});
		
	});
	
	$('input[name="user_del"]').click(function () {
		tr_item = $('#tr_id td:last').html();;
		if (!tr_item)
		{
			alert('Нельзя удалить несущствующую');
			exit();
		}
		alert('Del!');
		$.post('/specialty/delete/', {id: tr_item}, function(data) {
			alert('Специальность номер ' + tr_item + ' удалена');
			//tr_user.hide();
				$.get('/specialty/getlist/', function(data) {
					$('#items_div').html(data);
				});
			$('input[name="user_add"]').click();
		});
	});
	
});

		