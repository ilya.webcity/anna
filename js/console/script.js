// модуль Консоль запросов - 
$(document).ready(function () {  
  
    
});

function ShowSuccess(msg) {
    var console = $("#errmessage"); 
    
    console.removeClass("errmessage");
    console.addClass("okmessage");
    console.html(msg);
	
	$(".queryResult").empty();
}

function ShowError(msg) {
    var console = $("#errmessage"); 
    
    console.removeClass("okmessage");
    console.addClass("errmessage");
    console.html(msg);
	
	$(".queryResult").empty();
}

function clearConsoleMess()
{
    var hasOk = $('#errmessage').hasClass('okmessage');
    var hasErr = $('#errmessage').hasClass('errmessage');        
    $("#errmessage").removeClass('okmessage', hasOk);
    $("#errmessage").removeClass('errmessage', hasErr);
    $("#errmessage").empty();
}

function LoaderProcess() {  
    $(".queryResult").empty();
    $(".queryResult").append("<img src=\"/img/ajax_loader.gif\" class=\"loaderimg\"></img>");
}

// ===============================
// ФУНКЦИИ ВЫВОДА ДАННЫХ НА ФОРМУ
// ===============================

function ShowResult(result)
{
        
        //alert('123');
        if (result.error == 1) {
            ShowError(result.text);
            return;
        } else if (!result.table)
		{
            ShowSuccess(result.text);
            return;
        }
        
        var resultTable = result.table;
                
        $(".queryResult").empty();
        var out = "";           
                        
        out = out + '<table id="table_mess">';        
        out = out + '<tr>';
        
        // Формирование заголовков 
        for (var key in resultTable[0]) {
			out = out + '<th>' + key + '</th>';
        }               
                              
        out = out +  '</tr>';             
                                        
        // формируем строки
        for (var i = 0; i < resultTable.length; i++)
        { 
            if (i % 2 == 0)
                out = out + '<tr class="alt">';
            else
                out = out + '<tr>';
			
			for (var key in resultTable[i]) {
				//alert(resultTable[i][key]);
				out = out + '<td style="text-align:center;">' + resultTable[i][key] + '</td>';
			}            
                                           
            out = out + '</tr>';           
        } 
        
        out = out + '</table>';

		//alert(out);
        $(".queryResult").append(out);
}

function query() {
    
    var query_text = $('#query_text').val();
	clearConsoleMess();
       
    LoaderProcess();   
           
    $.post('/console/query', {query_text: query_text}, function(result) { ShowResult (result); }, "json");    
                        
}

function showtables() {

	var query_text = 'show tables';
	$('#query_text').val(query_text);
	clearConsoleMess();       
    LoaderProcess(); 
	$.post('/console/query', {query_text: query_text}, function(result) { ShowResult (result); }, "json");      

} 
