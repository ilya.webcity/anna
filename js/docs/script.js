// модуль docs - администраторская часть
var redactor_object;

$(document).ready(function () {  

    var doc_cdate = $("#doc_cdate").val();
    var doc_udate = $("#doc_udate").val();
    
    // Активируем датаПикеры 
    $.datepicker.setDefaults(
        $.extend($.datepicker.regional["ru"])
    );
    $("#doc_cdate").datepicker();
    $("#doc_cdate").datepicker("option", "dateFormat", "yy-mm-dd");
    // устанавливаем дату
    $("#doc_cdate").datepicker("setDate", doc_cdate);

    // Дата обновления
    $("#doc_udate").datepicker();
    $("#doc_udate").datepicker("option", "dateFormat", "yy-mm-dd");
    // устанавливаем дату
    $("#doc_udate").datepicker("setDate", doc_udate);
    
    //Редактирование соседних тем
    $('#chaptermenu').dcFloater({
                width: 150,
                location: 'top',
                align: 'right',
                offsetLocation: 10,
                offsetAlign: 20,
                speedFloat: 1500,
                speedContent: 600,
                tabText: 'Темы раздела',
                autoClose: true,
                event: 'click'
    });
    
    // ПОДКЛЮЧАЕМ ВИзУАЛЬНЫЙ РЕДАКТОР
    	
   // ПОДКЛЮЧАЕМ ВИзУАЛЬНЫЙ РЕДАКТОР  
	
	$('#doc_content').tinymce({
			// Location of TinyMCE script
			script_url : '/js/tiny_mce/tiny_mce.js',
			
			// НОВОЕ
			file_browser_callback : "tinyBrowser",
			// НОВОЕ
			
			// General options
			width: "100%",
			height: "500px",
			theme : "advanced",
			plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",
			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			// Example content CSS (should be your site CSS)

			content_css : "/css/style.css",

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "/js/tiny_mce/plugins/lists/template_list.js",
			external_link_list_url : "/js/tiny_mce/plugins/lists/link_list.js",
			external_image_list_url : "/js/tiny_mce/plugins/lists/image_list.js",
			media_external_list_url : "/js/tiny_mce/plugins/lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});

    
});

function ShowSuccess(msg) {
    var console = $("#errmessage"); 
    
    console.removeClass("errmessage");
    console.addClass("okmessage");
    console.html(msg);
    
    console = $("#errmessage2"); 
    
    console.removeClass("errmessage");
    console.addClass("okmessage");
    console.html(msg);
}

function ShowError(msg) {
    var console = $("#errmessage"); 
    
    console.removeClass("okmessage");
    console.addClass("errmessage");
    console.html(msg);
    
    console = $("#errmessage2"); 
    
    console.removeClass("okmessage");
    console.addClass("errmessage");
    console.html(msg);
}

function clearConsoleMess()
{
    var hasOk = $('#errmessage').hasClass('okmessage');
    var hasErr = $('#errmessage').hasClass('errmessage');        
    $("#errmessage").removeClass('okmessage', hasOk);
    $("#errmessage").removeClass('errmessage', hasErr);
    $("#errmessage").html('');
}

function saveCourse() {
    
    var doc_id = $('#doc_id').val();
    var doc_pid = $('#doc_pid').val();
    var doc_type = $('#doc_type').val();
    var doc_title = $('#doc_title').val();
    //var doc_content = redactor_object.getCodeEditor();
    var doc_content = $('#doc_content').val();
    var doc_author = $('#doc_author').val();
    var doc_cdate = $('#doc_cdate').val();
    var doc_udate = $('#doc_udate').val();
    var doc_updater = $('#doc_updater').val();
    var doc_number = $('#doc_number').val();
    var doc_test = $('#doc_test').val();
       
    var errText = '';
    if (doc_title == '') {
        errText += 'ОШИБКА: Не указан заколовок!<br/>';
    }
    if (doc_cdate == '') {
        errText += 'ОШИБКА: Не указана дата создания!<br/>';
    }
    if (doc_udate == '') {
        errText += 'ОШИБКА: Не указана дата обновления!<br/>';
    }
    
    if (errText != '') {
        ShowError(errText);
        return;
    }
    
    $.post("/docs/saveDoc",
        {
            doc_id: doc_id,
            doc_pid: doc_pid,
            doc_type: doc_type,
            doc_title: doc_title,
            doc_content: doc_content,
            doc_author: doc_author,
            doc_cdate: doc_cdate,
            doc_udate: doc_udate,
            doc_updater: doc_updater,
            doc_number: doc_number,
            doc_test: doc_test
        },
        function (result)
        {              
            
            if (result >= 0)
            {
                ShowSuccess("Данные успешно сохранены!");    
            }   
            else
            {
                ShowError("В процессе сохранения возникли ошибки!");        
            } 
        }, "json")
}

function newCourse() {
    
    var doc_pid = $('#doc_pid').val();
    var doc_type = $('#doc_type').val();
    var doc_title = $('#doc_title').val();
    var doc_content = $('#doc_content').val();
    //var doc_content = redactor_object.getCodeEditor();
    var doc_author = $('#doc_author').val();
    var doc_cdate = $('#doc_cdate').val();
    var doc_udate = $('#doc_udate').val();
    var doc_updater = $('#doc_updater').val();
    var doc_number = $('#doc_number').val();
    var doc_test = $('#doc_test').val();
    
        
    var errText = '';
    if (doc_title == '') {
        errText += 'ОШИБКА: Не указан заколовок!<br/>';
    }
    if (doc_cdate == '') {
        errText += 'ОШИБКА: Не указана дата создания!<br/>';
    }
    if (doc_udate == '') {
        errText += 'ОШИБКА: Не указана дата обновления!<br/>';
    }
    
    if (errText != '') {
        ShowError(errText);
        return;
    }
    
    $.post("/docs/saveNewDoc",
        {            
            doc_pid: doc_pid,
            doc_type: doc_type,
            doc_title: doc_title,
            doc_content: doc_content,
            doc_author: doc_author,
            doc_cdate: doc_cdate,
            doc_udate: doc_udate,
            doc_updater: doc_updater,
            doc_number: doc_number,
            doc_test: doc_test
        },
        function (result)
        {              
            
            if (result >= 0)
            {
                ShowSuccess("Документ успешно создан!");    
            }   
            else
            {
                ShowError("В процессе создания возникли ошибки!");        
            } 
        }, "json")
}

function delCourse() {

    if (confirm("Вы уверены, что хотите удалить текущий документ?") == false) {
        return;
    }
    var doc_id = $('#doc_id').val(); 
    
    $.post("/docs/deleteDoc",
        {
            doc_id: doc_id            
        },
        function (result)
        {              
            if (result >= 0)
            {
                location.href = '/docs/panel/doc';
                ShowSuccess("Документ успешно удален!");                 
            }   
            else
            {
                ShowError("В процессе удаления возникли ошибки!");        
            } 
        }, "json")    
    
}