﻿$(document).ready(function(){


	redactor =  $('#doc_content').redactor();
	
	$('.del').live('click',function(){
		alert('Del!');
		tr_user = $(this).parent().parent();
		$.post('/menu/delete/', {id: tr_user.attr('id_itm')}, function(data) {
			alert('Пункт меню номер ' + tr_user.attr('id_itm') + ' удален');
			//tr_user.hide();
			$.get('/menu/getlist/', function(data) {
				$('#items_div').html(data);
			});
			$.get('/menu/getselect/', function(data) {
				$('select[name="menu_pid"]').parent().html(data);
			});
			$('input[name="user_add"]').click();
		});
	});
	var old_id = 0;
	$('.item_table tr td span').live('click', function(){
		//alert('edit');
		tr_user = $(this).parent().parent();
		$.getJSON('/docs/get/', {id: tr_user.attr('id_itm')}, function(data) {
			//alert('Пользователь номер ' + tr_user.attr('id_usr') + ' удален');
			//tr_user.hide();;
			//alert(data.doc_author);
			if (tr_user.attr('id_itm') != old_id)
			if ($('#items_div_add').html() != '')
			{
				redactor.destroy();
				tr_user.children('td').children('div').html($('#items_div_add').html());
				$('#items_div_add').html('');
				redactor =  $('#doc_content').redactor();
			}
			else
			{
				redactor.destroy();
				tr_user.children('td').children('div').html($('tr[id_itm="'+old_id+'"]').children('td').children('div').html());
				$('tr[id_itm="'+old_id+'"]').children('td').children('div').html('');
				redactor =  $('#doc_content').redactor();
			}
			old_id = tr_user.attr('id_itm');
			
			$('#tr_id td:last').html(data.doc_id);
			$('input[name="doc_title"]').val(data.doc_title);
			$('select[name="doc_pid"]').val(data.doc_pid);
			redactor.setHtml(data.doc_content);
			//$('textarea[name="doc_content"]').val(data.doc_content);
			$('td[name="doc_author"]').html(data.doc_author);
			if (data.doc_updater !=0)
				$('td[name="doc_author"]').html($('td[name="doc_author"]').html()+'/'+data.doc_updater);
			$('input[name="doc_number"]').val(data.doc_number);
			
			//$('select[name="color"]').val(data.color);
			//$('img[id="'+data.color+'"]').click();
		});
	});
	
	$('input[name="user_add"]').live('click', function(){
		

			$('#tr_id td:last').html('');
			$('input[name="doc_title"]').val('');
			$('select[name="doc_pid"]').val('');
			redactor.setHtml('code');
			//$('textarea[name="doc_content"]').val('');
			$('td[name="doc_author"]').html('');
			$('input[name="doc_number"]').val('');
	});
	
	$('input[name="user_edit"]').live('click', function () {
		
		id = $('#tr_id td:last').html();
		if (id == null)
			id = 0;
			//alert(id);
		doc_title = $('input[name="doc_title"]').val();
		if (!doc_title)
		{
			alert('Введите название');
			exit();
		}
		doc_pid = $('select[name="doc_pid"]').val();
		doc_content = $('textarea[name="doc_content"]').val();
		doc_number = $('input[name="doc_number"]').val();
		if (!doc_number)
		{
			//alert('Введите номер');
			doc_number = 1;
			//exit();
		}
		
		$.get('/docs/add/', 
			{
				doc_id: id, 
				doc_title: doc_title, 
				doc_pid: doc_pid, 
				doc_content: doc_content, 
				doc_number: doc_number
			}, 
			function(data) {
				if (id)
					alert('Документ ' + doc_title + ' изменен');
				else
					alert('Документ создан');
				redactor.destroy();
				$('#items_div_add').html($('tr[id_itm="'+old_id+'"]').children('td').children('div').html());	
				old_id = 0;
				redactor =  $('#doc_content').redactor();
				
				//alert(data);
				//tr_user.hide();
				$.get('/docs/getlist/', function(data) {
					$('#items_div').html(data);
				});
				$.get('/docs/getselect/', function(data) {
					$('select[name="doc_pid"]').parent().html(data);
				});
				$('input[name="user_add"]').click();
		});
		
	});
	
	$('input[name="user_del"]').live('click', function () {
		tr_item = $('#tr_id td:last').html();;
		if (!tr_item)
		{
			alert('Нельзя удалить');
			exit();
		}
		alert('Del!');
		$.post('/docs/delete/', {id: tr_item}, function(data) {
			alert('удален');
			//tr_user.hide();
				$.get('/docs/getlist/', function(data) {
					$('#items_div').html(data);
				});
				$.get('/docs/getselect/', function(data) {
					$('select[name="doc_pid"]').parent().html(data);
				});
			$('input[name="user_add"]').click();
		});
	});
	
	$.get('/docs/getlist/', function(data) {
		$('#items_div').html(data);
	});
	
	
	$.get('/docs/getselect/', function(data) {
		$('select[name="doc_pid"]').parent().html(data);
	});
});

		