﻿$(function(){
	
	$("#form").validate({
		rules: {
			teacher: {
				required: true				
			},
			name: {
				required: true,
				minlength: 4
			},
			group: {
				required: true				
			},
			theme: {
				required: true,
				minlength: 8
			},			
			message: {
				required: true
			}
		},
		messages: {
			teacher: {
				required: 'Выберите преподавателя'				
			},
			name: {
				required: 'Введите имя'				
			},
			group: {
				required: 'Укажите номер группы'				
			},
			theme: {
				required: 'Укажите тему',
				minlength: 'Минимальная длина: 8'
			},
			message: {
				required: 'Необходимо ввести вопрос'
			}
		},
		success: function(label) {
			label.html('OK').removeClass('error').addClass('ok');
			setTimeout(function(){
				label.fadeOut(500);
			}, 2000)
		}
	});
	
});