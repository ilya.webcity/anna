// модуль для загрузки информации о тестах, к которым имеет отношение студент

function LoaderProcess() {
    $(".scoreConsole").append("<img src=\"/img/ajax_loader.gif\" class=\"loaderimg\"></img>");
}

function allTests()
{
    LoaderProcess();
    
    $.post("score/allTests", {},
    
        function (result) {
                  
            if (result != null)
            {                                             
                DataShow(result);                
            }
            else
            {
                $(".scoreConsole").append("<h3>Вы еще не участвовали ни в одном тесте..</h3>");   
            }
        }, "json")
}

function doneTests()
{
    LoaderProcess();
    
    $.post("score/doneTests", {},
    
        function (result) {
                  
            if (result != null)
            {                                             
                DataShow(result);                
            }
            else
            {
                $(".scoreConsole").append("<h3>Вы еще не участвовали ни в одном тесте..</h3>");   
            }
        }, "json")
}

function failTests()
{
    LoaderProcess();
    
    $.post("score/failTests", {},
    
        function (result) {
                  
            if (result != null)
            {                                             
                DataShow(result);                
            }
            else
            {
                $(".scoreConsole").append("<h3>Вы еще не участвовали ни в одном тесте..</h3>");   
            }
        }, "json")
}

function DataShow(result)
{
    // добавляем данные в поле scoreConsole
                $(".scoreConsole").empty();
                var out = "";
                
                out = out + '<table id="table">';
                out = out + '<tr>';
                out = out +   '<th>№ п.п.</th>';
                out = out +   '<th>Дата</th>';
                out = out +   '<th>Тест</th>';
                out = out +   '<th>Описание</th>';
                out = out +   '<th>Оценка</th>';
                out = out +   '<th>Статус</th>';
                out = out +   '<th>Раздел</th>';
                out = out +  '</tr>';             
                                                
                // формируем строки
                for (i = 0; i < result.length; i++)
                {
                    //out = "";
                                       
                    if (i % 2 == 0)
                        out = out + '<tr class="alt">';
                    else
                        out = out + '<tr>';
                    
                    out = out + '<td style="text-align:center; width: 70px;">' + (i + 1) + '</td>';
                    out = out + '<td style="text-align:center; width: 120px;">' + result[i].study_date + '</td>';
                    out = out + '<td><a href="test/go/' + result[i].test_id + '">' + result[i].test_title + '</a></td>';
                    out = out + '<td>' + result[i].test_about + '</td>';
                    out = out + '<td style="text-align:center;">' + result[i].study_mark + '</td>';
                    // проверяем, пройден ли тест
                    if (result[i].study_done == 1)
                        status = '<img src="/img/score/ok.png" title="Тест пройден"></img>';
                    else
                        status = '<img src="/img/score/fail.png" title="Тест не пройден"></img>';
                     
                    out = out + '<td style="text-align:center;">' + status + '</td>';
                    out = out + '<td><a href="docs/id/' + result[i].doc_id + '">' + result[i].doc_title + '</a></td>';
                    out = out + '</tr>'; 
                    
                    //$(".scoreConsole").append(out);                   
            
                } 
                
                out = out + '</table>'; 
                $(".scoreConsole").append(out);
}
