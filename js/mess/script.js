// модуль сообщений пользователя
$(document).ready(function () {  
    CatchInboxMail();
    setInterval("CatchInboxMail();", 4000); 
    $("#table_mess tr td").live("click", ReadMessage);    
 
});


function LoaderProcess() {
    
    $(".messConsole").empty();
    $(".messConsole").append("<img src=\"/img/ajax_loader.gif\" class=\"loaderimg\"></img>");
    
    $("#infomessage").removeClass("okmessage");
    $("#infomessage").html("");
}

function ReadMessage() {
// ф-ия открывает сообщение с mId;
    var mId = $(this).parent().attr('mess_id');
    var mType = $(this).parent().attr('mess_type');
    
    LoaderProcess();
     
    $.post("/mess/ReadMessage", 
        {
            id: mId,
            type: mType
        },
        
        function (result)
        {
            if (result != null)
            {
                // выводим сообщение на экран
                var mess = result[0];
                
                var FromTo = mess.user_fname + " " + mess.user_n + ". " + mess.user_l + ".";
                
                var out = "<hr>";
                
                out = out + '<div class="message">';
                out = out + '<p><b>Дата: </b>' + mess.mess_date + ' - ' + mess.mess_time + '</p>';
                if (mType == 'inbox')
                    out = out + '<p><b>От кого: </b>' + FromTo + '</p>';
                else
                    out = out + '<p><b>Кому: </b>' + FromTo + '</p>';
                out = out + '<p><b>Тема: </b>' + mess.mess_title + '</p>';
                out = out + '<hr>';
                out = out + '<p><b>Сообщение: </b></p><p>' + mess.mess_text + '</p>';
                
                var attach = mess.mess_attachment;
                
                if (attach != false) 
                {                   
                    //-- аттачи
                    out += '<div class="attachField">';
                    
                    out += '<h2>Прикрепленные файлы</h2>';   
 
                    // выводим все аттачи
                    for (j = 0; j < attach.length; j++)
                    {                            
                        var ex = attach[j];
                        out = out + '<a href=/download?date=' + ex.date + '&name=' + ex.file + '&source=' + ex.source + '>';
                        
                        out = out + '<img src="/img/attach.png" style="padding-right: 10px;" title="Cкачать"></img>' + attach[j].title + '</a>';                                 
                        
                        if (j < attach.length - 1)                            
                            out = out + '<br>';   
                    }
                    out += '</div>'
                }
                
                //формируем переменные для ответа
                var ToUserId = mess.user_id;
                var Theme = mess.mess_title;
                
                if (mType == 'inbox') 
                    out += '<br/><input type="submit" value="Ответить" onClick="ShowMailForm(' + ToUserId + ', \''+ FromTo + '\', \'' + Theme + '\');"/>'; 
                
                out = out + "</div>";
                
                $(".messConsole").html(out);
            } 
            else
            {
                $(".messConsole").html('<div calss="error">Сообщение на найдено :(</div>');    
            }  
        }, "json")                    
}

function MailBoard() {
    // отображает форму создания письма (при нажатии на кнопку "Написать письмо")
    
    LoaderProcess();
    
    // получаем список преподов
    $.post("/mess/getTeachersList", {},
    
        function (result) {
                  
            if (result.count != 0)
            {                                             

                // выводим формочку
                var console = $(".messConsole");
                console.empty();
                
                // формируем поле ошибок
                var Status = '<div id="errmessage"></div>'; 
                
                // формируем форму    
                var MailForm = Status + '<hr><div class="wrapper">';       
                MailForm += '<form id="form" class="blocks" action="/mess/mail" method="POST"  enctype="multipart/form-data"><p>';
                MailForm += '<label>Кому:</label>';
                
                MailForm += '<select name="userid" class="select">';
                for (i = 0; i < result.length; i++)
                {
                    var line = result[i];
                    MailForm += '<option value="' + line.user_id + '" class="line">' + line.user_fio + '</option>';
                }
                MailForm += '</select></p><p>';               
                
                MailForm += '<label>Тема:</label>';
                MailForm += '<input type="text" id="Theme" class="text" name="theme"/></p><p>';
                MailForm += '<label>Текст письма:</label>';
                MailForm += '<textarea class="textarea" id="MailText" name="message"></textarea></p><p>';
                MailForm += '<input type="file" id="uploadedfile" class="btnfile" name="uploadedfile" value="Прикрепить"></input></p><p>';
                MailForm += '<label>&nbsp;</label>';
                MailForm += '<input type="button" class="btn" value="Отправить письмо" onClick="Mail();"/></form></div>';
                
                console.html(MailForm);
                                    
            }
            else
            {
                $(".messConsole").html('<div calss="error">В системе отсутствуют преподаватели. Сообщение невозможно отправить!</div>');   

            }
        }, "json")         
}

function ShowMailForm(UserId, UserName, Theme) {
    // отображает форму создания письма (при нажатии на кнопку "ответить")
    var console = $(".messConsole");
    console.empty();
    
    // формируем поле ошибок
    var Status = '<div id="errmessage"></div>'; 
    
    // формируем форму    
    var MailForm = Status + '<hr><div class="wrapper">';       
    MailForm += '<form id="form" class="blocks" action="/mess/mail" method="POST"  enctype="multipart/form-data"><p>';
    MailForm += '<label>Кому:</label>';
    MailForm += '<input type="text" id="ToUser" class="text" name="user" user_id="' + UserId + '" value="' + UserName + '"/></p><p>';
    MailForm += '<input type="hidden" name="userid" value="' + UserId + '"/>';
    MailForm += '<label>Тема:</label>';
    MailForm += '<input type="text" id="Theme" class="text" name="theme" value="RE: ' + Theme + '"/></p><p>';
    MailForm += '<label>Текст письма:</label>';
    MailForm += '<textarea class="textarea" id="MailText" name="message"></textarea></p><p>';
    MailForm += '<input type="file" id="uploadedfile" class="btnfile" name="uploadedfile" value="Прикрепить"></input></p><p>';
    MailForm += '<label>&nbsp;</label>';
    MailForm += '<input type="button" class="btn" value="Отправить письмо" onClick="Mail();"/></form></div>';
    
    console.html(MailForm);     
}

function CatchInboxMail()
// ф-ия проверяет наличие новых писем
{
    $.post("/mess/NewMailCount", {},
    
        function (result) {
                  
            if (result.count != 0)
            {                                             
                $(".inbox_title").empty();
                $(".inbox_title").append("<span style=\"color: #A52A2A;  text-decoration: none;\">Входящие (" + result.count + ")</span>");                
            }
            else
            {
                $(".inbox_title").empty();
                $(".inbox_title").append("Входящие");   
            }
        }, "json")   
}    
    
function Inbox()
{
    LoaderProcess();
    
    $.post("/mess/Inbox", {},
    
        function (result) {
                  
            if (result != false)
            {                                             
                DataShow(result, 'inbox');                
            }
            else
            {
                $(".messConsole").empty();
                $(".messConsole").append("<h3>Пока что Вам еще никто ничего не написал.. :(</h3>");   
            }
        }, "json") 
}

function Outbox()
{
    LoaderProcess();
    
    $.post("/mess/Outbox", {},
    
        function (result) {
                  
            if (result != false)
            {                                             
                DataShow(result, 'outbox');                
            }
            else
            {
                $(".messConsole").empty();
                $(".messConsole").append("<h3>Вы еще никому ничего не писали..</h3>");   
            }
        }, "json")
}

function Mail(type)
// отправка письма
{
    
    var errconsole = $("#errmessage");    
    var UserId = $("#ToUser").attr("user_id");
    
    var Theme = $("#Theme").attr("value");
    var MailText = $("#MailText").val();
    var File = $("#uploadedfile").val();
         
    var errmess = "";
    
    if (UserId == "")
        errmess = "ОШИБКА: Укажите, кому Вы пишите письмо!<br>";              
    
    if (Theme == "")
        errmess += "ОШИБКА: Отсутвует тема!<br>"
        
    if (MailText == "")
        errmess += "ОШИБКА: Не введен текст письма!"
        
    if (errmess != "")
    {
        errconsole.addClass("errmessage");
        errconsole.html(errmess);
        return;
    }
    
    $("#form").submit();
    
    
}

function DataShow(result, type)
{
    // добавляем данные в поле messConsole
                $(".messConsole").empty();
                var out = "";
                var mType = ''; // тип собщения
                
                out = out + '<table id="table_mess">';
                out = out + '<tr>';
                out = out +   '<th>№ п.п.</th>';
                out = out +   '<th>Дата</th>';
                if (type == 'inbox') 
                {
                    out = out +   '<th>От кого</th>';
                    mType = 'inbox';
                } else
                {
                    out = out +   '<th>Кому</th>';
                    mType = 'outbox';    
                }
                out = out +   '<th>Тема</th>';
                out = out +   '<th>Приложение</th>';
                out = out +  '</tr>';             
                                                
                // формируем строки
                for (i = 0; i < result.length; i++)
                {
                    //out = "";
                    
                    if ((result[i].mess_unreaded == 1) && (type == 'inbox'))
                    {
                        // красим, как непрочитанное
                        out = out + '<tr class="unreaded" mess_id="' + result[i].mess_id + '" mess_type="' + mType + '">';                        
                    } else
                    {                                      
                        // выводим, как обычное
                        if (i % 2 == 0)
                            out = out + '<tr class="alt" mess_id="' + result[i].mess_id + '" mess_type="' + mType + '">';
                        else
                            out = out + '<tr mess_id="' + result[i].mess_id + '" mess_type="' + mType + '">';
                    }
                    
                    out = out + '<td style="text-align:center; width: 70px;">' + (i + 1) + '</td>';
                    out = out + '<td style="text-align:center; width: 120px;"><span title ="' + result[i].mess_time + '">' + result[i].mess_date + '</span></td>';
                    
                    FromTo = '';
                    
                    if (type == 'inbox') 
                    {   
                        FromTo = result[i].user_fname + " " + result[i].user_n + ". " + result[i].user_l + ".";
                    } else if (type == 'outbox')
                    {
                        FromTo = result[i].user_fname + " " + result[i].user_n + ". " + result[i].user_l + ".";    
                    }
                    out = out + '<td style="text-align:center; valign: middle;"><img src="/img/user_ico.png" style="padding-right: 10px;"></img>' + FromTo + '</td>';
                    out = out + '<td style="text-align:left;">' + result[i].mess_title + '</td>';
                    
                    var attach = result[i].mess_attachment;
                    
                    if (attach != false) 
                    {                   
                        out = out + '<td style="text-align:center; width: 70px;">';  
                        out = out + '<img src="/img/score/ok.png" title="В письме есть приложенные файлы"></img>';
                        out = out + '</td>';
                    } else
                    {
                        out = out + '<td style="text-align:center; width: 70px;"><img src="/img/score/fail.png" title="В письме нет приложенных файлов"></img></td>';
                    }
                        
                    out = out + '</tr>';           
                } 
                
                out = out + '</table>'; 
                $(".messConsole").append(out);
}
