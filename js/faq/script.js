// модуль faq - администраторская часть
$(document).ready(function () {  
    CatchNotPublicFaq();
    setInterval("CatchNotPublicFaq();", 4000); 
    $("#table_mess tr td").live("click", OpenFaq); 
});

function LoaderProcess() {
    
    $(".faqConsole").empty();
    $(".faqConsole").append("<img src=\"/img/ajax_loader.gif\" class=\"loaderimg\"></img>");
    
    $("#infomessage").removeClass("okmessage");
    $("#infomessage").html("");
}

function OpenFaq() {
// ф-ия открывает faq с mId;
    var mId = $(this).parent().attr('faq_id');

    LoaderProcess();
     
    $.post("/faq/OpenFaq", 
        {
            id: mId
        },
        
        function (result)
        {
            
            if (result != null)
            {
                // выводим faq на экран
                var faq = result[0];
                

                var FromU = faq.ans_fuser.user_fname + " " + faq.ans_fuser.user_n + ". " + faq.ans_fuser.user_l + ".";
                var ToU = faq.ans_tuser.user_fname + " " + faq.ans_tuser.user_n + ". " + faq.ans_tuser.user_l + "."; 
                
                // формируем поле ошибок
                var Status = '<div id="errmessage"></div>'; 
                                
                var out = Status + '<hr><div class="wrapper">';       
                out += '<form id="form" class="blocks" method="POST" action="/faq/saveFaq">';
                out += '<input type="hidden" id="mId" value="' + mId + '"/>';
                out += '<p><label>Дата вопроса:</label><input id="datepickerQuest" type="text" class="text"/></p>';
                out += '<p><label>Дата ответа:</label><input id="datepickerAns" type="text" class="text"/></p>';
                
                
                // автор
                out = out + '<p><label><b>Автор: </b></label>';
                out += '<select name="fuserid" id="fuserid" class="select">';
                for (i = 0; i < faq.allUsers.length; i++)
                {
                    var line = faq.allUsers[i];
                    if (line.user_id == faq.ans_fuser.user_id)
                    {
                        var select = " selected";
                    }
                    else
                    {
                        var select = "";
                    }                      
                    out += '<option value="' + line.user_id + '" class="line"' + select + '>' + line.user_fname + " " + line.user_n + ". " + line.user_l + '</option>';
                }
                out += '</select></p>';
                // автор
                
                // кому
                out = out + '<p><label><b>Кому: </b></label>';
                out += '<select name="tuserid" id="tuserid" class="select">';
                for (i = 0; i < faq.allUsers.length; i++)
                {
                    var line = faq.allUsers[i];
                    if (line.user_id == faq.ans_tuser.user_id)
                    {
                        var select = " selected";
                    }
                    else
                    {
                        var select = "";
                    }                      
                    out += '<option value="' + line.user_id + '" class="line"' + select + '>' + line.user_fname + " " + line.user_n + ". " + line.user_l + '</option>';
                }
                out += '</select></label></p>';
                // кому
                                                 
                out += '<p><label>Тема:</label>';
                out += '<input type="text" id="ans_theme" class="text" name="ans_theme" value="' + faq.ans_theme + '"></input></p>';
                out += '<p><label>Вопрос:</label>';
                out += '<textarea class="textarea" id="ans_ask" name="ans_ask">' + faq.ans_ask + '</textarea></p>';
                out += '<p><label>Ответ:</label>';
                out += '<textarea class="textarea" id="ans_text" name="ans_text">' + faq.ans_text + '</textarea></p>';
                out += '<p><label>Публикация:</label>';
                
                if (faq.ans_public == 1)
                {
                    var radiopublic = " checked";
                    var radiounpublic = "";
                }
                else
                {
                    var radiopublic = "";
                    var radiounpublic = " checked";    
                }
                
                out += '<input type="radio" name="ispublic" id="ispublic" value="1"' + radiopublic + '>Опубликован<br>';
                out += '<input type="radio" name="ispublic" id="ispublic" value="0"' + radiounpublic + '>Неопубликован<br>';                
                
                out += '<p><label>&nbsp;</label>';
                out += '<input type="button" class="btn" value="Сохранить" onClick="Save();"/></p>'
                out += '<input type="button" class="btn" value="Удалить" onClick="Delete();"/></p></form>';                
                                
                out = out + "</div>";
                
                $(".faqConsole").html(out);
                // Активируем датаПикера дата вопроса
                $.datepicker.setDefaults(
                    $.extend($.datepicker.regional["ru"])
                );
                $("#datepickerQuest").datepicker();
                $("#datepickerQuest").datepicker("option", "dateFormat", "yy-mm-dd");
                // устанавливаем дату
                $("#datepickerQuest").datepicker("setDate", faq.ans_dask);
                
                // Дата ответа
                $("#datepickerAns").datepicker();
                $("#datepickerAns").datepicker("option", "dateFormat", "yy-mm-dd");
                // устанавливаем дату
                $("#datepickerAns").datepicker("setDate", faq.ans_danswer);
                
            } 
            else
            {
                $(".faqConsole").html('<div calss="error">Вопрос не найден :(</div>');    
            }  
        }, "json")                    
}

// +
function CatchNotPublicFaq()
// ф-ия проверяет наличие неопубликованных faq
{
    $.post("/faq/aCatchNotPublicFaqCount", {},
    
        function (result) {
            
            if (result.count != 0)
            {                                             
                $(".notpublick_title").empty();
                $(".notpublick_title").append("<span style=\"color: #A52A2A;  text-decoration: none;\">Неопубликованные (" + result.count + ")</span>");                
            }
            else
            {
                $(".notpublick_title").empty();
                $(".notpublick_title").append("Неопубликованные");   
            }
        }, "json")   
}    
// + 
// Получает список неопубликованных faq    
function NotPublic()
{
    LoaderProcess();
    
    $.post("/faq/aGetNotPublicList", {},
    
        function (result) {
                   
            if (result != false)
            {                                             
                
                DataShow(result, 'notpublick');                
            }
            else
            {
                $(".faqConsole").empty();
                $(".faqConsole").append("<h3>Пусто :(</h3>");   
            }
        }, "json") 
}

function Public()
{
    LoaderProcess();
    
    $.post("/faq/aGetPublicList", {},
    
        function (result) {
                  
            if (result != false)
            {                                             
                DataShow(result, 'publick');                
            }
            else
            {
                $(".faqConsole").empty();
                $(".faqConsole").append("<h3>Пусто :(</h3>");   
            }
        }, "json")
}

function Save()
// сохранить FAQ
{         
    var errconsole = $("#errmessage");  

    var id = $("#mId").val();
        
    var FromU = $("#fuserid").val();
    var ToU = $("#tuserid").val();
    //var dask = $("#datepickerQuest").datepicker("getDate");
    var dask = $("#datepickerQuest").val();
    //var danswer = $("#datepickerAns").datepicker("getDate");
    var danswer = $("#datepickerAns").val(); 
    var theme = $("#ans_theme").val();
    var ans_text = $("#ans_text").val();
    var ans_ask = $("#ans_ask").val();    
    var ispublic = $("#ispublic:checked").val();
          
    var errmess = "";
    
    if (FromU == "")
        errmess = "ОШИБКА: Укажите, автора вопроса!<br>";              
    
    if (ToU == "")
        errmess += "ОШИБКА: Укажате, кто отвечает на вопрос!<br>";
        
    if (dask == null)
        errmess += "ОШИБКА: Укажите дату вопроса!<br>";
    
    if (danswer == null)
        errmess += "ОШИБКА: Укажите дату ответа!<br>"
    
    if (theme == "")
        errmess += "ОШИБКА: Укажате тему вопроса!<br>";
        
    if (ans_text == "")
        errmess += "ОШИБКА: Укажате вопрос!<br>";
        
    if (ans_ask == "")
        errmess += "ОШИБКА: Укажате ответ!<br>";
        
    if (errmess != "")
    {
        errconsole.addClass("errmessage");
        errconsole.html(errmess);
        return;
    }    
    
    //Отправляем данные
    $.post("/faq/saveFaq", 
        {
            id: id,
            FromU: FromU,
            ToU: ToU,
            dask: dask,
            danswer: danswer,
            theme: theme,
            ans_text: ans_text,
            ans_ask: ans_ask,
            ispublic: ispublic                                                            
        },
        
        function (result)
        {              
            if(result)
            {
                errconsole.addClass("okmessage");
                errconsole.html("Данные успешно сохранены!")    
            }   
            else
            {
                errconsole.addClass("errmessage");
                errconsole.html("В процессе сохранения возникли ошибки!");        
            } 
        }, "json"
    )    
    
}

function DataShow(result, type)
{
    // добавляем данные в поле faqConsole
        $(".faqConsole").empty();
        var out = "";
        var mType = ''; // тип собщения
                        
        out = out + '<table id="table_mess">';
        
        out = out + '<tr>';
        out = out +   '<th>№ п.п.</th>';
        out = out +   '<th>Дата вопроса</th>';
        out = out +   '<th>Дата ответа</th>';
        out = out +   '<th>Автор</th>';                 
        out = out +   '<th>Тема</th>';
        out = out +   '<th>Вопрос</th>';                
        out = out +  '</tr>';             
                                        
        // формируем строки
        for (i = 0; i < result.length; i++)
        {   
                     
            // выводим, как обычное
            if (i % 2 == 0)
                out = out + '<tr class="alt" faq_id="' + result[i].ans_id + '">';
            else
                out = out + '<tr faq_id="' + result[i].ans_id + '">';
           
            out = out + '<td style="text-align:center; width: 70px;">' + (i + 1) + '</td>';
            out = out + '<td style="text-align:center; width: 120px;">' + result[i].ans_dask + '</span></td>';
            if (result[i].ans_danswer == -1)
            {
                var c = '<img src="/img/question_ico.png"></img>';
            }
            else
            {
                var c = '<span>' + result[i].ans_danswer + '</span>';
            }
            out = out + '<td style="text-align:center; width: 120px;">' + c +  '</td>';
        
            FromU = result[i].user_fname + " " + result[i].user_n + ". " + result[i].user_l + ".";
            
            out = out + '<td style="text-align:center; valign: middle;"><img src="/img/user_ico.png" style="padding-right: 10px;"></img>' + FromU + '</td>';
            out = out + '<td style="text-align:left;">' + result[i].ans_theme + '</td>';   
            out = out + '<td style="text-align:left;">' + result[i].ans_ask + '</td>';                   
                
            out = out + '</tr>';           
        } 
        
        out = out + '</table>'; 
        $(".faqConsole").append(out);
}
