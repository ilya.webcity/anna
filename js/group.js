﻿$(document).ready(function(){
	$('.del').click(function(){
		alert('Del!');
		tr_user = $(this).parent().parent();
		$.post('/group/delete/', {id: tr_user.attr('id_itm')}, function(data) {
			alert('Группа номер ' + tr_user.attr('id_itm') + ' удалена');
			//tr_user.hide();
			$.get('/group/getlist/', function(data) {
				$('#items_div').html(data);
			});
		});
	});
	
	$('.item_table tr td').live('click', function(){
		//alert('edit');
		tr_user = $(this).parent();
		$.getJSON('/group/get/', {id: tr_user.attr('id_itm')}, function(data) {
			//alert('Пользователь номер ' + tr_user.attr('id_usr') + ' удален');
			//tr_user.hide();;
			//alert(data.group_id);
			$('#tr_id td:last').html(data.group_id);
			$('#tr_group_cdate td:last').html(data.group_cdate);
			$('select[name="group_specialty"]').val(data.group_specialty);
			$('input[name="group_code"]').val(data.group_code);
			$('input[name="group_cstudents"]').val(data.group_cstudents);
		});
	});
	
	$('input[name="user_add"]').click(function () {
		
		$('#tr_id td:last').html('');
		$('#tr_group_cdate td:last').html('');
		$('input[name="group_specialty"]').val('');
		$('input[name="group_code"]').val('');
		$('input[name="group_cstudents"]').val('');
	});
	
	$('input[name="user_edit"]').click(function () {
		
		id = $('#tr_id td:last').html();
		if (id == null)
			id = 0;
			//alert(id);
		group_code = $('input[name="group_code"]').val();
		if (!group_code)
		{
			alert('Введите номер группы');
			exit();
		}
		group_specialty = $('select[name="group_specialty"]').val();
		group_cstudents = $('input[name="group_cstudents"]').val();
		if (!group_cstudents)
		{
			alert('Введите колличество студентов');
			exit();
		}
		$.get('/group/add/', 
			{
				group_id: id, 
				group_code: group_code, 
				group_specialty:group_specialty, 
				group_cstudents:group_cstudents 
			}, 
			function(data) {
				if (id)
					alert('Группа номер ' + group_code + ' изменена');
				else
					alert('Группа создана');
				//alert(data);	
				//tr_user.hide();
				$.get('/group/getlist/', function(data) {
					$('#items_div').html(data);
				});
				$('input[name="user_add"]').click();
		});
		
	});
	
	$('input[name="user_del"]').click(function () {
		tr_item = $('#tr_id td:last').html();;
		if (!tr_item)
		{
			alert('Нельзя удалить несущствующую группу');
			exit();
		}
		alert('Del!');
		$.post('/group/delete/', {id: tr_item}, function(data) {
			alert('Группа номер ' + tr_item + ' удалена');
			//tr_user.hide();
				$.get('/group/getlist/', function(data) {
					$('#items_div').html(data);
				});
			$('input[name="user_add"]').click();
		});
	});
	
});

		