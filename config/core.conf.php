<?php

// -- Главный путь к файлам
define('MAIN_DIR', getEnv('DOCUMENT_ROOT'));

// -- Путь к ядру
define('CORE_DIR', MAIN_DIR . '/core');

// -- Путь к модулям
define('MODULES_DIR', MAIN_DIR . '/modules');

// -- Модуль и действие по умолчанию
define('DEFAULT_MODULE', 'id');
define('DEFAULT_ACTION', 'index');

?>