<?php

error_reporting(E_ALL ^ E_NOTICE);

require './core/func.php';
require './core/core.php';
require './core/json.php';

require './config/core.conf.php';
require './config/db.conf.php';

$core = new Core;

try {

	$core->Run();

} catch(Exception $e) {

    echo 'Выброшено исключение: ',  $e->getMessage(), "\n";

}

?>