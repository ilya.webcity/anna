<?php

class model_download extends model {     

    public function upload() {
        
        # Формируем путь
        $uploaddir = MAIN_DIR . '/attachments';
        $uploaddir = $this->makeDirs($uploaddir);
        
        #Генерирует случайное имя файла
        $fileName = md5(time() . '-' . mt_rand(1000, 9999));
        
        #Вытягиваем размер файла
        (double)$size = $_FILES['uploadedfile']['size'] / 1024.0;
        
        #Имя файла (как у пользователя)
        $fileTitle = $_FILES['uploadedfile']['name'];
        
        #Дата файла
        $fileDate = date('Y-m-d');
        
        #Формируем массив данных о файле для добавления в БД
        $dataA = array(
            array (
            'title'     => $fileTitle, 
            'file'      => $fileTitle, 
            'date'      => $fileDate, 
            'size'      => $size . 'кб', 
            'source'    => $fileName
            )                   
        );
               
        $uploadfile = $uploaddir .'/'. $fileName;
        copy($_FILES['uploadedfile']['tmp_name'], $uploadfile);
        
        return $dataA;
        
    }
    
    public function download($name, $date, $source) {
        
        # формируем путь
        $date = str_replace("-", "/", $date);
        
        $source_f = MAIN_DIR . "/attachments/{$date}/{$source}";

        # ????????? ?? ????????????? ? ???????? ???? ????
        if(!is_file($source_f)) return false;

        # ???????? ?????? ?????
        $size = filesize($source_f);

        # ??????? ????????, ????? ??????? ???????? ??? ????????? ? ?????? ????? $name (?????? ?? ?????????? ????? ? ????????)
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . $name . '"; size="' . $size . '"');
        //print($source_f);
        
        # ??????? ?????????? ????? ? ???????
        readfile($source_f);

        # ???? ?????? )))
        exit;
        
    }
    
    public function makeDirs($uploaddir)
    {
        
        # ?????????? ??????????? ??? ??????? ????
        $path1 = date('Y'); // 2011
        $uploaddir = $uploaddir . '/'. $path1;
        
        if (!is_dir($uploaddir))
        {
            // ??????? ????? ? ?????
            mkdir($uploaddir, 0777);
        } 
        
        $path2 = date('m'); // 01
        $uploaddir = $uploaddir . '/' . $path2;
        if (!is_dir($uploaddir))
        {
            // ??????? ????? c ???????
            mkdir($uploaddir);
        }
        
        $path3 = date('d'); // 01
        $uploaddir = $uploaddir . '/' . $path3;
        if (!is_dir($uploaddir))
        {
            // ??????? ????? c ????
            mkdir($uploaddir);
        }
        
        return $uploaddir;        
        
    }
      
    
}

?>