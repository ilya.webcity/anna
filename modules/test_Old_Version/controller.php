﻿<?php

class controller_test extends controller {

	public function action_list() {

		$model = LoadModel('test');
        
        $this->view = 'test/show.tpl';
		
		$logged = $this->core->global['auth']->logged;
        
		$this->data['title'] = "Тесты";
		
        if ($logged) 
        {
            $this->data['err'] = $this->req['err'];
            $this->data['sendok'] = $this->req['sendok'];
            
            $this->data['test'] = $model->get_test_list();
            //$this->data['teachers']	    = $model->get_teachers_list(); 
            //$this->data['user']         = $this->core->global['auth']->user;
        }

		$this->render();   

	}
	public function action_asklist() {

		$model = LoadModel('test');
        
        $this->view = 'test/showask.tpl';
		
		$logged = $this->core->global['auth']->logged;
        
		$this->data['title'] = "Вопросы";
		
        if ($logged) 
        {
            $this->data['err'] = $this->req['err'];
            $this->data['sendok'] = $this->req['sendok'];
            
            $this->data['test'] = $model->get_test_list();
            $this->data['ask'] = $model->get_ask_list();
            //$this->data['teachers']	    = $model->get_teachers_list(); 
            //$this->data['user']         = $this->core->global['auth']->user;
        }

		$this->render();   

	}
	
	public function action_delete() {

		$model = LoadModel('test');
		$del = $model->del_test($_POST['id']);
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		print $_POST['id'];
		die();
	}
	
	public function action_deleteask() {

		$model = LoadModel('test');
		$del = $model->del_ask($_POST['id']);
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		print $_POST['id'];
		die();
	}
	
	public function action_get() {

		$model = LoadModel('test');
		//	$_GET['id'] = 1;
		$usr = $model->get_test($_GET['id']);
		$usr = $usr[0];
		
		$model_u =  LoadModel('user');
		$value1 = $model_u->get_user($usr['test_author']);
		
		if(!empty($value1))
			$usr['test_author'] = $value1[0]['user_name'];
		echo json_encode($usr);
		die();
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		//print_r($usr);
		print "{\r\n";
		foreach ($usr as $key => $value)
			print "	\"".$key."\": \"".$value."\",\r\n";
		
		print "	\"error\": \"0\"\r\n";
/*		print "	\"user_type\": \"".$usr['user_type']."\",\r\n";
		print "	\"user_name\": \"".$usr['user_name']."\",\r\n";
		print "	\"user_fname\": \"".$usr['user_fname']."\",\r\n";
		print "	\"user_lname\": \"".$usr['user_lname']."\",\r\n";
		print "	\"user_group\": \"".$usr['user_group']."\",\r\n";
		print "	\"user_number\": \"".$usr['user_number']."\",\r\n";
		print "	\"user_dregistration\": \"".$usr['user_dregistration']."\",\r\n";
		print "	\"user_ldate\": \"".$usr['user_ldate']."\"\r\n";*/
		print "}\r\n";
		die();
	}
	
	public function action_getask() {

		$model = LoadModel('test');
		//$_GET['id'] = 1;
		$usr = $model->get_ask($_GET['id']);
		$usr = $usr[0];
		
		echo json_encode($usr);
		die();
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		//print_r($usr);
		print "{\r\n";
		foreach ($usr as $key => $value)
			print "	\"".$key."\": \"".$value."\",\r\n";
		
		print "	\"error\": \"0\"\r\n";
/*		print "	\"user_type\": \"".$usr['user_type']."\",\r\n";
		print "	\"user_name\": \"".$usr['user_name']."\",\r\n";
		print "	\"user_fname\": \"".$usr['user_fname']."\",\r\n";
		print "	\"user_lname\": \"".$usr['user_lname']."\",\r\n";
		print "	\"user_group\": \"".$usr['user_group']."\",\r\n";
		print "	\"user_number\": \"".$usr['user_number']."\",\r\n";
		print "	\"user_dregistration\": \"".$usr['user_dregistration']."\",\r\n";
		print "	\"user_ldate\": \"".$usr['user_ldate']."\"\r\n";*/
		print "}\r\n";
		die();
	}
	
	public function action_add() {

		$model = LoadModel('test');
		print $_GET['test_id'].'--<br>';
		$iferror = false;
		$item=array();
		print '{';
		print '"error": {';
		$item['test_name'] = $_GET['test_name'];
		$item['test_about'] = $_GET['test_about'];
		$item['test_ctrue'] = $_GET['test_ctrue'];
		$item['test_cask'] = $_GET['test_cask'];
		$item['test_cdate'] = date('Y-m-d');
		$item['test_author'] = empty($this->core->global['auth']->user['user_id'])?0:$this->core->global['auth']->user['user_id'];
		
		
		
		if (empty($_GET['test_id']))
		{
			/*if ($model->valid_num($_GET['group_code']) > 0)
			{
				print '"num": "1"';
				$iferror = true;
			}
			else
				print '"num": "0"';
			*/	
		}
		else
		{
			$item['test_id'] = $_GET['test_id'];
		}
		
		print '}';
		print '}';
		//print_r($item);
		
		if (!$iferror)
		{
			if (empty($_GET['test_id']))
			{
				$model->set_test($item);
				print 'ok';
			}
			else
			{
				$model->edit_test($item);
			}
		}
		//if (empty($_POST['id']))
			//print 'add';
		//$del = $model->del_user($_POST['id']);
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		//print $_POST['id'];
		die();
	}
	
	public function action_addask() {

		$model = LoadModel('test');
		print $_GET['test_id'].'--<br>';
		$iferror = false;
		$item=array();
		print '{';
		print '"error": {';
		$item['testask_text'] = $_GET['testask_text'];
		$item['testask_test'] = empty($_GET['testask_test'])?1:$_GET['testask_test'];
		$item['testask_type'] = empty($_GET['testask_type'])?1:$_GET['testask_type'];
		
		if (empty($_GET['testask_id']))
		{
			/*if ($model->valid_num($_GET['group_code']) > 0)
			{
				print '"num": "1"';
				$iferror = true;
			}
			else
				print '"num": "0"';
			*/	
		}
		else
		{
			$item['testask_id'] = $_GET['testask_id'];
		}
		
		print '}';
		print '}';
		//print_r($item);
		
		if (!$iferror)
		{
			if (empty($_GET['testask_id']))
			{
				$model->set_ask($item);
				print 'ok';
			}
			else
			{
				$model->edit_ask($item);
			}
		}
		//if (empty($_POST['id']))
			//print 'add';
		//$del = $model->del_user($_POST['id']);
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		//print $_POST['id'];
		die();
	}
	
	public function action_getlistask() {

		$model = LoadModel('test');
		//$_POST['id'] = 2;
		$items = $model->get_ask_list();
		//$usr = $usr[0];
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		//print_r($usr);
		print "<table class='item_table'>
		<tr>
			<th>№</th>
			<th>Тест</th>
			<th>Вопрос</th>
			<th>Действия</th>
		</tr>";
		foreach ($items as $item)
		{
		$item['testask_test'] = $model->get_test($item['testask_test']);
		$item['testask_test'] = $item['testask_test'][0]['test_name'];
			print "			
		<tr id_itm='".$item['testask_id']."'>
			<td>".$item['testask_id']."</td>
			<td>".$item['testask_test']."</td>
			<td>".$item['testask_text']."</td>
			<td><span class='del' >Удалить</span></td>
		</tr>";
		}
		print "</table>";
		die();
	}
	
	public function action_getlist() {

		$model = LoadModel('test');
		//$_POST['id'] = 2;
		$items = $model->get_test_list();
		//$usr = $usr[0];
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		//print_r($usr);
		print "<table class='item_table'>
		<tr>
			<th>№</th>
			<th>Название</th>
			<th>Действия</th>
		</tr>";
		foreach ($items as $item)
		{
			print "			
		<tr id_itm='".$item['test_id']."'>
			<td>".$item['test_id']."</td>
			<td>".$item['test_name']."</td>
			<td><span class='del' >Удалить</span></td>
		</tr>";
		}
		print "</table>";
		die();
	}




}


?>