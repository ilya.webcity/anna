Hello world! The time is {$time|date_format:"%d.%m.%Y, %H:%M:%S"} ({$time})

<h2>Requested params</h2>
<pre>
{$req|print_r:1}
</pre>

<h2>Tables in DB</h2>
<pre>
{$tables|print_r:1}
</pre>


<h2>News</h2>
{foreach $news as $n}
<p>{$n.date} - <a href="/test/readnews/{$n.id}/{$n.date}/">{$n.title}</a></p>
{foreachelse}
<p>Нет новостей</p>
{/foreach}