{if $logged}
{if $err}<div class="errmessage">{$err}</div>{/if} 
{if $sendok}<div class="okmessage">{$sendok}</div>{/if}
<script type="text/javascript" src="/js/ask_list.js"></script>

<div id='items_div'>
<table class='item_table'>
		<tr>
			<th>№</th>
			<th>Название</th>
			<th>Действия</th>
		</tr>
</table>
</div>

<table class='item_add' align=center>
	<tr>
		<th align=right>Параметр</th>
		<th align=left>Значение</th>
	</tr>
	<tr id='tr_id'>
		<td align=right>Id</td>
		<td align=left ></td>
	</tr>
	<tr >
		<td align=right>Тест *</td>
		<td align=left>
			<select name='testask_test'>			
				{foreach $test as $item}
				<option value={$item.test_id}>{$item.test_name}</option>
				{/foreach}
			</select>
		</td>
	</tr>
	<tr id='tr_testask_text'>
		<td align=right>Вопрос *</td>
		<td align=left>
			<textarea name='testask_text' style='width:200px; height:50px'></textarea>
		</td>
	</tr>
	<tr >
		<td align=right>Тип *</td>
		<td align=left>
			<select name='testask_type'>
				<option value=1>Закрытый вопрос</option>
				<option value=2>Открытый вопрос</option>
				<option value=3>Сопоставить варианты</option>
			</select>
		</td>
	</tr>
	<tr >
		<td align=right>Ответы </td>
		<td align=left id='answer_list'>
			
		</td>
	</tr>
	
	<tr id='tr_usr_subit'>
		<td align=right><input name='user_add' value='Новый' type='submit' size=3></td>
		<td align=left><input name='user_edit' value='Сохранить' type='submit' size=3><input name='user_del' value='Удалить' type='submit' size=3></td>
	</tr>
	
</table>

<div class="clear"></div>
</div>


{else}
    <p class="NotAuthorised">Вы не авторизованы!!!</p>
{/if}
