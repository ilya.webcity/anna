<?php

class model_ann extends model {

	public function get_user_info() {         
        
        return $this->core->global['auth']->user;   
        
    }
    
    public function get_ann_item($id) { 
        
        $uinfo = $this->get_user_info();
        $ann = $this->db->pick('SELECT `ann_title`, `ann_cdate`, `user_name`, `user_fname`, `user_lname`, `ann_text` FROM `announcement` JOIN  `user` ON ann_user = user_id WHERE `ann_id` = ' . (int)$id .' AND FIND_IN_SET("' . $uinfo['user_type'] . '", `ann_type`)');
        if ($uinfo['user_type'] == 'student')
        {
            $r = $this->db->pick('SELECT * FROM `rights_announ` WHERE `ra_announ` = ' . (int)$id . ' AND `ra_group` = ' . (int)$uinfo['user_group']);
            if (!$r) return false;  
        }        
        
        $ann['ann_cdate'] =  GetFormatedDate($ann['ann_cdate']);       
        
        return $ann;        

	}
    
    public function get_ann_board() {
        
        $anns = array();
        $uinfo = $this->get_user_info();
        
        if (($uinfo['user_type'] == 'guest') || ($uinfo['user_type'] == 'admin'))
        {               
            $string = '';
            $isJOIN = '';
        } 
        else  
        {
            $string = ' `ra_group` = ' . $uinfo['user_group'] . ' AND'; 
            $isJOIN = 'JOIN `rights_announ` ON a.ann_id = ra_announ ';  
        }
    
        $anns = $this->db->query('SELECT `ann_id`, `ann_cdate`, `ann_edate`, `ann_title`, `ann_text` FROM `announcement` AS a '. $isJOIN .'WHERE' . $string . ' FIND_IN_SET("' . $uinfo['user_type'] . '", `ann_type`) ORDER BY `ann_cdate` ASC');
        if ($anns)
        {
            foreach ($anns as &$v)
            {
                $v['ann_cdate'] = GetFormatedDate($v['ann_cdate']);
            
            }
        }
    
        return $anns;
        
    }
    
    public function getallanns() {
        
        return $this->db->query('SELECT `ann_id`, `ann_title` FROM `announcement`');
        
    }
    
    public function redaction($ann_id) {
        
        $res = $this->db->query('SELECT * FROM `announcement` WHERE `ann_id` = ' . (int)$ann_id . ' LIMIT 1');
        $res = $res[0];
        
        $res['closeanns'] = $this->db->query('SELECT `ann_id`, `ann_title` FROM `announcement` WHERE `ann_id` != ' . (int)$ann_id);
        $res['availableAuthors'] = $this->getAllAuthors();
        return $res;
    }     

    public function saveAnn($inputAr, $ann_id) {
        
        return $this->db->Update('announcement', $inputAr, 'ann_id', $ann_id);
        
    }
    
    public function addAnn($inputAr) {
        
        return $this->db->Insert('announcement', $inputAr);
        
    }
    
    public function getAllAuthors() {
    
        return $this->db->query('SELECT `user_id` AS `ann_user`, `user_name`, `user_lname`, `user_fname` FROM `user` WHERE `user_type` = "admin"');
                
    }
    
    public function deleteAnn($ann_id) {
        return $this->db->query('DELETE FROM `announcement` WHERE `ann_id` = '. $ann_id);
    }
}

?>