{* Админская часть - редактирование объявления *}
    <link type="text/css" href="/css/datapicker/jquery-ui-1.8.18.custom.css" rel="stylesheet" /> 
    <script type="text/javascript" src="/js/ann/script.js"></script>
    
    {* ДатаПикер *}
    <script src="/js/datapicker/jquery-ui-1.8.18.custom.min.js" type="text/javascript"></script>
    <script src="/js/datapicker/jquery.ui.datepicker-ru.js" type="text/javascript"></script>
    
    {* Плавающее меню *}
    <script type="text/javascript" src="/js/docmenufloater/jquery.hoverintent.minified.js"></script>
    <script type="text/javascript" src="/js/docmenufloater/jquery.floater.1.2.js"></script>
    
    {* ВИЗУАЛЬНЫЙ РЕДАКТОР *}
    <script type="text/javascript" src="/js/tiny_mce/jquery.tinymce.js"></script>
	<script src="/js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php" type="text/javascript"></script>
            
{* /Админская часть - редактирование объявления *}
<div id = "errmessage"></div>

<p>
    <label>&nbsp;</label>
    
    {if $ann.ann_id}
        <input type="button" class="btn" value="Сохранить" onClick="saveAnn();" />        
        <input type="button" class="btn" value="Удалить" onClick="delAnn();" />
    {else}
        <input type="button" class="btn" value="Создать" onClick="newAnn();" />
        <input type="button" class="btn" value="Назад" onClick="javascript:history.back();"/> 
    {/if}
</p>
<br/>
<hr>
<br/> 
<div class="wrapper">          
    <form id="form" class="blocks"> 
    <input type="hidden" id="ann_id" name="ann_id" value="{$ann.ann_id}"/>      
                
        <p>
            <label>Объявление для:</label>            
            <select name="ann_type" class="select" id="ann_type" style="width: 220px;">
                             
                <option value="admin" class="line" {if $ann.ann_type == 'admin'}selected{/if}>Администраторов</option>
                <option value="student" class="line" {if $ann.ann_type == 'student'}selected{/if}>Студентов</option>
                <option value="guest" class="line" {if $ann.ann_type == 'guest'}selected{/if}>Гостей</option>
            
            </select>
        </p>
        <p>
            <label>Заголовок *:</label>
            <input type="text" class="text" name="ann_title" id="ann_title" value="{$ann.ann_title}" style="width: 600px;"/>
        </p>
        <p>            
            <textarea name="ann_text" id="ann_text" style="height: 350px;">{$ann.ann_text}</textarea>
        </p>
        <p>
            <label>Автор:</label>            
            <select name="ann_user" class="select" id="ann_user">
                {foreach $ann.availableAuthors as $author}
                    <option value="{$author.ann_user}" class="line" {if $author.ann_user == $ann.ann_user}selected{/if}>{$author.user_fname} {$author.user_name|truncate:1:'':true}. {$author.user_lname|truncate:1:'':true}.</option>
                {/foreach}
            </select>
        </p>
        <p>
            <label>Дата создания *:</label>
            <input type="text" class="text" name="ann_cdate" id="ann_cdate" value="{$ann.ann_cdate}" style="width: 90px;"/>
        </p>
        <p>
            <label>Дата окончания *:</label>
            <input type="text" class="text" name="ann_edate" id="ann_edate" value="{$ann.ann_edate}" style="width: 90px;"/>
        </p>
        </div>          
        <br/>
        <div id = "errmessage2"></div>
        <hr>
        <p>
            <label>&nbsp;</label>     
            {if $ann.ann_id}
                <input type="button" class="btn" value="Сохранить" onClick="saveAnn();" />        
                <input type="button" class="btn" value="Удалить" onClick="delAnn();" />
            {else}
                <input type="button" class="btn" value="Создать" onClick="newAnn();" />
                <input type="button" class="btn" value="Назад" onClick="javascript:history.back();"/> 
            {/if}
        </p>
    </form>    

    {if $ann.closeanns}
    {* Дополнительное меню. Отображает страницы раздела *} 
        <div id="chaptermenu">
            <ul class="nav-main">
                {foreach $ann.closeanns as $p}
                    <li>
                        <a href="/ann/redaction/{$p.ann_id}" title="{$p.ann_title}">
                            <span>                                
                                {$p.ann_title|truncate:20:'..':true}                                    
                            </span>
                        </a>
                    </li>
                {/foreach}
                    <li><hr></li>
                    <li><a href="/ann/admin"><span><b><u>Панель</u></b></span></a></li>
            </ul>
            <div class="dc-corner"><span></span></div>
        </div>
    {*/ Дополнительное меню *}
    {/if}

<div class="clear"></div>
</div>
