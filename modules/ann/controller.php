<?php

class controller_ann extends controller {

    public $access = array(

        '_default' => array('student' => true, 'admin' => true),
        'getallanns' => array('admin' => true)        

    );
    
    public function action_index() {
        
        $this->redirect('/');
    
    } 

    public function action_read() {

        $news = LoadModel('ann');

        $ann_item = $news->get_ann_item($this->path[0]);
        if(!$ann_item) Error_404();

        $this->data['title'] = $ann_item['ann_title'];
        $this->data['title_page'] = 'Объявление: ' . $this->data['title']; 
        $this->data['ann_item'] = $ann_item;
        $this->view = 'ann/read.tpl';

        $this->render();
    }
    
    # АДМИНСКИЕ
    
    # Получить список всех объявления
    public function action_admin() {
     
        $model = LoadModel('ann');
        $this->data['title'] = 'Редактирование объявлений';
        $this->data['title_page'] = 'Редактирование объявлений';
        
        $this->view = 'ann/admin.tpl';
        $this->data['annsList'] = $model->getallanns();
        
        $this->render(); 
        
    }
    
    # Открыть объявление на редактирование
    public function action_redaction() {
            
        $model = LoadModel('ann');
        $this->data['title'] = 'Редактирование объявления';
        $this->data['title_page'] = 'Редактирование объявления';
        
        $this->view = 'ann/redaction.tpl';
        $this->data['ann'] = $model->redaction((int)$this->path[0]);
        
        $this->render();        
    }
    
    # Добавление объявления
    public function action_add() {
            
        $model = LoadModel('ann');
        $this->data['title'] = 'Создание нового объявления';
        $this->data['title_page'] = 'Создание нового объявления';
        
        $this->view = 'ann/redaction.tpl';
        $ann = array(
            'availableAuthors' => $model->getAllAuthors()
        );
        $this->data['ann'] = $ann;
        
        $this->render();        
    } 
    
     
    
    public function action_saveAnn() {
        
        $model = LoadModel('ann');
        $inputArr = array(
            'ann_title' => $this->req['ann_title'],
            'ann_text' => $this->req['ann_text'],
            'ann_user' => (int)$this->req['ann_user'],
            'ann_cdate' => $this->req['ann_cdate'],
            'ann_edate' => $this->req['ann_edate'],
            'ann_type' => $this->req['ann_type'] 
        );
        
        return $model->saveAnn($inputArr, (int)$this->req['ann_id']);
    }
    
    public function action_deleteAnn() {
        
        $model = LoadModel('ann');
        return $model->deleteAnn((int)$this->req['ann_id']);
        
    }
    
    public function action_addAnn() {
        
        $model = LoadModel('ann');
        $inputArr = array(
            'ann_title' => $this->req['ann_title'],
            'ann_text' => $this->req['ann_text'],
            'ann_user' => (int)$this->req['ann_user'],
            'ann_cdate' => $this->req['ann_cdate'],
            'ann_edate' => $this->req['ann_edate'],
            'ann_type' => $this->req['ann_type'] 
        );
        
        return $model->addAnn($inputArr);
    }                 
    
}                       

?>