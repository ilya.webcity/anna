<?php

class controller_mess extends controller {

    
    public function action_getTeachersList() {
    
    $logged = $this->core->global['auth']->logged;     
        
        if ($logged) 
        {
            $model = LoadModel('mess');
            $model->GetTeachersList();                               
            
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }       
        
    }   
    
    public function action_index() {    
        
        $logged = $this->core->global['auth']->logged;     
        
        if ($logged) 
        {
            $model = LoadModel('mess');
                    
            $this->view = 'mess/mess.tpl';
            $this->data['title'] = 'Мои сообщения';                     
            $this->data['title_page'] = 'Мои сообщения';
            
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }

        $this->render();   

    }
    
    public function action_Inbox() {
        
        $logged = $this->core->global['auth']->logged;     
        
        if ($logged) 
        {
            $model = LoadModel('mess');
            $model->GetMail('inbox');                                 
            
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
            $this->render();
        }                        
              
    }
    
    public function action_SendingDone() {
        
        $logged = $this->core->global['auth']->logged;     
        
        if ($logged) 
        {
            $this->view = "mess/mess.tpl";
            $this->data['mailstatusok'] = "Сообщение успешно отправлено"; 
            
            $this->render();                                            
            
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
            $this->render();
        }                        
              
    }
   
    public function action_Outbox() {
        
        $logged = $this->core->global['auth']->logged;     
        
        if ($logged) 
        {
            $model = LoadModel('mess');
            $model->GetMail('outbox');                                
            
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
            $this->render();
        }                        
              
    }

    public function action_Mail() {
        
        $logged = $this->core->global['auth']->logged;     
        
        if ($logged) 
        {
            $modelMess = LoadModel('mess');
            $modelDown = LoadModel('download');            
            
            echo "<pre>";
            print_r($_POST);
            echo "</pre>";
            
            $FromUserId = $this->core->global['auth']->user['user_id'];
            $ToUserId = $this->req['userid'];
            $MessDate = date('Y-m-d H:i:s');
            $Theme  = htmlspecialchars($this->req['theme']);
            $MailText  = htmlspecialchars($this->req['message']);
            $attach_str = "";
            
           
            # Если были присоединенные файлы, закачиваем их и добавляем инфу в сообщение                        
            if (($_FILES['uploadedfile']['name'] != '') && ($_FILES['uploadedfile']['size'] != 0))  {
                
                $attach_ar = $modelDown->upload();
                
                # Сериализуем массив аттача
                $attach_str = serialize($attach_ar);                
            }                                                   
            
            #Массив данных о письме
            $mailData = array (
                    "fuser"     => $FromUserId,
                    "tuser"     => $ToUserId,
                    "mess_date" => $MessDate,
                    "theme"     => $Theme,
                    "text"      => $MailText,
                    "attach"     => $attach_str
            );
             
            
            $modelMess->SendMail($mailData);
                        
            #Говорим юзеру, что сообщение успешно отправлено
            $this->redirect('/mess/sendingdone');                        
                                        
            
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
            $this->render();
        }                        
              
    }
    
    public function action_NewMailCount() {
        
        $logged = $this->core->global['auth']->logged;     
        
        if ($logged) 
        {
            $model = LoadModel('mess');
            $model->GetNewMailCount();                                 
            
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
            $this->render();
        }                        
              
    }
    
    public function action_AddAttach() {
        $model = LoadModel('mess');
        $model->AddAttach();    
    }
    
    public function action_ReadMessage() {
    //-- Читаем сообщение
        
        $logged = $this->core->global['auth']->logged;     
        
        if ($logged) 
        {
            $model = LoadModel('mess');
            $mess_id = htmlspecialchars($this->req['id']);
            $mess_type = htmlspecialchars($this->req['type']);
            
            $model->ReadMessage($mess_id, $mess_type);                                 
            
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
            $this->render();
        }                        
              
    }
    
}


?>