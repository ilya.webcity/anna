<?php

class model_mess extends model {     

    # УДАЛИТЬ!
    public function AddAttach() {
        
    $attaches = array(
            array('title' => 'Аттач 1', 
                  'file' => 'file1.pdf', 
                  'date' => '2011-03-12', 
                  'size' => '50Kb', 
                  'source' => 'qwerty1'),
            array('title' => 'Аттач 2', 
                  'file' => 'file2.pdf', 
                  'date' => '2011-03-13', 
                  'size' => '60Kb', 
                  'source' => 'qwerty2')
                  );
                                    
    $attaches_str = serialize($attaches);
    
    $data = array ('mess_attachment' => $attaches_str);
    $this->db->update('message', $data, 'mess_id', 3);
    
    $att_str = $this->db->pick('SELECT * FROM `message` WHERE `mess_id` = 1', 'mess_attachment');
    $attr_ar = unserialize($att_str);
    
    echo "<pre>";
    print_r($attr_ar);
    echo "</pre>";           
    } 
    
    # Получает список преподавателей
    public function GetTeachersList() {

        $result = $this->db->query('SELECT `user_id`, CONCAT(`user_fname`," ",SUBSTRING(`user_name`,1,1),". ",SUBSTRING(`user_lname`,1,1),".") AS `user_fio` FROM `user` WHERE `user_type` = "admin"');

        echo json_encode($result);        
    }
    
    # Отправляет письмо
    public function SendMail($mailData) {    
       
        $data = array(
                    'mess_unreaded'     => 1,                      
                    'mess_fuser'        => $mailData['fuser'],
                    'mess_tuser'        => $mailData['tuser'],
                    'mess_date'         => $mailData['mess_date'], 
                    'mess_title'        => $mailData['theme'], 
                    'mess_text'         => $mailData['text'], 
                    'mess_attachment'   => $mailData['attach']       
        );
    
        # Добавляем сообщение в БД
        $this->db->insert('message', $data);        
        
    }   
    
    # Получает количество новых сообщений  
    public function GetNewMailCount() {
        
        $user_id = $this->core->global['auth']->user['user_id'];  
       
        $num = $this->db->pick('SELECT COUNT(*) AS `count` FROM `message` WHERE `mess_tuser` = ' . (int)$user_id . ' AND `mess_unreaded` = 1', 'count');
        
        $result['count'] = $num;
               
        echo json_encode($result);   
        
    }  
    
    # Читает конкретное сообщение
    public function ReadMessage($mess_id, $mess_type) {
        //-- прочитать сообщение
        $user_id = $this->core->global['auth']->user['user_id'];        
        
        $query = 'SELECT `mess_date`, `mess_title`, `user_id`, `mess_unreaded`, `mess_text`, `mess_attachment`, `user_fname`, SUBSTRING(`user_name`,1,1) AS `user_n`, SUBSTRING(`user_lname`,1,1) AS `user_l` FROM `message` JOIN `user` ON ';
        
        if ($mess_type == 'inbox')        
        // входящее сообщение        
            $query .= 'mess_fuser = user_id WHERE `mess_id` = ' . (int)$mess_id . ' AND `mess_tuser` = ' . (int)$user_id;
        
        else
        // исходящее сообщение
            $query .= 'mess_tuser = user_id WHERE `mess_id` = ' . (int)$mess_id . ' AND `mess_fuser` = ' . (int)$user_id;
            
        $result = $this->db->query($query);        
        
        if ($result)
        {
            $result = $this->DateTransformer($result);
            // помечаем как прочитанное
            if (($result[0]['mess_unreaded'] == 1) && ($mess_type == 'inbox'))
            {
                $data['mess_unreaded'] = 0;
                $this->db->update('message', $data, 'mess_id', $mess_id);                
            } 
            
            // вытягиваем аттачи
            $result = $this->GetAttach($result);       
        }        
        
        echo json_encode($result);
    }
    
    # Получает список входящих, исходящий сообщений
    public function GetMail($type = 'inbox') {
        //-- получаем вся входящие | исходящие сообщения
        
        $user_id = $this->core->global['auth']->user['user_id']; 

        $query = 'SELECT `mess_id`, `mess_unreaded`, `mess_fuser`, `user_fname`, SUBSTRING(`user_name`,1,1) AS `user_n`, SUBSTRING(`user_lname`,1,1) AS `user_l`, `mess_date`, `mess_title`, `mess_attachment` FROM `message` JOIN `user` ON ';
        
        if ($type == 'inbox')
            $query .= 'mess_fuser = user_id WHERE `mess_tuser` = ' . (int)$user_id . ' ORDER BY `mess_unreaded`, `mess_date` DESC';
        elseif ($type == 'outbox')
            $query .= 'mess_tuser = user_id WHERE `mess_fuser` = ' . (int)$user_id . ' ORDER BY `mess_date` DESC'; 
        
        $result = $this->db->query($query);
              
        if ($result)
        {            
            $result = $this->DateTransformer($result);
            //-- Вытягиваем аттачи
            $result = $this->GetAttach($result);            
        }
        
        echo json_encode($result);       
        
    }  
    
    # Получает аттачи из сериализованного массива
    public function GetAttach($mess_ar) {
        //-- Вытягиваем аттачи
        foreach ($mess_ar as &$v)
        {
            $v['mess_attachment'] = unserialize($v['mess_attachment']);
        }
        
        return $mess_ar;
                    
    }
    
    # Вывод форматированной даты
    public function DateTransformer($result) {
        
        foreach ($result as &$v)
        {
            $v['mess_time'] = GetFormatedDate($v['mess_date'],' ',':','time');
            $v['mess_date'] = GetFormatedDate($v['mess_date']);                   
        }
        
        return $result;        
    }  
    
}

?>