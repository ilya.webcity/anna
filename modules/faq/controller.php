﻿<?php

class controller_faq extends controller {

	public function action_index() {

		$logged = $this->core->global['auth']->logged;

        if ($logged)
        {
            $model = LoadModel('faq');

            $this->view = 'faq/board.tpl';
            $this->data['title'] = "Вопросы преподавателю";
            $this->data['title_page'] = 'Вопросы преподавателю';
            $this->data['err'] = $this->req['err'];
            $this->data['sendok'] = $this->req['sendok'];

            $this->data['questions']    = $model->get_questions_list();
            $this->data['teachers']	    = $model->get_teachers_list();
            $this->data['user']         = $this->core->global['auth']->user;
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }

		$this->render();

	}

	public function action_send() {

        $this->view = 'faq/board.tpl';
        $logged = $this->core->global['auth']->logged;
	    if ($logged)
        {
            $model = LoadModel('faq');

            $fuser = $this->core->global['auth']->user['user_id'];
            $tuser = mysql_escape_string($this->req['teacher']);
            $theme = mysql_escape_string($this->req['theme']);
            $ask   = mysql_escape_string($this->req['message']);

            $r = $model->send($fuser, $tuser, $theme, $ask);

            if ($r)
            {
                $mes = 'Вопрос успешно отправлен на модерацию';
                $this->redirect('/faq?sendok=' .$mes);
            }
            else
            {
                $mes = 'Вопрос не отправлен!';
                $this->redirect('/faq?err=' .$mes);
            }

        }

        $this->render();

	}

    // Администраторские процедуры
    public function action_afaq() {

        $logged = $this->core->global['auth']->logged;

        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('faq');

                $this->view = 'faq/admin_panel.tpl';
                $this->data['title'] = "Вопросы преподавателю";
                $this->data['title_page'] = 'Вопросы преподавателю';

                $this->data['faqstatusok']    = 'OK!';

            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            }

        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }

        $this->render();

    }

    public function action_aCatchNotPublicFaqCount() {

        $logged = $this->core->global['auth']->logged;

	    if($logged) {

            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin') {

                // это админ, все ок
                $model = LoadModel('faq');
                $result = $model->getNotPublicFaqCount();

				json($result);

            } else {

                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';

            }

        } else {

            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';

        }

        $this->render();

    }

    public function action_aGetNotPublicList()
    {

        $logged = $this->core->global['auth']->logged;

        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('faq');
                $result = $model->getList('notpublic');               

                json($result);

            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            }

        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }

        $this->render();

    }
    
    public function action_aGetPublicList()
    {

        $logged = $this->core->global['auth']->logged;

        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('faq');
                $result = $model->getList('public');               

                json($result);

            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            }

        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }

        $this->render();

    }
    
    public function action_OpenFaq()
    {

        $logged = $this->core->global['auth']->logged;

        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('faq');
                $id = htmlspecialchars($this->req['id']);
                
                $result = $model->getOneFaq($id);               
                
                json($result);

            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            }

        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }

        $this->render();

    }

    
    public function action_saveFaq()
    {

        $logged = $this->core->global['auth']->logged;

        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('faq'); 
       
                $inputArray = array(
                    'ans_fuser' => (int)htmlspecialchars($this->req['FromU']),
                    'ans_tuser' => (int)htmlspecialchars($this->req['ToU']),
                    'ans_theme' => htmlspecialchars($this->req['theme']),
                    'ans_ask'   => htmlspecialchars($this->req['ans_ask']),
                    'ans_text'  => htmlspecialchars($this->req['ans_text']),
                    'ans_public' => (int)htmlspecialchars($this->req['ispublic']),
                    'ans_dask'  => htmlspecialchars($this->req['dask']),
                    'ans_danswer' => htmlspecialchars($this->req['danswer'])                
                );
                $id = (int)htmlspecialchars($this->req['id']);
                
                $result = $model->saveFaq($id, $inputArray);               
                
                json($result);                                                                                      
            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            }

        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }

        $this->render();

    }
}


?>