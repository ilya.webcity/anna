<?php

class model_faq extends model {


    public function get_questions_list() {

        $qs = $this->db->query('SELECT `ans_dask`, `ans_ask`, `ans_text`, `user_name`, `user_fname`, `user_lname` FROM `answer`, `user` WHERE `ans_public` = 1 AND `user_id` = `ans_tuser` ORDER BY `ans_dask` DESC');
        if ($qs)
        {
            foreach ($qs as &$v)
            {
                $v['ans_dask'] = GetFormatedDate($v['ans_dask']);
            }
        }

        return $qs;

    }

    public function get_teachers_list() {

        return $this->db->query('SELECT `user_id`, `user_name`, `user_fname`, `user_lname` FROM `user` WHERE `user_type` = "admin"');

    }

    public function send($fuser, $tuser, $theme, $ask)
    {
        $data = array('ans_fuser' => $fuser,
                      'ans_tuser' => $tuser,
                      'ans_theme' => $theme,
                      'ans_ask'   => $ask,
                      'ans_dask'  => date('Y-m-d H:i:s')
        );
        return $this->db->insert('answer', $data);
    }

    // Администраторские ф-ии

    # Количество неопубликованных факов
    public function getNotPublicFaqCount() {
        $num = $this->db->pick('SELECT COUNT(*) AS `count` FROM `answer` WHERE `ans_public` = 0', 'count');
        $result['count'] = (int)$num;
        return $result;
    }

    # Получить список опу / неопуб faq
    public function getList ($type = 'public') {

        if ($type == 'public')
        {
            $public = 1;
        }
        else
        {
            $public = 0;
        }

        $query = 'SELECT `ans_id`, `ans_theme`, SUBSTRING(`ans_ask`, 1, 30) AS `ans_ask`, `ans_dask`, `ans_danswer`, `user_fname`, SUBSTRING(`user_name`,1,1) AS `user_n`, SUBSTRING(`user_lname`,1,1) AS `user_l` FROM `answer` JOIN `user` ON ans_fuser = user_id WHERE `ans_public` = '. $public . ' ORDER BY `ans_dask` DESC';

        $result = $this->db->query($query);

        if ($result)
        {
            $result = $this->DateTransformer($result);
        }

        return $result;

    }
    
    # Открываем конкретное faq
    public function getOneFaq ($faqid) {         

        $query = 'SELECT * FROM `answer` WHERE `ans_id` = '. (int)$faqid;
             
        $result = $this->db->query($query);

        if ($result)
        {   
            $FromUId = (int)$result[0]['ans_fuser'];
            $ToUId = (int)$result[0]['ans_tuser'];
            
            $qFrom = 'SELECT `user_id`, `user_fname`, SUBSTRING(`user_name`,1,1) AS `user_n`, SUBSTRING(`user_lname`,1,1) AS `user_l` FROM `user` WHERE `user_id` = '. (int)$FromUId;
            $qTo = 'SELECT `user_id`, `user_fname`, SUBSTRING(`user_name`,1,1) AS `user_n`, SUBSTRING(`user_lname`,1,1) AS `user_l` FROM `user` WHERE `user_id` = '. (int)$ToUId;
            $resF = $this->db->query($qFrom);
            $resT = $this->db->query($qTo);                      
                       
            $result[0]['ans_fuser'] = (array)$resF[0];
            $result[0]['ans_tuser'] = (array)$resT[0];
            
            # Вытянем весь список юзеров для полей Автор и Кому
            $q = 'SELECT `user_id`, `user_fname`, SUBSTRING(`user_name`,1,1) AS `user_n`, SUBSTRING(`user_lname`,1,1) AS `user_l` FROM `user`';
            $resAll = $this->db->query($q);
            
            $result[0]['allUsers'] = $resAll;
            
        }

        return $result;

    }

    # Сохраняем faq
    public function saveFaq ($id, $inputArray) {
             
        $result = $this->db->update('answer', $inputArray, 'ans_id', $id);        
        
        return $id;

    }

    
    # Форматер даты
    public function DateTransformer($result) {

        foreach ($result as &$v)
        {
            $v['ans_dasktime'] = GetFormatedDate($v['ans_dask'],' ',':','time'); 
            $v['ans_dask'] = GetFormatedDate($v['ans_dask']); 
            $v['ans_danswertime'] = GetFormatedDate($v['ans_danswer'],' ',':','time');  
            
            if (strtotime ($v['ans_danswer']) <= 0 ) 
            { 
                $v['ans_danswer'] = -1;
            }
            else
            {
                $v['ans_danswer'] = GetFormatedDate($v['ans_danswer']);
            }
            
        }

        return $result;
    }
}

?>