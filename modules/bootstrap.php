<?php

function bootstrap(&$core, &$ctrl) {

	$auth = LoadModel('id');
	$core->sm->assign('logged', $auth->logged);
	$core->sm->assign('user', $auth->user);

	$core->global['auth'] = & $auth;

	$type = & $auth->user['user_type'];
	if(!$type) $type = 'guest';
	$access = & $ctrl->access;

	if(!$access['_default']) $access['_default'] = array('admin' => true, 'student' => true);
	if(!$access[$core->act]) $access[$core->act] = $access['_default'];
	if(!$access[$core->act][$type]) Error_404(); //throw new Exception('В доступе отказано');


    //-- Получаем меню
    $menu = menuloader($core);

    //-- Вытягиваем активные объявления
    $ann = LoadModel('ann');
    $announs = $ann->get_ann_board();

    $core->sm->assign('announcements', $announs);
    $core->sm->assign('menu', $menu);
	$core->sm->assign('common_data', 'Общая инфа');

}

function menuLoader(&$core)
{
    $menu = array();
    $type = $core->global['auth']->user['user_type'];
    if (!$type) $type = 'guest';

    $menu = $core->db->query('SELECT * FROM `menu` WHERE `menu_pid` = 0 AND FIND_IN_SET("' . $type . '", `menu_type`) ORDER BY `menu_number` ASC');
    if($menu) foreach($menu as $k => $v) {
	    $menu[$k]['menu_sub'] = $core->db->query('SELECT * FROM `menu` WHERE `menu_pid` = ' . (int)$v['menu_id'] . ' AND  FIND_IN_SET("' . $type . '", `menu_type`) ORDER BY `menu_number`');
	}

	return $menu;
}

?>