<?php

class model_id extends model {

    public $logged;
    public $user = array();
    public $error = '';

    public function __construct() {
        session_start();
        parent::__construct();
        $this->logged = & $_SESSION['logged'];
        $this->user = & $_SESSION['user'];
    }

    public function login($login, $passwd) {
        if($this->logged) {
            $this->error = 'Вы уже авторизованы';
            return false;
        }
        
        $login = mysql_escape_string($login); 
        
        $data = $this->db->pick('SELECT * FROM `user` WHERE `user_login` = "'. $login .'"');
        
        if ($data == false) 
        {
            $this->error = 'Пользователь не найден!';
            return false;
        }
        else
        {
            $cpasswd = $data['user_passwd'];
            
            $passwd = mysql_real_escape_string($passwd);            
            $passwd = md5($passwd);             
            
            if ($passwd != $cpasswd)  
            {
                $this->error = 'Такое сочетание пользователя и пароля не найдено!';
                return false;   
            }
        }
        
        $this->logged = true; 
        
        // -- Вытягиваем данные о группе, специальности
        $group = $this->db->pick('SELECT `group_code`, `group_specialty`  FROM `group` WHERE `group_id` = "'. $data['user_group'] .'"');
                             
        // -- Регистрируем время входа юзера в БД                    
        $this->db->update('user', array('user_ldate' => date('Y-m-d H:i:s')), 'user_id', $data['user_id']);
                                
        $this->user = array('login' => $login,
                            'user_id' => $data['user_id'], 
                            'user_type' => $data['user_type'],
                            'user_name' => $data['user_name'],
                            'user_fname' => $data['user_fname'],
                            'user_lname' => $data['user_lname'],
                            'user_group' => $data['user_group'],
                            'user_group_code' => $group['group_code'],
                            'user_spec' => $group['group_specialty']        
                            );         
        return true;
    }

    public function logout() {
        if(!$this->logged) {
            $this->error = 'Вы не авторизованы';
            return false;
        }
        $this->logged = false;
        $this->user = array();
        return true;
    }



}

?>