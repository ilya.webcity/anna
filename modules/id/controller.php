<?php

class controller_id extends controller {

	public $access = array(
		'_default' => array('guest' => true, 'admin' => true, 'student' => true)
	);

	public function action_index() {
		$this->action_login();
	}

	public function action_login() {

		$auth = & $this->core->global['auth'];

		if($auth->logged) go('/docs/doc/');



		$this->title = 'Авторизация';
		$this->view = 'id/login.tpl';
		if($_POST) {

			$r = $auth->login($this->req['login'], $this->req['passwd']);

			if($r) {

				$this->redirect('/');

			} else {
				$this->data['err'] = 'Ошибка: ' . $auth->error;
			}

		}

		$this->render();

	}

	public function action_logout() {

		$auth = LoadModel('id');
		$auth->logout();

		$this->redirect('/id/login');

	}


}


?>