{* Админская часть - редактирование конференции *}
    <link type="text/css" href="/css/datapicker/jquery-ui-1.8.18.custom.css" rel="stylesheet" /> 
    <script type="text/javascript" src="/js/conference/admin.js"></script>
    
    {* ДатаПикер *}
    <script src="/js/datapicker/jquery-ui-1.8.18.custom.min.js" type="text/javascript"></script>
    <script src="/js/datapicker/jquery.ui.datepicker-ru.js" type="text/javascript"></script>
    
    {* Плавающее меню *}
    <script type="text/javascript" src="/js/docmenufloater/jquery.hoverintent.minified.js"></script>
    <script type="text/javascript" src="/js/docmenufloater/jquery.floater.1.2.js"></script>
                
{* /Админская часть - редактирование конференции *}
<div id = "errmessage"></div>

<p>
    <label>&nbsp;</label>
    
    {if $conf.cr_id}
        <input type="button" class="btn" value="Сохранить" onClick="saveConf();" />        
        <input type="button" class="btn" value="Удалить" onClick="delConf();" />
    {else}
        <input type="button" class="btn" value="Создать" onClick="newConf();" />
        <input type="button" class="btn" value="Назад" onClick="javascript:history.back();"/> 
    {/if}
</p>
<br/>
<hr>
<br/> 
<div class="wrapper">          
    <form id="form" class="blocks"> 
    <input type="hidden" id="cr_id" name="cr_id" value="{$conf.cr_id}"/>               
        
        <p>
            <label>Заголовок *:</label>
            <input type="text" class="text" name="cr_title" id="cr_title" value="{$conf.cr_title}" style="width: 600px;"/>
        </p>        
        <p>
            <label>Автор:</label>            
            <select name="cr_admin" class="select" id="cr_admin">
                {foreach $conf.availableAuthors as $author}
                    <option value="{$author.user_id}" class="line" {if $author.user_id == $conf.cr_admin}selected{/if}>{$author.user_fname} {$author.user_name|truncate:1:'':true}. {$author.user_lname|truncate:1:'':true}.</option>
                {/foreach}
            </select>
        </p>
        <p>
            <label>Дата создания *:</label>
            <input type="text" class="text" name="cr_cdate" id="cr_cdate" value="{$conf.cr_cdate}" style="width: 90px;"/>
        </p>
        <p>
            <label>Статус:</label>            
            <select name="cr_open" class="select" id="cr_open">                
                <option value="1" class="line" {if $conf.cr_open == 1}selected{/if}>Открыта</option>
                <option value="0" class="line" {if $conf.cr_open == 0}selected{/if}>Закрыта</option>
            </select>
        </p>
        </div>          
        <br/>
        <div id = "errmessage2"></div>
        <hr>
        <p>
            <label>&nbsp;</label>     
            {if $conf.cr_id}
                <input type="button" class="btn" value="Сохранить" onClick="saveConf();" />        
                <input type="button" class="btn" value="Удалить" onClick="delConf();" />
            {else}
                <input type="button" class="btn" value="Создать" onClick="newConf();" />
                <input type="button" class="btn" value="Назад" onClick="javascript:history.back();"/> 
            {/if}
        </p>
    </form>    

    {if $conf.closeconfs}
    {* Дополнительное меню. Отображает страницы раздела *} 
        <div id="chaptermenu">
            <ul class="nav-main">
                {foreach $conf.closeconfs as $p}
                    <li>
                        <a href="/conference/redaction/{$p.cr_id}" title="{$p.cr_title}">
                            <span>                                
                                {$p.cr_title|truncate:20:'..':true}                                    
                            </span>
                        </a>
                    </li>
                {/foreach}
                    <li><hr></li>
                    <li><a href="/conference/admin"><span><b><u>Панель</u></b></span></a></li>
            </ul>
            <div class="dc-corner"><span></span></div>
        </div>
    {*/ Дополнительное меню *}
    {/if}

<div class="clear"></div>
</div>
