<?php

class model_conference extends model {
  
    public function save_user($user, $room_id) {
        
        $user_id = $this->db->pick('SELECT * FROM `conference_user` WHERE `cu_room` = ' . (int)$room_id . ' AND `cu_name` = "' . $user . '"', 'cu_id');
        
        if ($user_id == false)
        {
            $data = array('cu_name' => $user,                      
                      'cu_room' => $room_id                      
            );
        
            $this->db->insert('conference_user', $data);
                 
        } 
        else
        {
            // обновляем время активности юзера
            $this->db->update('conference_user', array('cu_lactivity' => date('Y-m-d H:i:s')), `cu_id`, $user_id);
        }  
                
    }   
    
    public function get_room($room_id)    {
        return $this->db->Pick('SELECT * FROM `conference_room` WHERE `cr_id` = ' . (int)$room_id);
    }
    
    public function get_rooms_list()     {
        $group_id = $this->core->global['auth']->user['user_group'];              
        $r = $this->db->query('SELECT `cr_id`, `cr_title` FROM `conference_room` JOIN `rights_conference` ON cr_id = rc_room WHERE `rc_group` = ' . (int)$group_id . ' AND `cr_open` = 1 ORDER BY `cr_cdate`');
                
        return $r;        
    }
    
    public function send($name, $text, $room_id) { 
        
        $name = substr($name, 0, 200); 
        $name = htmlspecialchars($name); 
        $name = mysql_escape_string($name); 
        
        $text = substr($text, 0, 200); 
        $text = htmlspecialchars($text); 
        $text = mysql_escape_string($text);
     
        $data = array('cl_name' => $name,
                      'cl_text' => $text,
                      'cl_room' => $room_id                     
        );
        
        return $this->db->insert('conference_line', $data);
        
    }
    
    public function load($last_message_id, $room_id) {     
        
        $data = array();
        
        $this->db->query("DELETE FROM `conference_user` WHERE `cu_room` = " . (int)$room_id . " AND `cu_lactivity` < SUBTIME(NOW(),'0 0:10:0')");
        $this->db->query("DELETE FROM `conference_line` WHERE `cl_room` = " . (int)$room_id . " AND `cl_ts` < SUBTIME(NOW(),'0:5:0')");
        
        $lines = $this->db->query('SELECT * FROM `conference_line` WHERE `cl_id` > ' . (int)$last_message_id .' AND `cl_room` = ' . (int)$room_id . ' ORDER BY `cl_id` DESC');    
        $users = $this->db->query('SELECT * FROM `conference_user` WHERE `cu_room` = ' . (int)$room_id . ' ORDER BY `cu_id` ASC');          
        
        $data['users'] = $users; 
        $data['mess'] = array_reverse($lines); 
        
        if (!$lines) {
            $data['last_id'] = $last_message_id;            
        }
        else
        {
            $data['last_id'] = $lines[0]['cl_id'];
            foreach ($data['mess'] as $key => $value)
            {
                $data['mess'][$key]['cl_ts'] = GetFormatedDate($value['cl_ts'],'',':','time');
            }
        }                      
        
        echo json_encode($data);
  
    }

    
    # Админские
    public function getallconfs() {
        
        return $this->db->query('SELECT `cr_id`, `cr_title` FROM `conference_room`');
        
    }
    
    public function redaction($cr_id) {
        
        $res = $this->db->query('SELECT * FROM `conference_room` WHERE `cr_id` = ' . (int)$cr_id . ' LIMIT 1');
        $res = $res[0];
        
        $res['closeconfs'] = $this->db->query('SELECT `cr_id`, `cr_title` FROM `conference_room` WHERE `cr_id` != ' . (int)$cr_id);
        $res['availableAuthors'] = $this->getAllAuthors();
        return $res;
    } 
    
    public function getAllAuthors() {
    
        return $this->db->query('SELECT `user_id`, `user_name`, `user_lname`, `user_fname` FROM `user` WHERE `user_type` = "admin"');
                
    }
    
    public function saveConf($inputAr, $cr_id) {
        
        return $this->db->Update('conference_room', $inputAr, 'cr_id', $cr_id);
        
    }
    
    public function addConf($inputAr) {
        
        return $this->db->Insert('conference_room', $inputAr);
        
    }
    
    public function deleteConf($cr_id) {
        return $this->db->query('DELETE FROM `conference_room` WHERE `cr_id` = '. $cr_id);
    }
}

?>