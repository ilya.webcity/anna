{if $room}
    {* конференция *}
    <script type="text/javascript" src="/js/conference/script.js"></script>  
    {* /конференция *}
    <div class="chatBoard">
        <div class="chat">
            <div id="chat_area"></div>
        </div>
        <div class="userlist">
            {foreach $userlist as $u}
                <span><a href="javascript:void(0)" onClick="sayTo('1234');">12132</a></span>
            {foreachelse}
                <span style="color: #696969;">Никого нет</span>
            {/foreach}
        </div>
        <form id="pac_form" action="">
        <table style="width: 100%;">
                <tr>
                        <td>Имя:</td>
                        <td>Сообщение:</td>
                        <td></td>
                </tr>
                <tr>
                        <!-- Поле ввода имени -->
                        <td><input type="text" id="pac_name" class="r4" value="{$chatuser}" disabled></td>
                        
                        <!-- Поле ввода сообщения -->
                        <td style="width: 75%;"><input type="text" id="pac_text" class="r4" value=""></td>
                        
                        <!-- Кнопка "Отправить" -->
                        <td><input type="submit" value="Отправить"></td>
                </tr>
        </table>
        </form>
    </div>
{else}
    <p>Конференция закрыта либо у Вас нет соответствующих прав доступа!</p>    
{/if}
