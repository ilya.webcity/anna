﻿<?php

class controller_conference extends controller {
    
    public $access = array(

        '_default' => array('student' => true, 'admin' => true),
        'admin' => array('admin' => true)        

    );
    
    public $room = array ();

	public function action_index() {     
        
        $logged = $this->core->global['auth']->logged;
        
        if ($logged) {            
                      
            $model = LoadModel('conference');
            
            $this->view = 'conference/roomslist.tpl';
            $this->data['title'] = 'В данный момент доступны конференции:';
           
            $activeRooms = $model->get_rooms_list();
            if ($activeRooms) 
            {
                $this->data['title'] = 'В данный момент доступны конференции:';  
                $this->data['rooms'] = $activeRooms;
            } else {
                $this->data['title'] = 'В данный момент доступных конференций нет!';
            }
            
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        } 
        
        $this->render();		  

	}
    
    public function action_room() {     
        
        $logged = $this->core->global['auth']->logged;
        
        if ($logged) {
            
            $model = LoadModel('conference');
            
            //-- Получаем данные по конференции
            $room = $model->get_room($this->core->path[0]);
            
            //-- Сохраняем данные о комнате
            session_start();
            $_SESSION['room'] = $room;              
                    
            $this->data['room'] = $room;
            
            if ($room)            
            {                
                // заходим в комнату 
                $user = $this->core->global['auth']->user['login']; 
                $model->save_user($user, $room['cr_id']);
                
                Header("Cache-Control: no-cache, must-revalidate"); 
                Header("Pragma: no-cache");
                
                $this->view = 'conference/chat.tpl';
                $this->data['title'] = $room['cr_title'];
               
                $this->data['chatuser'] = $user;
            }            
            
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        } 
        
        $this->render();          

    }

    public function action_send() {
        
        $model = LoadModel('conference');
        
        session_start();
        $room = & $_SESSION['room'];
        
        $user = $this->core->global['auth']->user['login']; 
        $model->save_user($user, $room['cr_id']);
        
        $model->send($this->req['name'], $this->req['text'], $room['cr_id']);
        
    }

	public function action_load() {
        
        $model = LoadModel('conference');
        
        session_start();
        $room = & $_SESSION['room'];        
        
        $model->load(intval($this->req['last']), $room['cr_id']);    

	}
    
    # Админские
    
    # Получить список всех конферейнций
    public function action_admin() {
     
        $model = LoadModel('conference');
        $this->data['title'] = 'Редактирование конференций';
        $this->data['title_page'] = 'Редактирование конференций';
        
        $this->view = 'conference/admin.tpl';        
        $this->data['confsList'] = $model->getallconfs();
        
        $this->render(); 
        
    }
    
    # Открыть конференцию на редактирование
    public function action_redaction() {
            
        $model = LoadModel('conference');
        $this->data['title'] = 'Редактирование конференции';
        $this->data['title_page'] = 'Редактирование конференции';
        
        $this->view = 'conference/redaction.tpl';
        $this->data['conf'] = $model->redaction((int)$this->path[0]);
        
        $this->render();        
    }
        
   # Добавление конференции
    public function action_add() {
            
        $model = LoadModel('conference');
        $this->data['title'] = 'Создание новой конференции';
        $this->data['title_page'] = 'Создание новой конференции';
        
        $this->view = 'conference/redaction.tpl';
        $conf = array(
            'availableAuthors' => $model->getAllAuthors()
        );
        $this->data['conf'] = $conf;
        
        $this->render();        
    }     
        
    public function action_saveConf() {
        
        $model = LoadModel('conference');
        $inputArr = array(
            'cr_title' => $this->req['cr_title'],
            'cr_open' => $this->req['cr_open'],
            'cr_admin' => (int)$this->req['cr_admin'],
            'cr_cdate' => $this->req['cr_cdate']            
        );
        
        return $model->saveConf($inputArr, (int)$this->req['cr_id']);
    }
    
    public function action_deleteConf() {
        
        $model = LoadModel('conference');
        return $model->deleteConf((int)$this->req['cr_id']);
        
    }
    
    public function action_addAnn() {
        
        $model = LoadModel('conference');
        $inputArr = array(
            'cr_title' => $this->req['cr_title'],
            'cr_open' => $this->req['cr_open'],
            'cr_admin' => (int)$this->req['cr_admin'],
            'cr_cdate' => $this->req['cr_cdate']            
        );
        
        return $model->addConf($inputArr);
    } 
    
}


?>