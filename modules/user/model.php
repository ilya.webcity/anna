<?php

class model_user extends model {
    

	public function get_user_list() {

		return $this->db->query('SELECT * FROM `user` WHERE 1');

	}
	
	public function del_user($id) {

		return $this->db->query('DELETE FROM `user` WHERE user_id = '.$id.';');

	}
	
	public function get_user($id) {

		return $this->db->query('SELECT * FROM `user` WHERE user_id = '.$id.';');

	}
	
	public function valid_login($user_login) {

		return $this->db->query('SELECT COUNT(user_id) FROM `user` WHERE user_login = '.$user_login.';');

	}
	
	public function generate_salt() {

		return 'anna';

	}
	
	public function set_user($user) {
		if (!empty($user))
		return $this->db->query("INSERT INTO  `edubase`.`user` (
			`user_id` ,
			`user_type` ,
			`user_name` ,
			`user_fname` ,
			`user_lname` ,
			`user_login` ,
			`user_passwd` ,
			`user_salt` ,
			`user_group` ,
			`user_dregistration` ,
			`user_number`
		)
		VALUES (	
			NULL ,  '".$user['user_type']."',  '".$user['user_name']."',  '".$user['user_fname']."',  '".$user['user_lname']."',  '".$user['user_login']."',  '".$user['user_passwd']."',  '".$user['user_salt']."',  '".$user['user_group']."',  '".$user['user_dregistration']."',  '".$user['user_number']."'
		);");	
		

	}
	
	public function edit_user($user) {
		if (!empty($user))
		{
		$qw = "UPDATE `user` 
		SET  
			`user_type`  =  '".$user['user_type']."',
			`user_name`  =  '".$user['user_name']."',
			`user_fname`  =  '".$user['user_fname']."',
			`user_lname`  =  '".$user['user_lname']."',
			`user_login`  =  '".$user['user_login']."',";
		if (!empty($user['user_salt']))
			$qw .= "`user_passwd`  =  '".$user['user_passwd']."',
				`user_salt`  =  '".$user['user_salt']."',";
		$qw .= "	`user_group`  =  '".$user['user_group']."',
			`user_number`  =  '".$user['user_number']."'
		
		WHERE  `user`.`user_id` = ".$user['user_id'].";
		";
		return $this->db->query($qw);
		}
		
	}
	
	public function get_questions_list() {

		return $this->db->query('SELECT `ans_dask`, `ans_ask`, `ans_text`, `user_name`, `user_fname`, `user_lname` FROM `answer`, `user` WHERE `ans_public` = 1 AND `user_id` = `ans_tuser` ORDER BY `ans_dask` DESC');

	}

	public function get_teachers_list() {

		return $this->db->query('SELECT `user_id`, `user_name`, `user_fname`, `user_lname` FROM `user` WHERE `user_type` = "admin"');

	}
    
    public function send($fuser, $tuser, $theme, $ask)
    {
        $data = array('ans_fuser' => $fuser,
                      'ans_tuser' => $tuser,
                      'ans_theme' => $theme,
                      'ans_ask'   => $ask,
                      'ans_dask'  => date('Y-m-d H:i:s')
        );
        return $this->db->insert('answer', $data);     
    }

}

?>