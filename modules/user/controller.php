﻿<?php

class controller_user extends controller {

	public function action_show() {

		$model = LoadModel('user');
        
		$model_group = LoadModel('group');
		
        $this->view = 'user/show.tpl';
		
		$logged = $this->core->global['auth']->logged;
        
		$this->data['title'] = "Пользователи";
		
        if ($logged) 
        {
            $this->data['err'] = $this->req['err'];
            $this->data['sendok'] = $this->req['sendok'];
            
            $this->data['users']    = $model->get_user_list();
            $this->data['group']    = $model_group->get_group_list_print();
            //$this->data['teachers']	    = $model->get_teachers_list(); 
            //$this->data['user']         = $this->core->global['auth']->user;
        }

		$this->render();   

	}
	
	public function action_delete() {

		$model = LoadModel('user');
		$del = $model->del_user($_POST['id']);
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		print $_POST['id'];
		die();
	}
	
	public function action_get() {

		$model = LoadModel('user');
		//$_POST['id'] = 2;
		$usr = $model->get_user($_GET['id']);
		$usr = $usr[0];
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		//print_r($usr);
		print "{\r\n";
		print "	\"user_id\": \"".$usr['user_id']."\",\r\n";
		print "	\"user_login\": \"".$usr['user_login']."\",\r\n";
		print "	\"user_type\": \"".$usr['user_type']."\",\r\n";
		print "	\"user_name\": \"".$usr['user_name']."\",\r\n";
		print "	\"user_fname\": \"".$usr['user_fname']."\",\r\n";
		print "	\"user_lname\": \"".$usr['user_lname']."\",\r\n";
		print "	\"user_group\": \"".$usr['user_group']."\",\r\n";
		print "	\"user_number\": \"".$usr['user_number']."\",\r\n";
		print "	\"user_dregistration\": \"".$usr['user_dregistration']."\",\r\n";
		print "	\"user_ldate\": \"".$usr['user_ldate']."\"\r\n";
		print "}\r\n";
		die();
	}
	
	public function action_add() {

		$model = LoadModel('user');
		$iferror = false;
		$user=array();
		print '{';
		print '"error": {';
		$user['user_fname'] = $_GET['user_fname'];
		$user['user_name'] = $_GET['user_name'];
		$user['user_lname'] = $_GET['user_lname'];
		$user['user_number'] = $_GET['user_number'];
		$user['user_group'] = $_GET['user_group'];
		$user['user_type'] = $_GET['user_type'];
		$user['user_login'] = $_GET['user_login'];
		$user['user_dregistration'] = date('Y-m-d');
		//$user['user_dregistration'] = date('');
		if (empty($_GET['id']))
		{
			if ($model->valid_login($_GET['user_login']) > 0)
			{
				print '"login": "1"';
				$iferror = true;
			}
			else
				print '"login": "0"';
			$user['user_salt'] = $model->generate_salt();
			$user['user_passwd'] = md5($_GET['user_passwd'].''.$user['user_salt']);
		}
		else
		{
			$user['user_id'] = $_GET['id'];
			if (!empty($user['user_passwd']))
			{
				$user['user_salt'] = $model->generate_salt();
				$user['user_passwd'] = md5($_GET['user_passwd'].''.$user['user_salt']);
			}
		}
		
		print '}';
		print '}';
		
		if (!$iferror)
		{
			if (empty($_GET['id']))
			{
				$model->set_user($user);
			}
			else
			{
				$model->edit_user($user);
			}
		}
		//if (empty($_POST['id']))
			//print 'add';
		//$del = $model->del_user($_POST['id']);
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		//print $_POST['id'];
		die();
	}
	
	public function action_getlist() {

		$model = LoadModel('user');
		//$_POST['id'] = 2;
		$usr = $model->get_user_list();
		//$usr = $usr[0];
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		//print_r($usr);
		print "<table class='user_table'>
		<tr>
			<th>№</th>
			<th>Login</th>
			<th>ФИО</th>
			<th>Тип</th>
			<th>Действия</th>
		</tr>";
		foreach ($usr as $user)
		{
			print "
		<tr id_usr='".$user['user_id']."'>
			<td>".$user['user_id']."</td>
			<td>".$user['user_login']."</td>
			<td>".$user['user_fname']." ".$user['user_name']." ".$user['user_lname']."</td>";
			if ($user['user_type'] == 'student')
				$texts = 'Студент';
			else
				$texts = 'Админ';
				
			print "
			<td>".$texts."</td>
			<td><span class='del' >Удалить</span></td>
		</tr>";
		}
		print "</table>";
		die();
	}


	public function action_index() {

		$model = LoadModel('faq');
        
        $this->view = 'faq/board.tpl';
		
		$logged = $this->core->global['auth']->logged;
        
		$this->data['title'] = "Вопросы преподавателю";
		
        if ($logged) 
        {
            $this->data['err'] = $this->req['err'];
            $this->data['sendok'] = $this->req['sendok'];
            
            $this->data['questions']    = $model->get_questions_list();
            $this->data['teachers']	    = $model->get_teachers_list(); 
            $this->data['user']         = $this->core->global['auth']->user;
        }

		$this->render();   

	}


	public function action_send() {
        
        $this->view = 'faq/board.tpl';
        $logged = $this->core->global['auth']->logged;         
	    if ($logged) 
        {
            $model = LoadModel('faq');
            
            $fuser = $this->core->global['auth']->user['user_id'];
            $tuser = mysql_escape_string($this->req['teacher']);
            $theme = mysql_escape_string($this->req['theme']);
            $ask   = mysql_escape_string($this->req['message']);      
              
            $r = $model->send($fuser, $tuser, $theme, $ask);
            
            if ($r)
            {
                $mes = 'Вопрос успешно отправлен на модерацию';
                $this->redirect('/faq?sendok=' .$mes);                
            }
            else
            {
                $mes = 'Вопрос не отправлен!';
                $this->redirect('/faq?err=' .$mes);
            }
            
        }
                
        $this->render();

	}

}


?>