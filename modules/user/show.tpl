{if $logged}
{if $err}<div class="errmessage">{$err}</div>{/if} 
{if $sendok}<div class="okmessage">{$sendok}</div>{/if}
<script type="text/javascript" src="/js/user.js"></script>

<div id='user_div'>
<table class='user_table'>
		<tr>
			<th>№</th>
			<th>Login</th>
			<th>ФИО</th>
			<th>Тип</th>
			<th>Действия</th>
		</tr>
{foreach $users as $user}
		<tr id_usr='{$user.user_id}'>
			<td>{$user.user_id}</td>
			<td>{$user.user_login}</td>
			<td>{$user.user_fname} {$user.user_name} {$user.user_lname}</td>
			<td>{if $user.user_type == 'student'}Студент{elseif $user.user_type == 'admin'}Админ{elseif $user.user_type == 'student'}Неизвестно{/if}</td>
			<td><span class='del' >Удалить</span></td>
		</tr>
{/foreach}
</table>
</div>

<table class='user_add' align=center>
	<tr>
		<th align=right>Параметр</th>
		<th align=left>Значение</th>
	</tr>
	<tr id='tr_usr_id'>
		<td align=right>Id</td>
		<td align=left ></td>
	</tr>
	<tr id='tr_usr_type'>
		<td align=right>Статус</td>
		<td align=left>
			<select name='user_type'>
				<option value='admin'>Админ</option>
				<option value='student'>Студент</option>
			</select>
		</td>
	</tr>
	<tr id='tr_usr_login'>
		<td align=right>Login *</td>
		<td align=left><input name='user_login' type='text' size=10></td>
	</tr>
	<tr id='tr_usr_pass'>
		<td align=right>Password</td>
		<td align=left><input name='user_passwd' type='text' size=10></td>
	</tr>
	<tr id='tr_usr_f'>
		<td align=right>Фамилия *</td>
		<td align=left><input name='user_fname' type='text'></td>
	</tr>
	<tr id='tr_usr_i'>
		<td align=right>Имя *</td>
		<td align=left><input name='user_name' type='text'></td>
	</tr>
	<tr id='tr_usr_o'>
		<td align=right>Отчество *</td>
		<td align=left><input name='user_lname' type='text'></td>
	</tr>
	<tr id='tr_usr_gruop'>
		<td align=right>Группа</td>
		<td align=left>
			<select name='user_group'>
				{foreach $group as $gr}
				<option value='{$gr.group_id}'>{$gr.group_code}</option>
				{/foreach}
			</select>
		</td>
	</tr>
	<tr id='tr_usr_num'>
		<td align=right>Номер в группе</td>
		<td align=left><input name='user_number' type='text' size=3></td>
	</tr>
	<tr id='tr_usr_subit'>
		<td align=right><input name='user_add' value='Новый' type='submit' size=3></td>
		<td align=left><input name='user_edit' value='Сохранить' type='submit' size=3><input name='user_del' value='Удалить' type='submit' size=3></td>
	</tr>
	
</table>

<div class="clear"></div>
</div>


{else}
    <p class="NotAuthorised">Вы не авторизованы!!!</p>
{/if}
