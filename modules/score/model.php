<?php

class model_score extends model {     

	public function GetTestsList($type = 'all') {
        
        $user_id = $this->core->global['auth']->user['user_id'];  
        $filter = '';
        
        if ($type == 'all')
        {
            $filter =  ' ';        
        } 
        elseif ($type == 'done')
        {
            $filter =  ' AND `study_done` = 1 ';       
        }
        elseif ($type == 'fail')
        {
            $filter =  ' AND `study_done` = 0 ';        
        }
        
        $result = $this->db->query('SELECT `study_date`, `test_id`, `test_title`, `test_about`, `study_done`, `study_mark`, `doc_id`, `doc_title` FROM `study` JOIN `test` ON study_test = test_id JOIN `document` ON doc_test = test_id WHERE `study_user` = ' . (int)$user_id . $filter . 'ORDER BY `study_date` DESC');
        
        if ($result)
        {
            foreach ($result as &$v)
            {
                $v['study_date'] = GetFormatedDate($v['study_date']);
            
            }
        }
         
        return $result;   
        
    }    
    
}

?>