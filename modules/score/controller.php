<?php

class controller_score extends controller {

    public function action_index() {    
        
        $logged = $this->core->global['auth']->logged;     
        
        if ($logged) 
        {
            $model = LoadModel('score');
                    
            $this->view = 'score/score.tpl';
            $this->data['title'] = 'Моя успеваемость';
            $this->data['title_page'] = 'Моя успеваемость';                     
            
        } else {
            $this->data['title_page'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }

        $this->render();   

    }
    
    public function action_allTests() {
        
        $logged = $this->core->global['auth']->logged;     
        
        if ($logged) 
        {
            $model = LoadModel('score');
            json($model->GetTestsList());                    
            
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
            $this->render();
        }                        
              
    }
    
    public function action_doneTests() {
        
        $logged = $this->core->global['auth']->logged;     
        
        if ($logged) 
        {
            $model = LoadModel('score');
            json($model->GetTestsList('done'));                     
            
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
            $this->render();
        }                        
              
    }

    public function action_failTests() {
        
        $logged = $this->core->global['auth']->logged;     
        
        if ($logged) 
        {
            $model = LoadModel('score');
            json($model->GetTestsList('fail'));                     
            
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
            $this->render();
        }                        
              
    }
    
}


?>