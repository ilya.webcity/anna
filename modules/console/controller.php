<?php

class controller_console extends controller {

    public $studentDBUser;
    
    public $access = array(

        '_default' => array('student' => true, 'admin' => true)        

    );
    
    public function action_index() { 
        
        $model = LoadModel('console');
        $this->view = 'console/console.tpl';
        $this->data['title'] = 'Консоль запросов';
        $this->data['title_page'] = 'Консоль запросов';
        
        $this->render();
        
    }
    
    # Обработка запроса
    public function action_query() {        

        $model = LoadModel('console');
        
        $query_text = trim(htmlspecialchars($this->req['query_text']));
        //$query_text = 'SELECT * FROM `table1`';        
        json($model->query($query_text));
        
        //debug($model->query($query_text));
        
        
    }             
    
    
}


?>