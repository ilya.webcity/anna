<?php

class model_console extends model {

    public function query($query) {
        
        $studDB = new DB(DB_HOST, STUDENT_DB_USER, STUDENT_DB_PASS, STUDENT_DB_BASE);
        
        try {
            $result = $studDB->ConsoleQuery($query);
            return $result;        
        } 
        catch(Exception $e) 
        {   
            return $e->getMessage();
        }     
        
    }
    
}

?>