<html>
    <head>
        <title>{$title_page|default:'Заголовок не задан'}</title>
        <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon" /> 
		<link rel="stylesheet" type="text/css" href="/css/style.css">
        <link rel="stylesheet" type="text/css" href="/css/user.css">        

        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>        

        {* Визуальный редактор *}
        <script type="text/javascript" src="/js/redactor/js/redactor/redactor.js"></script>
        <link rel="stylesheet" href="/js/redactor/js/redactor/css/redactor.css" type="text/css" /> 
        {* /Визуальный редактор *}       
    </head>
    <body>
        <div class="header">

		</div>
        <div class="center">

            <div class="content">
				<h3>{$title}</h3>
				{$content}
			</div>
        </div>
        <div class="footer">
            {$footer}            
        </div>
    </body>
</html>