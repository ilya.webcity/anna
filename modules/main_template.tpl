<html>
    <head>
        <title>{$title_page|default:'Заголовок не задан'}</title>
        <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon" /> 
		<link rel="stylesheet" type="text/css" href="/css/style.css">
        <link rel="stylesheet" type="text/css" href="/css/user.css">        
        {* доп меню в разделе Учебный курс *}
        <link rel="stylesheet" type="text/css" href="/css/docmenufloater/style.css">
                        
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>        
        <script type="text/javascript" src="/js/menu.js"></script>
		<script type="text/javascript" src="/js/faq.js"></script>
        
        {* Твиттер *}
        <link rel="stylesheet" type="text/css" href="/css/twitter/scroll.css">
        <script type="text/javascript" src="/js/twitter/jquery.mousewheel.js"></script>
        <script type="text/javascript" src="/js/twitter/jscrollpane.js"></script>
        <script type="text/javascript" src="/js/twitter/script.js"></script>
        {* / Твиттер *}  

		{* форма Задать вопрос *}
		<script src="/js/form/validate.min.js" type="text/javascript"></script>
		<script src="/js/form/init.js" type="text/javascript"></script>
		{* форма Задать вопрос *}        
        {* табы для объявлений *}
        <script src="/js/tabs.js" type="text/javascript"></script>
        {* /табы для объявлений *}  
        {* Визуальный редактор *}
        <script type="text/javascript" src="/js/redactor/js/redactor/redactor.js"></script>
        <link rel="stylesheet" href="/js/redactor/js/redactor/css/redactor.css" type="text/css" /> 
        {* /Визуальный редактор *}       
    </head>
    <body>
        <div class="header">
			{if $logged}
				Вы вошли как <b>{$user.login}</b> (<a href="/id/logout">Выйти</a>)
			{else}
				<a href="/id/login">Войти</a>
			{/if}

		</div>
        <div class="center">

            <div class="menu_block">            
            
            {* картинко :) *}
            <div class="xtree">
                <img src="/img/siteico.png"></img><br/>
                <span class="ng"></span>
            </div>
            <hr style="padding-left: 4px;">
            
			{* меню *}
                <ul class="container">                    
                    {* элемент меню (раскрывающийся) *}
                    {if $menu}
                    {foreach $menu as $group}
                    <li class="menu">
                        <ul>
                            <li class="{if $group.menu_sub}button{else}button_empty{/if}"><a href="{if $group.menu_sub}javascript:void(0){else}{$group.menu_url}{/if}" class="{$group.color}">{$group.menu_title}</a></li>
                            <li class="dropdown">
                                <ul>
                                    {foreach $group.menu_sub as $item}
                                    <li><a href="{$item.menu_url}">{$item.menu_title}</a></li>
                                    {/foreach}
                                </ul>
                            </li>
                        </ul>           
                    </li>
                    {/foreach}
                    {else}
                        Меню отсутствует!
                    {/if}      
                                    
                </ul>
                
                {if $logged}                
                {* TwitterTincker *}
                
                {*
				<div id="twitter-ticker"> 
                    <!-- Контейнер для заголовка и иконки -->
                    <div id="top-bar">
                    <div id="twitIcon"><img src="/img/twitter/womtec.png" width="64" height="64" alt="Twitter" /></div>
                    <h2 class="tut">Твитты</h2>
                    </div>
                     
                    <!-- Контейнер с картиной загрузки, которая исчезает после загрузки твиттов -->
                    <div id="tweet-container">
                    <img id="loading" src="/img/twitter/loading.gif" width="16" height="11" alt="Loading" />
                    </div>
                     
                    <!-- Контейнер для вывода твиттов -->
                    <div id="scroll"></div>                 
                </div> 
				*}
                {* / TwitterTincker *}
                
                {/if}
                             
             </div>
             			 
            {if $announcements}
            <div class="announcement">                					
                <div class="section">
                    <ul class="tabs">
                    {assign var=fl value=1}
                    {* Формируем заголовки объявлений *}
                    {foreach $announcements as $a} 
                        <li{if $fl == 1} class="current"{/if}>[{$a.ann_cdate}] {$a.ann_title}</li>
                        {$fl =0}
                    {/foreach}
                    </ul>
                    {$fl =1}
                    {foreach $announcements as $a}                        
                        <div class="box{if $fl == 1} visible{/if}">    
                            <div class="anncontent">{$a.ann_text|truncate:110} <a href="/ann/read/{$a.ann_id}/{$a.ann_cdate}/">Подробнее</a></div>
                        </div>
                        {$fl =0}
                    {/foreach}                        
                </div>  	
            </div>
            {/if}              
            <div class="content">
				<h3>{$title}</h3>
				{$content}
			</div>
        </div>
        <div class="footer">
            {$footer}            
        </div>
    </body>
</html>