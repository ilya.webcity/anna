<?php

class model_profile extends model {     

	public function get_user_info($user_id) {

        $info = $this->db->Pick('SELECT * FROM `user` WHERE `user_id` = ' . (int)$user_id);
        
        if ($info)
        {
            $info['user_dregistration'] = GetFormatedDate($info['user_dregistration']);
            $info['user_ldate'] = GetFormatedDate($info['user_ldate'],'-',':','datetime');
        }
                        
        return $info;
	}
    
    public function get_group_info($group_id) {        
       
       return $this->db->Pick('SELECT `group_id`, `group_code`, `group_cdate`, `group_cstudents` FROM `group` WHERE `group_id` = ' .(int)$group_id);      
        
    }
    
        
    public function get_specialty_info($spec_id) {        
       
       return $this->db->Pick('SELECT `spec_name`, `spec_about` FROM `specialty` WHERE `spec_id` = ' .(int)$spec_id);      
        
    }
    
}

?>