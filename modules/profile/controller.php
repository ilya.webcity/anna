<?php

class controller_profile extends controller {

	public function action_index() {	
		
		$logged = $this->core->global['auth']->logged; 	
		
        if ($logged) 
        {
            $model = LoadModel('profile');
            $modelg = LoadModel('group');
            
            $user_id = $this->core->global['auth']->user['user_id'];
            $spec_id = $this->core->global['auth']->user['user_spec'];
            $group_id = $this->core->global['auth']->user['user_group'];
        
            $this->view = 'profile/profile.tpl';
            $this->data['title_page'] = 'Мои данные';
           
            $this->data['q'] = $model->get_user_info($user_id);
            $this->data['gr'] = $modelg->get_group_info($group_id);
            $this->data['sp'] = $model->get_specialty_info($spec_id);           
            
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }

		$this->render();   

	}

}


?>