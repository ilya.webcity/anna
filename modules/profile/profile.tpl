<div class = "user_info">
    <h2>Мои данные</h2>
    <p><b>Ф.И.О.</b>: {$q.user_fname} {$q.user_name|truncate:1:'':true}. {$q.user_lname|truncate:1:'':true}.</p>
    <p><b>Логин</b>: {$q.user_login}</p>
    <p><b>Дата регистрации</b>: {$q.user_dregistration}</p>
    <p><b>Последнее посещение</b>: {$q.user_ldate}</p>
    <hr>
    <h2><a href="/group/showlist/{$gr.group_id}" title="Показыть список учащихся группы">Моя группа</a></h2>
    <p><b>Номер группы</b>: {$gr.group_code}</p>
    <p><b>Дата создания</b>: {$gr.group_cdate}</p>
    <p><b>Количество студентов</b>: {$gr.group_cstudents}</p>
    <hr>
    <h2>Моя специальность</h2>
    <p><b>Название</b>: {$sp.spec_name|default:'Номер не определен'}</p>
    <p><b>Описание</b>: {$sp.spec_about|default:'Информация отсутствует'}</p>
</div>
