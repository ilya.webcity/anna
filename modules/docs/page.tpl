{if $page}

    <script>    
    
    $(document).ready(function($){        
        
        $("#back-top").hide();    
        $(function () {

            $(window).scroll(function () {
                if ($(this).scrollTop() > 100) {
                    $('#back-top').fadeIn();
                } else {
                    $('#back-top').fadeOut();
                }
            });

            $('#back-top a').click(function () {
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });
        }); 
        
        
        $('#chaptermenu').dcFloater({
                width: 150,
                location: 'top',
                align: 'right',
                offsetLocation: 10,
                offsetAlign: 20,
                speedFloat: 1500,
                speedContent: 600,
                tabText: 'Смотрите также',
                autoClose: true,
                event: 'click'
        });   
        
        
    });    
    </script>
    
    <script type="text/javascript" src="/js/docmenufloater/jquery.hoverintent.minified.js"></script>
    {*<script type="text/javascript" src="/js/docmenufloater/jquery.easing.js"></script>*}
    <script type="text/javascript" src="/js/docmenufloater/jquery.floater.1.2.js"></script> 
    
    {* ВИЗУАЛЬНЫЙ РЕДАКТОР *}
    <link rel="stylesheet" href="/js/redactor/css/redactor.css" />
    <script src="/js/redactor/redactor.js"></script>

    <p>
    {* Навигационная панелька *}
    {if $page.frw_page}
        <a id="left" href="/docs/id/{$page.frw_page.doc_id}" title='Перейти к "{$page.frw_page.doc_title}"'></a>
    {else}
        <a id="left" href="/docs/{$method}" title='К оглавлению'></a> 
    {/if}
    
    {if $page.nxt_page}
        <a id="right" href="/docs/id/{$page.nxt_page.doc_id}" title='Перейти к "{$page.nxt_page.doc_title}"'></a>
    {else}
        <a id="right" href="/docs/{$method}" title='К оглавлению'></a> 
    {/if}
    {* /Навигационная панелька *}
    </p>
    <hr>
    <p>{$page.doc_content}</p>    
    {if $page.doc_attachment}
        <hr>
        <h2>Файлы к теме</h2>        
        {foreach $page.doc_attachment as $att}
            <a href="/download?date={$att.date}&name={$att.file}&source={$att.source}">  
                <img src="/img/attach.png" style="padding-right: 10px;" title="Cкачать">{$att.title}</img>
            </a>
        {/foreach}
        <hr>        
    {/if}
    {if $page.doc_test <> 0}
        <h2>Тесты</h2>
        <p><a href="/test/go/{$page.test.id}"><img src="/img/test_ico.png" style="padding-right: 10px;" title="Пройти тест">{$page.test.name}</img></a></p>
    {/if}
    
    <hr>
    <div>
    {* Навигационная панелька *}
    {if $page.frw_page}
        <a id="left" href="/docs/id/{$page.frw_page.doc_id}" title='Перейти к "{$page.frw_page.doc_title}"'></a>
    {else}
        <a id="left" href="/docs/study" title='К оглавлению'></a> 
    {/if}
    
    {if $page.nxt_page}
        <a id="right" href="/docs/id/{$page.nxt_page.doc_id}" title='Перейти к "{$page.nxt_page.doc_title}"'></a>
    {else}
        <a id="right" href="/docs/study" title='К оглавлению'></a> 
    {/if}
    {* /Навигационная панелька *}
    </div>
    
    <div class="clear"></div>
    
    {if $page.closepages}
    {* Дополнительное меню. Отображает страницы раздела *} 
        <div id="chaptermenu">
            <ul class="nav-main">
                {foreach $page.closepages as $p}
                    <li>
                        <a href="/docs/id/{$p.doc_id}" title="{$p.doc_title}">
                            <span>                                
                                {$p.doc_title|truncate:20:'..':true}                                    
                            </span>
                        </a>
                    </li>
                {/foreach}
                    <li><hr></li>
                    <li><a href="/docs/{$method}"><span><b><u>Оглавление</u></b></span></a></li>
            </ul>
            <div class="dc-corner"><span></span></div>
        </div>
    {*/ Дополнительное меню *}
    {/if}
    
    <p id="back-top">
        <a href="#top"><span></span>Хочу наверх :)</a>
    </p>
    
{else}
    <p>Запрашиваемая страница не найдена или не доступна!!!</p>
{/if}