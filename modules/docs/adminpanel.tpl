{* Админская часть - курс, метода, окр *}
    <script type="text/javascript" src="/js/docs/script.js"></script>    
{* /Админская часть - курс, метода, окр *}

<div class = "admin_accessmanager">
    <p><h3>        
        <a href ="/docs/panel/doc">Учебный курс</a> | 
        <a href ="/docs/panel/method">Методические указания</a> |
        <a href ="/docs/panel/cw">Контрольные работы</a> |
        <a href="/docs/addDoc/{$method}"><img src="/img/adddoc.png" style="margin: 4px 5px 0px 0px;"></img>Создать документ</a>         
    </h3></p>
    
    {* Кнопка "Новый док" *}
    <br> 
   
   {include file='docs/coursepanel.tpl'}    
    
    <div class = "clear"></div> 
</div>