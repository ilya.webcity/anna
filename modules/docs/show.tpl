{if $logged}
{if $err}<div class="errmessage">{$err}</div>{/if} 
{if $sendok}<div class="okmessage">{$sendok}</div>{/if}
<script type="text/javascript" src="/js/doc.js"></script>

<div id='items_div'>
</div>

<div id='items_div_add'>
<table class='item_add' align=center style='width:100%'>
	<tr>
		<th align=right>Параметр</th>
		<th align=left>Значение</th>
	</tr>
	<tr id='tr_id'>
		<td align=right>Id</td>
		<td align=left ></td>
	</tr>
	<tr >
		<td align=right>Название *</td>
		<td align=left><input name='doc_title' type='text' size=10></td>
	</tr>
	<tr >
		<td align=right>Родитель *</td>
		<td align=left>
			<select name='doc_pid'>
				<option value=0>Нету</option>
				{foreach $doc_item as $item}
				<option value={$item.doc_id}>{$item.doc_title}</option>
				{/foreach}
			</select>
		</td>
	</tr>
	<tr >
		<td align=center colspan=2>Контент *</td>
	<tr>
		<td align=left colspan=2>
			<textarea name='doc_content' style='width:100%; height:100px' id="doc_content"></textarea>
		</td>
	</tr>
	<tr >
	<tr >
		<td align=right>Автор/Редактор *</td>
		<td align=left name='doc_author'>
		</td>
	</tr>
		<td align=right>Номер *</td>
		<td align=left><input name='doc_number' type='text' size=10></td>
	</tr>
	
	<tr id='tr_usr_subit'>
		<td align=right><input name='user_add' value='Новый' type='submit' size=3></td>
		<td align=left><input name='user_edit' value='Сохранить' type='submit' size=3><input name='user_del' value='Удалить' type='submit' size=3></td>
	</tr>
	
</table>

</div>
<div class="clear"></div>
</div>


{else}
    <p class="NotAuthorised">Вы не авторизованы!!!</p>
{/if}
