<?php

class controller_docs extends controller {

	public $access = array(

		'_default' => array('student' => true, 'admin' => true),
		'admin' => array('admin' => true),
        'panel' => array('admin' => true),
        'redaction' => array('admin' => true),
        'saveDoc' => array('admin' => true),
        'addDoc' => array('admin' => true),
        'saveNewDoc' => array('admin' => true),
        'deleteDoc' => array('admin' => true)

	);

    public function action_index() {

        $logged = $this->core->global['auth']->logged;

        if ($logged)
        {
            $this->redirect();

        } else {
            $this->data['title'] = 'Ошибка авторизации!';
            $this->view = 'notauthpage.tpl';
            $this->render();
        }

    }

    public function action_addrd() {
        //$model = LoadModel('docs');
        //$model->addrd();
    }

    # Учебный курс
    public function action_doc() {

        $spec_id = $this->core->global['auth']->user['user_spec'];

        $model = LoadModel('docs');
        $docTree = $model->getCourseList($spec_id);

        $this->data['title'] = 'Содержание учебного курса';
        $this->data['title_page'] = 'Содержание курса';
        $this->data['docTree'] = $docTree;

        $this->view = 'docs/doctree.tpl';
        $this->render();        
    } 
    
    # Методическая литература
    public function action_method() {
       
         $spec_id = $this->core->global['auth']->user['user_spec'];

        $model = LoadModel('docs');
        $docTree = $model->getMethodicList($spec_id);

        $this->data['title'] = 'Список методической литературы';
        $this->data['title_page'] = 'Список методической литературы';
        $this->data['docTree'] = $docTree;

        $this->view = 'docs/doctree.tpl';
        $this->render();
        
    }
    
    # КР - ки
    public function action_cw() {
       
        $spec_id = $this->core->global['auth']->user['user_spec'];

        $model = LoadModel('docs');
        $docTree = $model->getCWList($spec_id);

        $this->data['title'] = 'Список контрольных работ';
        $this->data['title_page'] = 'Список контрольных работ';
        $this->data['docTree'] = $docTree;

        $this->view = 'docs/doctree.tpl';
        $this->render();
        
    }
    
    # Отображаем страницу по ID
    public function action_id() {
       
        $spec_id = $this->core->global['auth']->user['user_spec'];

        $page_id = (int)htmlspecialchars($this->path[0]);

        $model = LoadModel('docs');
        $page = $model->getDocById($spec_id, $page_id);

        $this->data['title'] = $page['doc_title'];
        $this->data['title_page'] = $page['doc_title']; // заголовок страницы (верхний)
        $this->data['page'] = $page;
        $this->data['method'] = $model->getDocTypeById($page_id);

        $this->view = 'docs/page.tpl';
        $this->render();        

    }


    #==============================
    # АДМИНИСТРАТОРСКИЕ Ф-ИИ
    #==============================

    public function action_admin() {

        $model = LoadModel('docs');
        $this->view = 'docs/adminpanel.tpl';
        
        $this->data['title'] = 'Редактирование материалов';
        $this->data['title_page'] = 'Редактирование курса';
        $this->render();

    }
    
    # Открывает форму редактирования док-а
    public function action_redaction() {
        
        $model = LoadModel('docs');
        $this->view = 'docs/courseredaction.tpl';
        
        $this->data['title'] = 'Редактирование документа';
        $this->data['title_page'] = 'Редактирование документа';
        
        $doc_id = (int)$this->path[0];
        
        if (!$model->isValidDocId($doc_id))
        {
            Error_404();
        }
        
        $this->data['course'] = $model->redactionedDoc($doc_id);
        $this->data['doc_updater'] = $this->core->global['auth']->user['user_id'];
        $this->data['title_page'] = 'Редактирование документа';
        $this->data['title'] = 'Редактирование документа';
        $this->data['method'] = $model->getDocTypeById($doc_id);
        
        //debug( $this->data['course']);
        
        $this->render();
        
    }

    # Панель редактирования курса
    public function action_panel() {
    
        $model = LoadModel('docs');
        $this->view = 'docs/adminpanel.tpl';        
        
        $type = $this->path[0];        
        
        switch($type)
        {
            case 'doc':                
                $this->data['title'] = 'Редактирование учебного курса';
                $this->data['title_page'] = 'Редактирование учебного курса';
                $this->data['docTree'] = $model->getCourseList(); // все
                break;
            case 'method':
                $this->data['title'] = 'Редактирование методической литературы';
                $this->data['title_page'] = 'Редактирование методической литературы';
                $this->data['docTree'] = $model->getMethodicList();   
                break;
            case 'cw':
                $this->data['title'] = 'Редактирование контрольных работ';
                $this->data['title_page'] = 'Редактирование контрольных работ';
                $this->data['docTree'] = $model->getCWList();
                break;              
        }
        $this->data['method'] = $type;
        
        $this->render();
        
    }
    
    # Сохраняет изменения в документа
    public function action_saveDoc() {
               
        $inputAr = array (            
            'doc_pid' => (int)$this->req['doc_pid'],
            'doc_title' => $this->req['doc_title'],
            'doc_type' => $this->req['doc_type'],
            'doc_content' => $this->req['doc_content'],
            'doc_author' => (int)$this->req['doc_author'],
            'doc_cdate' => $this->req['doc_cdate'],
            'doc_updater' => (int)$this->req['doc_updater'],
            'doc_udate' => $this->req['doc_udate'],
            'doc_number' => $this->req['doc_number'],
            'doc_test' => (int)$this->req['doc_test']        
        );
        $model = LoadModel('docs');
        $result = $model->saveDoc($inputAr, (int)$this->req['doc_id']);
        
        json($result);
        
    }
    
    # Сохранение нового дока
    public function action_saveNewDoc() {
               
        $inputAr = array (            
            'doc_pid' => (int)$this->req['doc_pid'],
            'doc_title' => $this->req['doc_title'],
            'doc_type' => $this->req['doc_type'],
            'doc_content' => $this->req['doc_content'],
            'doc_author' => (int)$this->req['doc_author'],
            'doc_cdate' => $this->req['doc_cdate'],
            'doc_updater' => (int)$this->req['doc_updater'],
            'doc_udate' => $this->req['doc_udate'],
            'doc_number' => $this->req['doc_number'],
            'doc_test' => (int)$this->req['doc_test']        
        );
        $model = LoadModel('docs');
        $result = $model->addDoc($inputAr);
        
        json($result);
        
    }
    
    # Создать новый док
    public function action_addDoc() {
    
        $model = LoadModel('docs');
        $doc_type = $this->path[0];
        
        $this->view = 'docs/courseredaction.tpl';
        switch ($doc_type)
        {
            case 'doc':
                $this->data['title'] = 'Создание документа учебного курса';
                $this->data['title_page'] = 'Создание документа учебного курса';
                break;
            case 'method':
                $this->data['title'] = 'Создание документа методической литературы';
                $this->data['title_page'] = 'Создание документа методической литературы';
                break;
             case 'cw':
                $this->data['title'] = 'Создание документа контрольной работы';
                $this->data['title_page'] = 'Создание документа контрольной работы';
                break;
        }
        $course = array ();
        $this->data['course'] = $model->getDopList($course, $doc_type);
        $this->data['method'] = $doc_type; 
        
        $this->render();
            
    }
    
    # Удаление дока
    public function action_deleteDoc() {
    
        $model = LoadModel('docs');
        $result = $model->deleteDoc((int)$this->req['doc_id']);
        
        json($result);
        
    }
    
    #==============================
    #==============================


    # Степины экшены
    public function action_list() {

        $model = LoadModel('docs');

        $this->view = 'docs/show.tpl';

        $logged = $this->core->global['auth']->logged;

        $this->data['title'] = "Контент";

        if ($logged)
        {
            $this->data['err'] = $this->req['err'];
            $this->data['sendok'] = $this->req['sendok'];

            $this->data['doc_item'] = $model->get_doc_list_id('0');
        }

        $this->render();

    }

    public function action_delete() {

        $model = LoadModel('docs');
        $del = $model->del_doc($_POST['id']);
        print $_POST['id'];
        die();
    }

    public function action_get() {

        $model = LoadModel('docs');
        $model_u = LoadModel('user');
        //$_GET['id'] = 1;
        $usr = $model->get_doc($_GET['id']);
        $usr = $usr[0];
        /*if (!empty($del))
            print 'OK ';
        else
            print "NO";*/
        //print_r($usr);
        //print_r($usr['menu_type']);
        //$usr['menu_type'] = explode(',', $usr['menu_type']);
        //print_r($usr['menu_type']);

        $value1 = $model_u->get_user($usr['doc_author']);
        if(!empty($value1))
            $usr['doc_author'] = $value1[0]['user_login'];
        $value2 = $model_u->get_user($usr['doc_updater']);
        if(!empty($value2))
            $usr['doc_updater'] = $value2[0]['user_login'];
        echo json_encode($usr);
        die();


        print "{\r\n";
        foreach ($usr as $key => $value)
            if ($key == 'doc_author' || $key == 'doc_updater')
            {
                $value1 = $model_u->get_user($value);
                $value1 = $value1[0];
                print "    \"".$key."\": \"".$value1['user_login']."\",\r\n";
            }
            else
                print "    \"".$key."\": \"".$value."\",\r\n";

        print "    \"error\": \"0\"\r\n";
/*        print "    \"user_type\": \"".$usr['user_type']."\",\r\n";
        print "    \"user_name\": \"".$usr['user_name']."\",\r\n";
        print "    \"user_fname\": \"".$usr['user_fname']."\",\r\n";
        print "    \"user_lname\": \"".$usr['user_lname']."\",\r\n";
        print "    \"user_group\": \"".$usr['user_group']."\",\r\n";
        print "    \"user_number\": \"".$usr['user_number']."\",\r\n";
        print "    \"user_dregistration\": \"".$usr['user_dregistration']."\",\r\n";
        print "    \"user_ldate\": \"".$usr['user_ldate']."\"\r\n";*/
        print "}\r\n";
        die();
    }

    public function action_add() {

        $model = LoadModel('docs');
        print $_GET['doc_id'].'--<br>';
        $iferror = false;
        $item=array();
        print '{';
        print '"error": {';
        $item['doc_title'] = $_GET['doc_title'];
        $item['doc_pid'] = $_GET['doc_pid'];
        $item['doc_content'] = $_GET['doc_content'];
        $item['doc_number'] = $_GET['doc_number'];
        $item['doc_cdate'] = date('Y-m-d');
        $item['doc_author'] = empty($this->core->global['auth']->user['user_id'])?0:$this->core->global['auth']->user['user_id'];
        $item['doc_updater'] = empty($this->core->global['auth']->user['user_id'])?0:$this->core->global['auth']->user['user_id'];
        $item['doc_test'] = empty($_GET['doc_test'])?0:$_GET['doc_test'];

        if (empty($_GET['doc_id']))
        {

        }
        else
        {
            $item['doc_id'] = $_GET['doc_id'];
        }

        print '}';
        print '}';
        //print_r($item);

        if (!$iferror)
        {
            if (empty($_GET['doc_id']))
            {
                $model->set_doc($item);
                print 'ok';
            }
            else
            {
                $model->edit_doc($item);
            }
        }
        die();
    }

    public function action_getlist() {

        $model = LoadModel('docs');
        //$_POST['id'] = 2;
        $items = $model->get_doc_list_id('0');
        //$usr = $usr[0];
        /*if (!empty($del))
            print 'OK ';
        else
            print "NO";*/
        //print_r($items);
        //print_r ($this->core->global['auth']->user);
        print "<table class='item_table' style='width:100%'>
        <tr>
            <th>Содержание</th>
        </tr>";
        $this->getlist('0','&nbsp;');
    /*    foreach ($items as $item)
        {
            print "
        <tr id_itm='".$item['doc_id']."'>
            <td>".$item['doc_number'].". <a href='".$item['doc_url']."'>".$item['doc_title']."</a></td>
        </tr>";
            foreach ($model->get_doc_list_id($item['doc_id']) as $subitem)
            {
                print "
            <tr id_itm='".$subitem['doc_id']."'>
                <td>&nbsp;&nbsp;&nbsp;".$item['doc_number'].".".$subitem['doc_number'].". <a href='".$subitem['doc_url']."'>".$subitem['doc_title']."</a></td>
            </tr>";

            }
        }*/
        print "</table>";
        die();
    }

    public function action_getselect() {

        $model = LoadModel('docs');
        //$_POST['id'] = 2;
        $items = $model->get_doc_list_id('0');
        //$usr = $usr[0];
        /*if (!empty($del))
            print 'OK ';
        else
            print "NO";*/
        //print_r($usr);

        print "    <select name='doc_pid'>
                <option value=0>Нету</option>";
        $this->getselect('0','');
        /*foreach ($items as $item)
        {
            print "
                <option value=".$item['doc_id'].">".$item['doc_number'].' '.$item['doc_title']."</option>";
            $id =  $item['doc_id'];
            while ($)
            {

            }
        }*/
        print "</select>";
        die();
    }

    public function getselect($id, $strnbsp) {
        $model = LoadModel('docs');
        $items = $model->get_doc_list_id($id);

        $str = '';
        if (!empty($id))
        {

            $items_parent = $model->get_doc($id);
            $items_parent = $items_parent[0];
            $str = $strnbsp.''.$items_parent['doc_number'].'.';
        }
        foreach ($items as $item)
        {
            print "
                <option value=".$item['doc_id'].">".$str.''.$item['doc_number'].'. '.$item['doc_title']."</option>";
            //$this->getselect($item['doc_id'], $strnbsp.'&nbsp;');
            $this->getselect($item['doc_id'], '&nbsp;'.$str);
        }
    }

    public function getlist($id, $strnbsp) {
		$model = LoadModel('docs');
		$items = $model->get_doc_list_id($id);
		$str = '';
		if (!empty($id))
		{

			$items_parent = $model->get_doc($id);
			$items_parent = $items_parent[0];
			$str = $strnbsp.''.$items_parent['doc_number'].'.';
		}

		foreach ($items as $item)
		{

			print "
		<tr id_itm='".$item['doc_id']."'>
			<td><span>".$str.''.$item['doc_number'].". ".$item['doc_title']."</span><div name='edit'></div></td>
		</tr>";

			//$this->getlist($item['doc_id'], $strnbsp.'&nbsp;&nbsp;');
			$this->getlist($item['doc_id'], '&nbsp;'.$str);
		}
    }
}


?>