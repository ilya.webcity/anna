{* ??????? ?? ????? ????*} 
<script>
$(document).ready(function(){

    $("#back-top").hide();
    
    $(function () {

        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#back-top').fadeIn();
            } else {
                $('#back-top').fadeOut();
            }
        });

        $('#back-top a').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });

});
</script>
{*/ ??????? ?? ????? ????*}
<p><h3>        
        <a href ="/docs/doc">Учебный курс</a> | 
        <a href ="/docs/method">Методические указания</a> |
        <a href ="/docs/cw">Контрольные работы</a>         
    </h3>
</p>

<ol class="docTree">
{foreach $docTree as $q}
    <li class="docTree">
        <a href="/docs/id/{$q.doc_id}">{$q.doc_title}</a>
        {* ?????? ??????? ??????????? *}
        {if $q.childs}
            {*<ol class="docTree"> *}  
            {foreach $q.childs as $ch1}
                <li class="docTreeL1">
                    <a href="/docs/id/{$ch1.doc_id}">&ensp;&ensp;&ensp;{$ch1.doc_title}</a>
                    {* ?????? ??????? ??????????? *}
                    {if $ch1.childs}
                        {*<ol class="docTree">*}  
                        {foreach $ch1.childs as $ch2}
                            <li class="docTree">
                                <a href="/docs/id/{$ch2.doc_id}">{$ch2.doc_title}</a>
                                {* ?????? ??????? ??????????? *}
                                {if $ch2.childs}
                                    {*<ol class="docTree">*}   
                                    {foreach $ch2.childs as $ch3}
                                        <li class="docTree">
                                            <a href="/docs/id/{$ch3.doc_id}">{$ch3.doc_title}</a>
                                            
                                        </li>
                                    {/foreach}
                                    {*</ol>*}
                                {/if}
                                {* /?????? ??????? ??????????? *}
                            </li>
                        {/foreach}
                        {*</ol>*}
                    {/if}
                    {* /?????? ??????? ??????????? *}
                </li>
            {/foreach}
            {*</ol>*}
        {/if}
        {* /?????? ??????? ??????????? *}
    </li>             
{/foreach}
</ol>

<p id="back-top">
        <a href="#top"><span></span>Хочу наверх :)</a>
</p>

