<?php

class model_docs extends model {

	# �����
    public function addrd () {  
        for ($i = 3; $i <= 45; $i++)
        {
            $data = array 
            (
                'rd_specialty' => 1,
                'rd_doc' => $i
            );
            $this->db->insert('rights_document', $data);
        }          
    }
    
    # �������� ������������ ���������� �� ��
    public function getMethodic () {  
        
           
        
    }
    
    # �������� ������� ���� (����������)
    public function getCourseList ($spec_id = -1, $doc_type = 'doc') {         
        
        # ���������� ���������� ������ �������
        $docTree = $this->makingDocTree(0, $spec_id, $doc_type);               
                
        return $docTree;   
    }
    
    # �������� ��� ��������� � ������������ �����������
    public function getMethodicList($spec_id = -1, $doc_type = 'method') {
        
        # ���������� ���������� ������ �������
        $docTree = $this->makingDocTree(0, $spec_id, $doc_type);               
                
        return $docTree;
        
    }
    
    # �������� ��� ��������� � ��
    public function getCWList($spec_id = -1, $doc_type = 'cw') {
        
        # ���������� ���������� ������ �������
        $docTree = $this->makingDocTree(0, $spec_id, $doc_type);               
                
        return $docTree;
        
    }
    
    # ���������� ������� ������ ����������
    public function makingDocTree($pid, $spec_id, $doc_type) {
    
        if ($spec_id != -1)
        {
            $query = 'SELECT `doc_id`, `doc_pid`, `doc_title`, `doc_number` FROM `document` JOIN `rights_document` ON doc_id = rd_doc WHERE `doc_type` = "' . $doc_type . '" AND `rd_specialty` = ' . (int)$spec_id . ' AND `doc_pid` = ' . (int)$pid . ' ORDER BY `doc_number`';    
        }
        else
        {
            $query = 'SELECT `doc_id`, `doc_pid`, `doc_title`, `doc_number` FROM `document` WHERE `doc_type` = "' . $doc_type . '" AND `doc_pid` = ' . (int)$pid . ' ORDER BY `doc_number`';    
        }
        
        $courseList = $this->db->query($query);
        $out = array();
        
        foreach ($courseList as $row)
        {
            $chidls = $this->makingDocTree($row['doc_id'], $spec_id, $doc_type);
            if ($chidls)
            {
                $row['childs'] = $chidls;
            } else
                $row['childs'] = array();
            $out[] = $row;
        }         
        return $out;    
    }  
    
    # �������� �������� �� ID
    public function getDocById($spec_id, $page_id) {
    
        $page = $this->db->pick('SELECT `doc_title`, `doc_pid`, `doc_type`,`doc_content`, `doc_test` FROM `document` JOIN `rights_document` ON doc_id = rd_doc WHERE `rd_specialty` = ' . (int)$spec_id . ' AND `doc_id` = ' . (int)$page_id);
        if ($page)
        {            
            # �������� �������� ����� �� ID
            if ($page['doc_test'] <> 0)
            {
               
                $test = $this->db->pick('SELECT `test_title`, `test_about` FROM `test` WHERE `test_id` = ' . (int)$page['doc_test']);
                if ($test)
                {
                    $page['test']['id'] = $page['doc_test'];
                    $page['test']['name'] = $test['test_title'];
                    $page['test']['about'] = $test['test_about'];
                }                     
            }
            
            # �������� ������ ���, ������� ��������� � ���� �� �������
            $closeToPage = $this->db->query('SELECT `doc_id`, `doc_title` FROM `document` WHERE `doc_pid` = '. (int)$page['doc_pid'] .' AND `doc_type` = "' . $page['doc_type'] . '" ORDER BY `doc_number`');
            if ($closeToPage)
                $page['closepages'] = $closeToPage;
            
            $number = -1;
                
            # �������� "��������-������"
            for ($i = 0; $i < sizeof ($closeToPage); $i++)
            {
                if ($closeToPage[$i]['doc_id'] == $page_id)
                {
                    $number = $i;
                    break;        
                }
            }
            
           
            
            try {$page['frw_page'] = $closeToPage[$number-1];} catch (Exception $e){}            
            try {$page['nxt_page'] = $closeToPage[$number+1];} catch (Exception $e){} 
        }
        
        return $page;           
        
    } 
    
    # �������� ������ �� ���������������� �������
    public function GetAttach(&$page) {
        //-- ���������� ������

        $page['doc_attachment'] = unserialize($page['doc_attachment']);       
        
        return $page;                    
    }    

    # ��������� ID �� ����������
    public function isValidDocId($doc_id) {
    
        $count = $this->db->Pick('SELECT COUNT(*) AS `count` FROM `document` WHERE `doc_id` = '. (int)$doc_id, 'count');
        if ($count == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }
    
    # �������� ��� ������ ��� �������������� ���������
    public function redactionedDoc($doc_id) {
    
        $result = $this->db->query('SELECT * FROM `document` WHERE `doc_id` = '.(int)$doc_id . ' LIMIT 1');
        $result = $result[0];
        if ($result)
        {
            $result = $this->getDopList($result);
            
            # �������� ������ ���, ������� ��������� � ���� �� �������
            $closeToPage = $this->db->query('SELECT `doc_id`, `doc_title` FROM `document` WHERE `doc_pid` = '. (int)$result['doc_pid'] .' AND `doc_type` = "'. $result['doc_type'] .'" ORDER BY `doc_number`');
            if ($closeToPage)
                $result['closepages'] = $closeToPage;
        }
        
        return $result;
    }
    
    # �������� ������ ��������� ���������, �������, ������
    public function getDopList(&$result, $doc_type='all') {
        
        if ($doc_type == 'all') {
            $result['availableParents'] = $this->db->query('SELECT `doc_id`, `doc_title` FROM `document` WHERE `doc_pid` = 0');    
        }
        else {
            $result['availableParents'] = $this->db->query('SELECT `doc_id`, `doc_title` FROM `document` WHERE `doc_pid` = 0 AND `doc_type` = "'. $doc_type .'"');        
        }
        
        $result['availableAuthors'] = $this->db->query("SELECT `user_id`, `user_fname`, `user_name`, `user_lname` FROM `user` WHERE `user_type` = 'admin'");
        $result['availableTests'] = $this->db->query('SELECT `test_id`, `test_about` FROM `test`'); 
        
        return $result;                            
    }
    
    # ��������� �������� � ��
    public function saveDoc($inputDate, $doc_id) {
        
        return $this->db->Update('document', $inputDate, 'doc_id', $doc_id);
        
    }
    
    # �������� ������ ����
    public function addDoc($inputDate) {
        
        return $this->db->Insert('document', $inputDate);
        
    }
    
    # �������� ����
    public function deleteDoc($doc_id) {
        
        return $this->db->query('DELETE FROM `document` WHERE `doc_id` = '. $doc_id);
        
    }
    
    # ������ ��� ���� �� ID
    public function getDocTypeById($doc_id) {
        
        return $this->db->Pick('SELECT `doc_type` FROM `document` WHERE `doc_id` = '. (int)$doc_id, 'doc_type');
        
    }
    
    
    
    #===========================
    #===========================
    
    # ������� ������
    public function get_doc_list() {

        return $this->db->query('SELECT * FROM `document` WHERE 1');

    }
    
    public function get_doc_list_id($id = '0') {

        //print_r($this->db->query('SELECT * FROM `menu` WHERE menu_pid = '.$id));
        return $this->db->query('SELECT * FROM `document` WHERE doc_pid = '.$id.' ORDER BY doc_number;');

    }
    
    public function del_doc($id) {

        return $this->db->query('DELETE FROM `document` WHERE doc_id = '.$id.';');

    }     
    
    public function get_doc($id) {

        return $this->db->query('SELECT * FROM `document` WHERE doc_id = '.$id.';');

    }
        
    public function set_doc($item){
       /* $data = array('ans_fuser' => $fuser,
                      'ans_tuser' => $tuser,
                      'ans_theme' => $theme,
                      'ans_ask'   => $ask,
                      'ans_dask'  => date('Y-m-d H:i:s')
        );*/
        //return $this->db->insert('group', $item); 
        //print_r($item);menu_id    menu_pid    menu_number    color    menu_title    menu_url    menu_type
        $qwer = "INSERT INTO `document` (
                `doc_id` ,
                `doc_pid` ,
                `doc_title`,
                `doc_content`,
                `doc_author`,
                `doc_cdate`,
                `doc_updater`,
                `doc_udate`,
                `doc_number`,
                `doc_test`
            )
            VALUES (
                NULL,
                '".$item['doc_pid']."',
                '".$item['doc_title']."',
                '".$item['doc_content']."',
                '".$item['doc_author']."',
                '".$item['doc_cdate']."',
                '0',
                '0',
                '".$item['doc_number']."',
                '".$item['doc_test']."'        
            )";
        //print $qwer;
        return $this->db->query($qwer);    
    }   
    
    public function edit_doc($item) {
        if (!empty($item))
        {
        $qw = "UPDATE `document` 
        SET  
            `doc_pid`  =  '".$item['doc_pid']."',
            `doc_title`  =  '".$item['doc_title']."',
            `doc_content`  =  '".$item['doc_content']."',
            `doc_updater`  =  '".$item['doc_updater']."',
            `doc_udate`  =  '".$item['doc_udate']."',
            `doc_number`  =  '".$item['doc_number']."',
            `doc_test`  =  '".$item['doc_test']."'";
        $qw .= "WHERE `doc_id` = ".$item['doc_id'].";
        ";
        //print $qw;
        return $this->db->query($qw);
        }
        
        
    }
}

?>