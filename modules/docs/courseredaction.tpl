{* Админская часть - редактирование документа курс *}
    <link type="text/css" href="/css/datapicker/jquery-ui-1.8.18.custom.css" rel="stylesheet" /> 
    <script type="text/javascript" src="/js/docs/script.js"></script>
    
    {* ДатаПикер *}
    <script src="/js/datapicker/jquery-ui-1.8.18.custom.min.js" type="text/javascript"></script>
    <script src="/js/datapicker/jquery.ui.datepicker-ru.js" type="text/javascript"></script>
    
    {* Плавающее меню *}
    <script type="text/javascript" src="/js/docmenufloater/jquery.hoverintent.minified.js"></script>
    <script type="text/javascript" src="/js/docmenufloater/jquery.floater.1.2.js"></script>
    
    {* ВИЗУАЛЬНЫЙ РЕДАКТОР *}
    <script type="text/javascript" src="/js/tiny_mce/jquery.tinymce.js"></script>
	<script src="/js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php" type="text/javascript"></script>
        
{* /Админская часть - редактирование документа курс *}
<div id = "errmessage"></div>

<p>
    <label>&nbsp;</label>
    
    {if $course.doc_id}
        <input type="button" class="btn" value="Сохранить" onClick="saveCourse();" />        
        <input type="button" class="btn" value="Удалить" onClick="delCourse();" />
    {else}
        <input type="button" class="btn" value="Создать" onClick="newCourse();" />
        <input type="button" class="btn" value="Назад" onClick="javascript:history.back();"/> 
    {/if}
</p>
<br/>
<hr>
<br/> 
<div class="wrapper">          
    <form id="form" class="blocks"> 
    <input type="hidden" id="doc_id" name="doc_id" value="{$course.doc_id}"/> 
    <input type="hidden" id="doc_updater" name="doc_updater" value="{$doc_updater}"/>      
        <p>
            <label>Порядковый номер:</label>
            <input type="text" class="text" name="doc_number" id="doc_number" value="{$course.doc_number}" style="width: 50px;"/>
        </p>
        <p>
            <label>Родитель:</label>            
            <select name="doc_pid" class="select" id="doc_pid" style="width: 600px;">
                <option value="0" class="line">Отсутствует</option> 
                {foreach $course.availableParents as $parent}
                    <option value="{$parent.doc_id}" class="line" {if $parent.doc_id == $course.doc_pid}selected{/if}>{$parent.doc_title}</option>
                {/foreach}                
            </select>
        </p>
        <p>
            <label>Тип документа:</label>            
            <select name="doc_type" class="select" id="doc_type" style="width: 220px;" {if !$course.doc_id}disabled{/if}>
            {if $course.doc_id}                 
                <option value="doc" class="line" {if $course.doc_type == 'doc'}selected{/if}>Курс</option>
                <option value="method" class="line" {if $course.doc_type == 'method'}selected{/if}>Методическая литература</option>
                <option value="cw" class="line" {if $course.doc_type == 'cw'}selected{/if}>Контрольная</option> 
            {else}
                {if $method == 'doc'}
                    <option value="doc" class="line">Курс</option>
                {elseif $method == 'method'}
                    <option value="method" class="line">Методическая литература</option>
                {elseif $method == 'cw'}
                    <option value="cw" class="line">Контрольная</option>
                {/if}            
            {/if}
            </select>
        </p>
        <p>
            <label>Заголовок *:</label>
            <input type="text" class="text" name="doc_title" id="doc_title" value="{$course.doc_title}" style="width: 600px;"/>
        </p>
        <p>
            
            <textarea name="doc_content" id="doc_content" style="height: 350px;">{$course.doc_content}</textarea>
        </p>
        <p>
            <label>Автор:</label>            
            <select name="doc_author" class="select" id="doc_author">
                {foreach $course.availableAuthors as $author}
                    <option value="{$author.user_id}" class="line" {if $author.user_id == $course.doc_author}selected{/if}>{$author.user_fname} {$author.user_name|truncate:1:'':true}. {$author.user_lname|truncate:1:'':true}.</option>
                {/foreach}
            </select>
        </p>
        <p>
            <label>Дата создания *:</label>
            <input type="text" class="text" name="doc_cdate" id="doc_cdate" value="{$course.doc_cdate}" style="width: 90px;"/>
        </p>
        <p>
            <label>Дата обновления *:</label>
            <input type="text" class="text" name="doc_udate" id="doc_udate" value="{$course.doc_udate}" style="width: 90px;"/>
        </p>
        <p>
            <label>Тест:</label>            
            <select name="doc_test" class="select" id="doc_test" style="width: 600px;">
                <option value="0" class="line">Отсутствует</option>
                {foreach $course.availableTests as $test}
                    <option value="{$test.test_id}" class="line" {if $test.test_id == $course.doc_test}selected{/if}>{$test.test_about}</option>
                {/foreach}                
            </select>
        </p></div>          
        <br/>
        <div id = "errmessage2"></div>
        <hr>
        <p>
            <label>&nbsp;</label>     
            {if $course.doc_id}
                <input type="button" class="btn" value="Сохранить" onClick="saveCourse();" />        
                <input type="button" class="btn" value="Удалить" onClick="delCourse();" />
            {else}
                <input type="button" class="btn" value="Создать" onClick="newCourse();" />
                <input type="button" class="btn" value="Назад" onClick="javascript:history.back();"/> 
            {/if}
        </p>
    </form>

    {if $course.closepages}
    {* Дополнительное меню. Отображает страницы раздела *} 
        <div id="chaptermenu">
            <ul class="nav-main">
                {foreach $course.closepages as $p}
                    <li>
                        <a href="/docs/redaction/{$p.doc_id}" title="{$p.doc_title}">
                            <span>                                
                                {$p.doc_title|truncate:20:'..':true}                                    
                            </span>
                        </a>
                    </li>
                {/foreach}
                    <li><hr></li>
                    <li><a href="/docs/panel/{$method}"><span><b><u>Оглавление</u></b></span></a></li>
            </ul>
            <div class="dc-corner"><span></span></div>
        </div>
    {*/ Дополнительное меню *}
    {/if}

<div class="clear"></div>
</div>
