{if $students}
    <table id="table">
    <tr>
        <th>№ п.п.</th>
        <th>Фамилия</th>
        <th>Имя</th>
        <th>Отчество</th>
        <th>Номер в журнале</th>
        <th>Регистрация</th>
    </tr>
    {assign var=number value=0}
    {foreach $students as $s}
        {$number = $number + 1}
        <tr{if $number % 2 == 0} class="alt"{/if}>
            <td style="text-align:center;">{$number}</td>
            <td>{$s.user_fname}</td>
            <td>{$s.user_name}</td>
            <td>{$s.user_lname}</td>
            <td style="text-align:center;">{$s.user_number}</td>
            <td style="text-align:center;">{$s.user_dregistration}</td>
        </tr>
    {/foreach}
    </table>
{else}
    <p>Список пока пуст..</p>
{/if}
