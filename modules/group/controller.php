<?php

class controller_group extends controller {

    public function action_index() {    
        
        $logged = $this->core->global['auth']->logged;     
        
        if ($logged) 
        {
                      
            
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }

        $this->render();   

    }
    
     public function action_showlist() {    
        
        $logged = $this->core->global['auth']->logged;     
        
        if ($logged) 
        {                          
            $model = LoadModel('group');
            $this->view = 'group/list.tpl';
            
            $group_id = htmlspecialchars($this->core->path[0]);
            $group = $model->get_group_info($group_id);             
            
            if (!$group)
            {
                // не нашли
                $this->data['title'] = 'Требуемая группа не найдена!';
                
            } 
            else
            {            
                $this->data['title'] = 'Список группы № ' . $group['group_code'];
                $this->data['students'] = $model->get_list($group_id);
            }
            
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }

        $this->render();   

    }
		public function action_show() {

		$model = LoadModel('group');
        
		$model_sp = LoadModel('specialty');
		
        $this->view = 'group/show.tpl';
		
		$logged = $this->core->global['auth']->logged;
        
		$this->data['title'] = "Группы";
		
        if ($logged) 
        {
            $this->data['err'] = $this->req['err'];
            $this->data['sendok'] = $this->req['sendok'];
            
            $this->data['group'] = $model->get_group_list_print();
            $this->data['specialty'] = $model_sp->get_specialty_list();
            //$this->data['teachers']	    = $model->get_teachers_list(); 
            //$this->data['user']         = $this->core->global['auth']->user;
        }

		$this->render();   

	}
	
	public function action_delete() {

		$model = LoadModel('group');
		$del = $model->del_group($_POST['id']);
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		print $_POST['id'];
		die();
	}
	
	public function action_get() {

		$model = LoadModel('group');
		//$_GET['id'] = 1;
		$usr = $model->get_group($_GET['id']);
		$usr = $usr[0];
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		//print_r($usr);
		print "{\r\n";
		foreach ($usr as $key => $value)
			print "	\"".$key."\": \"".$value."\",\r\n";
		
		print "	\"error\": \"0\"\r\n";
/*		print "	\"user_type\": \"".$usr['user_type']."\",\r\n";
		print "	\"user_name\": \"".$usr['user_name']."\",\r\n";
		print "	\"user_fname\": \"".$usr['user_fname']."\",\r\n";
		print "	\"user_lname\": \"".$usr['user_lname']."\",\r\n";
		print "	\"user_group\": \"".$usr['user_group']."\",\r\n";
		print "	\"user_number\": \"".$usr['user_number']."\",\r\n";
		print "	\"user_dregistration\": \"".$usr['user_dregistration']."\",\r\n";
		print "	\"user_ldate\": \"".$usr['user_ldate']."\"\r\n";*/
		print "}\r\n";
		die();
	}
	
	public function action_add() {

		$model = LoadModel('group');
		print $_GET['group_id'].'--<br>';
		$iferror = false;
		$item=array();
		print '{';
		print '"error": {';
		$item['group_code'] = $_GET['group_code'];
		$item['group_specialty'] = $_GET['group_specialty'];
		$item['group_cstudents'] = $_GET['group_cstudents'];
		$item['group_cdate'] = date('Y-m-d');
		
		
		if (empty($_GET['group_id']))
		{
			/*if ($model->valid_num($_GET['group_code']) > 0)
			{
				print '"num": "1"';
				$iferror = true;
			}
			else
				print '"num": "0"';
			*/	
		}
		else
		{
			$item['group_id'] = $_GET['group_id'];
			
		}
		
		print '}';
		print '}';
		//print_r($item);
		
		if (!$iferror)
		{
			if (empty($_GET['group_id']))
			{
				$model->set_group($item);
				print 'ok';
			}
			else
			{
				$model->edit_group($item);
				print 'ok2';
			}
		}
		//if (empty($_POST['id']))
			//print 'add';
		//$del = $model->del_user($_POST['id']);
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		//print $_POST['id'];
		die();
	}
	
	public function action_getlist() {

		$model = LoadModel('group');
		//$_POST['id'] = 2;
		$items = $model->get_group_list_print();
		//$usr = $usr[0];
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		//print_r($usr);
		print "<table class='item_table'>
		<tr>
			<th>№</th>
			<th>Номер</th>
			<th>Колличество студентов</th>
			<th>Специальность</th>
			<th>Действия</th>
		</tr>";
		foreach ($items as $item)
		{
			print "			
		<tr id_itm='".$item['group_id']."'>
			<td>".$item['group_id']."</td>
			<td>".$item['group_code']."</td>
			<td>".$item['group_cstudents']."</td>
			<td>".$item['spec_name']."</td>
			<td><span class='del' >Удалить</span></td>
		</tr>";
		}
		print "</table>";
		die();
	}



}


?>