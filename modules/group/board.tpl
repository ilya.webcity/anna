{if $logged}
{if $err}<div class="errmessage">{$err}</div>{/if} 
{if $sendok}<div class="okmessage">{$sendok}</div>{/if}

<div id="page-wrap">
{foreach $questions as $q}

    <dl class="faq">
        <dt><a class="date">{$q.ans_dask}</a> - {$q.ans_ask}</dt>
        <dd class="answer">
            <div>
                <p>Отвечает <i><u>{$q.user_fname} {$q.user_name|truncate:1:'':true}. {$q.user_lname|truncate:1:'':true}.</u></i></p>
                <p>{$q.ans_text}</p>
            </div>
        </dd>
    </dl> 	
{foreachelse} 

<p>Нет вопросов</p>
{/foreach}
<div class="clear"></div>
<br/>
<hr/>
<h3>Я хочу задать вопрос</h3>
<div class="wrapper">		
	<form id="form" class="blocks" action="/faq/send" method="post">
		<p>
			<label>Преподаватель:</label>			
			<select name="teacher" class="select">
				{foreach $teachers as $t}
					<option value="{$t.user_id}" class="line">{$t.user_fname} {$t.user_name|truncate:1:'':true}. {$t.user_lname|truncate:1:'':true}.</option>
				{/foreach}
			</select>
		</p>
		<p>
			<label>Имя:</label>
			<input type="text" class="text" name="name" value="{$user.user_fname} {$user.user_name|truncate:1:'':true}. {$user.user_lname|truncate:1:'':true}." disabled/>
		</p>
		<p>
			<label>Группа:</label>
			<input type="text" class="text" name="group" value="{$user.user_group_code}" disabled/>
		</p>
		<p>
			<label>Тема (кратко):</label>
			<input type="text" class="text" name="theme" />
		</p>		
		<p class="area">
			<label>Вопрос:</label>
			<textarea class="textarea" name="message"></textarea>
		</p>
		<p>
			<label>&nbsp;</label>
			<input type="submit" class="btn" value="Задать" />
		</p>
	</form>
</div>
<div class="clear"></div>
</div>


{else}
    <p class="NotAuthorised">Вы не авторизованы!!!</p>
{/if}
