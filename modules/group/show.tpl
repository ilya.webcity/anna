{if $logged}
{if $err}<div class="errmessage">{$err}</div>{/if} 
{if $sendok}<div class="okmessage">{$sendok}</div>{/if}
<script type="text/javascript" src="/js/group.js"></script>

<div id='items_div'>
<table class='item_table'>
		<tr>
			<th>№</th>
			<th>Номер</th>
			<th>Колличество студентов</th>
			<th>Специальность</th>
			<th>Действия</th>
		</tr>
{foreach $group as $item}
		<tr id_itm='{$item.group_id}'>
			<td>{$item.group_id}</td>
			<td>{$item.group_code}</td>
			<td>{$item.group_cstudents}</td>
			<td>{$item.spec_name}</td>
			<td><span class='del' >Удалить</span></td>
		</tr>
{/foreach}
</table>
</div>

<table class='item_add' align=center>
	<tr>
		<th align=right>Параметр</th>
		<th align=left>Значение</th>
	</tr>
	<tr id='tr_id'>
		<td align=right>Id</td>
		<td align=left ></td>
	</tr>
	<tr id='tr_group_code'>
		<td align=right>Номер *</td>
		<td align=left><input name='group_code' type='text' size=10></td>
	</tr>
	<tr id='tr_group_specialty'>
		<td align=right>Специальность *</td>
		<td align=left>
			<select name='group_specialty'>
				{foreach from=$specialty item=item}
				<option value='{$item.spec_id}'>{$item.spec_name}</option>
				{/foreach}
			</select>
		</td>
	</tr>
	<tr id='tr_group_cstudents'>
		<td align=right>Колличество студентов *</td>
		<td align=left><input name='group_cstudents' type='text' size=10></td>
	</tr>
	<tr id='tr_group_cdate'>
		<td align=right>Создана</td>
		<td align=left></td>
	</tr>
	<tr id='tr_usr_subit'>
		<td align=right><input name='user_add' value='Новый' type='submit' size=3></td>
		<td align=left><input name='user_edit' value='Сохранить' type='submit' size=3><input name='user_del' value='Удалить' type='submit' size=3></td>
	</tr>
	
</table>

<div class="clear"></div>
</div>


{else}
    <p class="NotAuthorised">Вы не авторизованы!!!</p>
{/if}
