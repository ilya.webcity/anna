<?php

class model_group extends model {     

	public function get_group_info($user_id) {
        
        $group = $this->db->Pick('SELECT `group_id`, `group_cdate`, `group_code`, `group_cstudents` FROM `group` JOIN `user` ON user_group = group_id WHERE `user_id` = ' . (int)$user_id);
        
        if ($group)
        {
            $group['group_cdate'] = GetFormatedDate($group['group_cdate']);
        }          
        
        return $group;
    }
    
    public function get_list($group_id) {        
    
        $list = $this->db->query('SELECT * FROM `user` WHERE `user_group` = ' . (int)$group_id . ' ORDER BY `user_fname`');
        
        if ($list)
        {
            foreach ($list as $k => $v)
            {
                $list[$k]['user_dregistration'] = GetFormatedDate($list[$k]['user_dregistration']);
            }            
        }
        
        return $list;
        
    }
	
	public function get_group_list() {

		return $this->db->query('SELECT * FROM `group` WHERE 1');

	}
	public function get_group_list_print() {

		return $this->db->query('SELECT * FROM `group`, `specialty` WHERE group_specialty = spec_id');

	}
	
	public function del_group($id) {

		return $this->db->query('DELETE FROM `group` WHERE group_id = '.$id.';');

	}
	
	public function valid_num($num) {

	return $this->db->query('SELECT COUNT(group_id) FROM `group` WHERE group_code = '.$num.';');

	}
	
	public function get_group($id) {
//print 'SELECT * FROM `group` WHERE group_id = '.$id.';';
		return $this->db->query('SELECT * FROM `group` WHERE group_id = '.$id.';');

	}
	
    
    public function set_group($item)
    {
       /* $data = array('ans_fuser' => $fuser,
                      'ans_tuser' => $tuser,
                      'ans_theme' => $theme,
                      'ans_ask'   => $ask,
                      'ans_dask'  => date('Y-m-d H:i:s')
        );*/
        //return $this->db->insert('group', $item); 
		//print_r($item);
		$qwer = "INSERT INTO `group` (
				`group_id` ,
				`group_cdate` ,
				`group_specialty` ,
				`group_code` ,
				`group_cstudents`
			)
			VALUES (
				NULL, '".$item['group_cdate']."',  '".$item['group_specialty']."',  '".$item['group_code']."',  '".$item['group_cstudents']."'		
			)";
		//print $qwer;
		return $this->db->query($qwer);    
    }

	
	public function edit_group($item) {
		if (!empty($item))
		{
		$qw = "UPDATE `group` 
		SET  
			`group_specialty`  =  '".$item['group_specialty']."',
			`group_code`  =  '".$item['group_code']."',
			`group_cstudents`  =  '".$item['group_cstudents']."'";
		$qw .= "WHERE  `group_id` = ".$item['group_id'].";
		";
		return $this->db->query($qw);
		}
		
		
	}
    
}

?>