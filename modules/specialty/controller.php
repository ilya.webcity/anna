﻿<?php

class controller_specialty extends controller {

	public function action_show() {

		$model = LoadModel('specialty');
        
        $this->view = 'specialty/show.tpl';
		
		$logged = $this->core->global['auth']->logged;
        
		$this->data['title'] = "Специальности";
		
        if ($logged) 
        {
            $this->data['err'] = $this->req['err'];
            $this->data['sendok'] = $this->req['sendok'];
            
            $this->data['specialty'] = $model->get_specialty_list();
            //$this->data['teachers']	    = $model->get_teachers_list(); 
            //$this->data['user']         = $this->core->global['auth']->user;
        }

		$this->render();   

	}
	
	public function action_delete() {

		$model = LoadModel('specialty');
		$del = $model->del_specialty($_POST['id']);
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		print $_POST['id'];
		die();
	}
	
	public function action_get() {

		$model = LoadModel('specialty');
		//$_GET['id'] = 1;
		$usr = $model->get_specialty($_GET['id']);
		$usr = $usr[0];
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		//print_r($usr);
		print "{\r\n";
		foreach ($usr as $key => $value)
			print "	\"".$key."\": \"".$value."\",\r\n";
		
		print "	\"error\": \"0\"\r\n";
/*		print "	\"user_type\": \"".$usr['user_type']."\",\r\n";
		print "	\"user_name\": \"".$usr['user_name']."\",\r\n";
		print "	\"user_fname\": \"".$usr['user_fname']."\",\r\n";
		print "	\"user_lname\": \"".$usr['user_lname']."\",\r\n";
		print "	\"user_group\": \"".$usr['user_group']."\",\r\n";
		print "	\"user_number\": \"".$usr['user_number']."\",\r\n";
		print "	\"user_dregistration\": \"".$usr['user_dregistration']."\",\r\n";
		print "	\"user_ldate\": \"".$usr['user_ldate']."\"\r\n";*/
		print "}\r\n";
		die();
	}
	
	public function action_add() {

		$model = LoadModel('specialty');
		print $_GET['spec_id'].'--<br>';
		$iferror = false;
		$item=array();
		print '{';
		print '"error": {';
		$item['spec_name'] = $_GET['spec_name'];
		$item['spec_about'] = $_GET['spec_about'];
		
		
		if (empty($_GET['spec_id']))
		{
			/*if ($model->valid_num($_GET['group_code']) > 0)
			{
				print '"num": "1"';
				$iferror = true;
			}
			else
				print '"num": "0"';
			*/	
		}
		else
		{
			$item['spec_id'] = $_GET['spec_id'];
		}
		
		print '}';
		print '}';
		//print_r($item);
		
		if (!$iferror)
		{
			if (empty($_GET['spec_id']))
			{
				$model->set_specialty($item);
				print 'ok';
			}
			else
			{
				$model->edit_specialty($item);
			}
		}
		//if (empty($_POST['id']))
			//print 'add';
		//$del = $model->del_user($_POST['id']);
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		//print $_POST['id'];
		die();
	}
	
	public function action_getlist() {

		$model = LoadModel('specialty');
		//$_POST['id'] = 2;
		$items = $model->get_specialty_list();
		//$usr = $usr[0];
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		//print_r($usr);
		print "<table class='item_table'>
		<tr>
			<th>№</th>
			<th>Название</th>
			<th>Действия</th>
		</tr>";
		foreach ($items as $item)
		{
			print "			
		<tr id_itm='".$item['spec_id']."'>
			<td>".$item['spec_id']."</td>
			<td>".$item['spec_name']."</td>
			<td><span class='del' >Удалить</span></td>
		</tr>";
		}
		print "</table>";
		die();
	}




}


?>