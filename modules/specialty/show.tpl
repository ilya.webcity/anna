{if $logged}
{if $err}<div class="errmessage">{$err}</div>{/if} 
{if $sendok}<div class="okmessage">{$sendok}</div>{/if}
<script type="text/javascript" src="/js/specialty.js"></script>

<div id='items_div'>
<table class='item_table'>
		<tr>
			<th>№</th>
			<th>Название</th>
			<th>Действия</th>
		</tr>
{foreach $specialty as $item}
		<tr id_itm='{$item.spec_id}'>
			<td>{$item.spec_id}</td>
			<td>{$item.spec_name}</td>
			<td><span class='del' >Удалить</span></td>
		</tr>
{/foreach}
</table>
</div>

<table class='item_add' align=center>
	<tr>
		<th align=right>Параметр</th>
		<th align=left>Значение</th>
	</tr>
	<tr id='tr_id'>
		<td align=right>Id</td>
		<td align=left ></td>
	</tr>
	<tr id='tr_spec_name'>
		<td align=right>Название *</td>
		<td align=left><input name='spec_name' type='text' size=10></td>
	</tr>
	<tr id='tr_spec_about'>
		<td align=right>Описание *</td>
		<td align=left>
			<textarea name='spec_about' style='width:200px; height:50px'></textarea>
		</td>
	</tr>
	<tr id='tr_usr_subit'>
		<td align=right><input name='user_add' value='Новый' type='submit' size=3></td>
		<td align=left><input name='user_edit' value='Сохранить' type='submit' size=3><input name='user_del' value='Удалить' type='submit' size=3></td>
	</tr>
	
</table>

<div class="clear"></div>
</div>


{else}
    <p class="NotAuthorised">Вы не авторизованы!!!</p>
{/if}
