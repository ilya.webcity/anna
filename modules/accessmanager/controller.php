<?php

class controller_accessmanager extends controller {

    public function action_index() {
        
        

    }
               
    #Отображает страницу управления доступом к документам
    public function action_access() {

        $logged = $this->core->global['auth']->logged;
        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('accessmanager');

                $this->view = 'accessmanager/panel.tpl';
                $this->data['title'] = "<hr>Доступ к <a href='/accessmanager/access/doc'>документам</a> | <a href='/accessmanager/access/conf'>конференциям</a> | <a href='/accessmanager/access/announ'>объявлениям</a><hr>";
                
                if ($this->path[0] == 'doc')
                {
                    $this->data['title_page'] = 'Доступ к документам';
                    $this->data['doc'] = 1;
                }
                else if ($this->path[0] == 'conf')
                {
                    $this->data['title_page'] = 'Доступ к конференциям';
                    $this->data['conf'] = 1;    
                }
                else if ($this->path[0] == 'announ')
                {
                    $this->data['title_page'] = 'Доступ к объявлениям';
                    $this->data['annon'] = 1;    
                }

            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            } 
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }
        $this->render();
    }

    #==============
    # ДОКУМЕНТЫ !!!
    #==============
    
    # Получает список всех документов и специальностей (группировка по документу)
    public function action_getDocs() {
        
        $logged = $this->core->global['auth']->logged;
        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('accessmanager');
                $result = $model->getDocs();               
                
                json($result);
            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            }
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }
        $this->render();        
    }
    
     # Получает список всех документов и специальностей (группировка по специальностям) 
    public function action_getSpecDocs() {
    
        $logged = $this->core->global['auth']->logged;
        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('accessmanager');
                $result = $model->getSpecDocs();               
                
                json($result);
            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            }
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }
        $this->render();
        
    }
    
    # Получаем данные по доступу к конкретному доку
    public function action_OpenDoc() {
        
        $logged = $this->core->global['auth']->logged;
        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('accessmanager');
                $doc_id = (int)$this->req['doc_id'];
                $result = $model->OpenDoc($doc_id);       
                
                json($result);
            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            }
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }
        $this->render();     
        
    }
    
    # Удаляет специальность из документа
    public function action_deleteSpec() {
     
        $logged = $this->core->global['auth']->logged;
        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('accessmanager');
                $doc_id = (int)$this->req['doc_id'];
                $spec_id = (int)$this->req['spec_id'];
                $result = $model->deleteSpec($doc_id, $spec_id);       
                
                json($result);
            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            }
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }
        $this->render();     
           
        
        
    }
    
    # Добавляет специальность к документа
    public function action_addSpec() {
     
        $logged = $this->core->global['auth']->logged;
        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('accessmanager');
                $doc_id = (int)$this->req['doc_id'];
                $spec_id = (int)$this->req['spec_id'];
                $result = $model->addSpec($doc_id, $spec_id);       
                
                json($result);
            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            }
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }
        $this->render();         
           
    }
    
    # Получаем данные по доступу к конкретной специальности
    public function action_OpenSpec() {
        
        $logged = $this->core->global['auth']->logged;
        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('accessmanager');
                $spec_id = (int)$this->req['spec_id'];
                $result = $model->OpenSpec($spec_id);       
                
                //echo "<pre>";
                //print_r($result);
                //echo "</pre>";
                //exit;
                
                json($result);
            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            }
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }
        $this->render();     
        
    }
    
    # ================
    # КОНФЕРЕНЦИЯ !!!!
    #=================
    
    # Получает список конференция - группа 
    public function action_getCGroups() {
    
        $logged = $this->core->global['auth']->logged;
        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('accessmanager');
                $result = $model->getCGroups();               
                
                json($result);
            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            }
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }
        $this->render();
        
    }
    
    # Список группа - конференция
    public function action_getGroupConfs() {
    
        $logged = $this->core->global['auth']->logged;
        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('accessmanager');
                $result = $model->getGroupConfs();               
                
                json($result);
            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            }
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }
        $this->render();
            
    }
    
    # Открываем Конференцию
    public function action_OpenConf() {
        
        $logged = $this->core->global['auth']->logged;
        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('accessmanager');
                $conf_id = (int)$this->req['conf_id'];
                $result = $model->OpenConf($conf_id);       
                
                json($result);
            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            }
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }
        $this->render();     
        
    }
    
    # Открываем Группа (конференция)
    public function action_OpenCGroup() {
        
        $logged = $this->core->global['auth']->logged;
        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('accessmanager');
                $group_id = (int)$this->req['group_id'];
                $result = $model->OpenCGroup($group_id);       
                
                json($result);
            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            }
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }
        $this->render();     
        
    }
    
    # Удаляем ДОСТУП
    public function action_deleteCR() {
     
        $logged = $this->core->global['auth']->logged;
        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('accessmanager');
                $conf_id = (int)$this->req['conf_id'];
                $group_id = (int)$this->req['group_id'];
                $result = $model->deleteCR($conf_id, $group_id);       
                
                json($result);
            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            }
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }
        $this->render(); 
                   
    }       
    
    # Добавляем ДОСТУП
    public function action_addCR() {
     
        $logged = $this->core->global['auth']->logged;
        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('accessmanager');
                $conf_id = (int)$this->req['conf_id'];
                $group_id = (int)$this->req['group_id'];
                $result = $model->addCR($conf_id, $group_id);       
                
                json($result);
            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            }
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }
        $this->render(); 
                   
    }
    
    
    # ================
    # ОБЪЯВЛЕНИЯ !!!!
    #=================
    
    # Получает список обявление - группы 
    public function action_getAGroups() {
    
        $logged = $this->core->global['auth']->logged;
        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('accessmanager');
                $result = $model->getAGroups();               
                
                json($result);
            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            }
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }
        $this->render();
        
    }
    
    # Список группа - объявления 
    public function action_getGroupAnnouns() {
        
        $logged = $this->core->global['auth']->logged;
        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('accessmanager');
                $result = $model->getGroupAnnouns();               
                
                json($result);
            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            }
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }
        $this->render();
        
    }
        
    # Открываем объявление
    public function action_OpenAnn() {
        
        $logged = $this->core->global['auth']->logged;
        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('accessmanager');
                $ann_id = (int)$this->req['ann_id'];
                $result = $model->OpenAnn($ann_id);       
                
                json($result);
            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            }
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }
        $this->render();     
        
    }
    
    # Открываем Группу
    public function action_OpenAGroup() {
        
        $logged = $this->core->global['auth']->logged;
        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('accessmanager');
                $group_id = (int)$this->req['group_id'];
                $result = $model->OpenAGroup($group_id);       
                
                json($result);
            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            }
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }
        $this->render();     
        
    }
     
    # Удаляем ДОСТУП
    public function action_deleteAR() {
     
        $logged = $this->core->global['auth']->logged;
        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('accessmanager');
                $ann_id = (int)$this->req['ann_id'];
                $group_id = (int)$this->req['group_id'];
                $result = $model->deleteAR($ann_id, $group_id);       
                
                json($result);
            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            }
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }
        $this->render(); 
                   
    }       
    
    # Добавляем ДОСТУП
    public function action_addAR() {
     
        $logged = $this->core->global['auth']->logged;
        if ($logged)
        {
            $user_type = $this->core->global['auth']->user['user_type'];

            if ($user_type == 'admin')
            {
                // это админ, все ок
                $model = LoadModel('accessmanager');
                $ann_id = (int)$this->req['ann_id'];
                $group_id = (int)$this->req['group_id'];
                $result = $model->addAR($ann_id, $group_id);       
                
                json($result);
            }
            else
            {
                // Несанкционированный доступ
                $this->data['title'] = 'Отказано в доступе';
                $this->view = 'notauthpage.tpl';
            }
        } else {
            $this->data['title'] = 'Ошибка авторизации';
            $this->view = 'notauthpage.tpl';
        }
        $this->render(); 
                   
    }
    
}


?>