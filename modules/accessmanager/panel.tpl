{* Админская часть - права доступа *}
    <script type="text/javascript" src="/js/accessmanager/script.js"></script>    
{* /Админская часть - права доступа *}

<div class = "admin_accessmanager">
    <p><h3>
        {if $doc}
            <a href="javascript:void(0)" onClick ="docs();">Документы</a> | 
            <a href="javascript:void(0)" onClick ="specDocs();">Специальности</a>
        {elseif $conf}
            <a href="javascript:void(0)" onClick ="c_groups();">Конференции</a> | 
            <a href="javascript:void(0)" onClick ="groupConfs();">Группы</a>
        {elseif $annon}
            <a href="javascript:void(0)" onClick ="a_groups();">Объявления</a> | 
            <a href="javascript:void(0)" onClick ="groupAnnouns();">Группы</a>            
        {/if}
    </h3></p>
    <div id = "errmessage" visible="false"></div>
    <div class = "accessConsole"></div>
    <div class="clear"></div> 
</div>