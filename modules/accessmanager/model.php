<?php

class model_accessmanager extends model {

    #=========================
    #       ДОКУМЕНТЫ
    #=========================
    
    # Получить список доков и спец (групп по док)
    public function getDocs() {
    
        $query = 'SELECT DISTINCT `doc_title` AS `field1`, `doc_id` as `id` FROM `document` ORDER BY `doc_id`';

        $result = $this->db->query($query);
        
        if ($result)
        {
            foreach ($result as &$v)
            {
                $rd_doc = (int)$v['id'];
                 
                $q = 'SELECT `spec_name` AS `title` FROM `rights_document` JOIN `specialty` ON rd_specialty = spec_id WHERE `rd_doc` = ' . $rd_doc . ' ORDER BY `spec_id`';
                $v['field2'] = $this->db->query($q); 
            }    
        }
        return $result;
        
    }

    # Получить данные доступа к конкретному доку
    public function OpenDoc($doc_id) {
        
        $query = 'SELECT `doc_title`, `doc_id` FROM `document` WHERE `doc_id` = '. $doc_id;
        
        $result = $this->db->query($query);
        $result = $result[0];
        
        $qOK = 'SELECT `spec_name`, `spec_id` FROM `rights_document` JOIN `specialty` ON spec_id = rd_specialty WHERE `rd_doc` = '. $doc_id;    
        $result['accessok'] = (array)$this->db->query($qOK);
        
        $str = '';
        // cформируем список айдишников
        if (sizeof($result['accessok']) != 0)
        {
            $str = ' WHERE `spec_id` NOT IN (';
            foreach ($result['accessok'] as $v)
            {
                $str = $str . '' . $v['spec_id'] . ', ';    
            } 
            $str = trim($str);
            $str = trim($str, ',');
            
            $str = $str . ')';                  
        }
        
        $qERR = 'SELECT DISTINCT `spec_name`, `spec_id` FROM `specialty`'. $str;    
        $result['accesserr'] = $this->db->query($qERR);
        
        return $result;
        
    }
    
    # Удаляем специальность из доступа к конкретному доку
    public function deleteSpec($doc_id, $spec_id) {
        
        $query = 'DELETE FROM `rights_document` WHERE `rd_doc` = '. $doc_id .' AND `rd_specialty` = '. $spec_id;
        $result = $this->db->query($query);
        
        return $result;        
        
    }
    
    # Добавляем специальность для доступа к конкретному доку
    public function addSpec($doc_id, $spec_id) {
        
        $inputAr = array(
           'rd_doc' => $doc_id,
           'rd_specialty' => $spec_id
        );
        
        // проверим, нет ли такого доступа уже
        $res = $this->db->Pick('SELECT COUNT(*) AS `count` FROM `rights_document` WHERE `rd_specialty` = '. $spec_id .' AND `rd_doc` = '. $doc_id, 'count');
        if ($res > 0) 
        {
            return false;
        }    
            
        $result = $this->db->Insert('rights_document', $inputAr);
        
        return $result;        
        
    }
    
    # Получить список доков и спец (групп по спецухе)
    public function getSpecDocs() {
        
        $query = 'SELECT DISTINCT `spec_name` AS `field1`, `spec_id` as `id` FROM `specialty` ORDER BY `spec_id`';

        $result = $this->db->query($query);
        
        if ($result)
        {
            foreach ($result as &$v)
            {
                $rd_spec = (int)$v['id'];
                 
                $q = 'SELECT `doc_title` AS `title` FROM `rights_document` JOIN `document` ON rd_doc = doc_id WHERE `rd_specialty` = ' . $rd_spec . ' ORDER BY `doc_id`';
                $v['field2'] = $this->db->query($q); 
            }    
        }
        return $result;    
        
    }
    
    # Получить данные доступа к конкретной спецухе
    public function OpenSpec($spec_id) {
        
        $query = 'SELECT `spec_name`, `spec_id` FROM `specialty` WHERE `spec_id` = '. (int)$spec_id;
        
        $result = $this->db->query($query);
        $result = $result[0];
               
        $qOK = 'SELECT `doc_title`, `doc_id` FROM `rights_document` JOIN `document` ON doc_id = rd_doc WHERE `rd_specialty` = '. $spec_id;    
        $result['accessok'] = (array)$this->db->query($qOK);
        
        $str = '';
        // cформируем список айдишников
        if (sizeof($result['accessok']) != 0)
        {
            $str = ' WHERE `doc_id` NOT IN (';
            foreach ($result['accessok'] as $v)
            {
                $str = $str . '' . $v['doc_id'] . ', ';    
            } 
            $str = trim($str);
            $str = trim($str, ',');
            
            $str = $str . ')';                  
        }
        
        $qERR = 'SELECT DISTINCT `doc_title`, `doc_id` FROM `document`'. $str;    
        $result['accesserr'] = $this->db->query($qERR);
        
        return $result;
        
    }
    
    #=========================
    #       КОНФЕРЕНЦИИ
    #=========================     
    
    # Получить список конфереция - группа
    public function getCGroups() {
    
        $query = 'SELECT DISTINCT `cr_title` AS `field1`, `cr_id` as `id` FROM `conference_room` ORDER BY `cr_id`';

        $result = $this->db->query($query);
        
        if ($result)
        {
            foreach ($result as &$v)
            {
                $cr_id = (int)$v['id'];
                 
                $q = 'SELECT `group_code` AS `title` FROM `rights_conference` JOIN `group` ON rc_group = group_id WHERE `rc_room` = ' . $cr_id . ' ORDER BY `group_id`';
                $v['field2'] = $this->db->query($q); 
            }    
        }
        return $result;        
    }
    
    # Получить список группа - конференция
    public function getGroupConfs() {
        
        $query = 'SELECT DISTINCT `group_code` AS `field1`, `group_id` as `id` FROM `group` ORDER BY `group_id`';

        $result = $this->db->query($query);
        
        if ($result)
        {
            foreach ($result as &$v)
            {
                $group_id = (int)$v['id'];
                 
                $q = 'SELECT `cr_title` AS `title` FROM `rights_conference` JOIN `conference_room` ON rc_room = cr_id WHERE `rc_group` = ' . $group_id . ' ORDER BY `cr_id`';
                $v['field2'] = $this->db->query($q); 
            }    
        }
        return $result;
            
    }
    
    # Открываем конференцию
    public function OpenConf($conf_id) {
        
        $query = 'SELECT `cr_title` AS `conf_title`, `cr_id` AS `conf_id` FROM `conference_room` WHERE `cr_id` = '. (int)$conf_id;
        
        $result = $this->db->query($query);
        $result = $result[0];
               
        $qOK = 'SELECT `group_code` AS `group_name`, `group_id` FROM `rights_conference` JOIN `group` ON group_id = rc_group WHERE `rc_room` = '. $conf_id;    
        $result['accessok'] = (array)$this->db->query($qOK);
        
        $str = '';
        // cформируем список айдишников
        if (sizeof($result['accessok']) != 0)
        {
            $str = ' WHERE `group_id` NOT IN (';
            foreach ($result['accessok'] as $v)
            {
                $str = $str . '' . $v['group_id'] . ', ';    
            } 
            $str = trim($str);
            $str = trim($str, ',');
            
            $str = $str . ')';                  
        }
        
        $qERR = 'SELECT DISTINCT `group_code` AS `group_name`, `group_id` FROM `group`'. $str;    
        $result['accesserr'] = $this->db->query($qERR);
        
        return $result;
        
    }
    
    # Открываем группу 
    public function OpenCGroup($group_id) {
        
        $query = 'SELECT `group_code` AS `group_title`, `group_id` FROM `group` WHERE `group_id` = '. (int)$group_id;
        
        $result = $this->db->query($query);
        $result = $result[0];
               
        $qOK = 'SELECT `cr_title` AS `conf_name`, `cr_id` AS `conf_id` FROM `rights_conference` JOIN `conference_room` ON cr_id = rc_room WHERE `rc_group` = '. $group_id;    
        $result['accessok'] = (array)$this->db->query($qOK);
        
        $str = '';
        // cформируем список айдишников
        if (sizeof($result['accessok']) != 0)
        {
            $str = ' WHERE `cr_id` NOT IN (';
            foreach ($result['accessok'] as $v)
            {
                $str = $str . '' . $v['conf_id'] . ', ';    
            } 
            $str = trim($str);
            $str = trim($str, ',');
            
            $str = $str . ')';                  
        }
        
        $qERR = 'SELECT DISTINCT `cr_title` AS `conf_name`, `cr_id` AS `conf_id` FROM `conference_room`'. $str;    
        $result['accesserr'] = $this->db->query($qERR);
        
        return $result;
        
    }    
        
    # Удаляем ДОСТУП
    public function deleteCR($conf_id, $group_id) {
        
        $query = 'DELETE FROM `rights_conference` WHERE `rc_room` = '. $conf_id .' AND `rc_group` = '. $group_id;
        $result = $this->db->query($query);
        
        return $result;        
        
    }
    
    # Добавляем ДОСТУП
    public function addCR($conf_id, $group_id) {
        
        $inputAr = array(
           'rc_room' => $conf_id,
           'rc_group' => $group_id
        );
        
        // проверим, нет ли такого доступа уже
        $res = $this->db->Pick('SELECT COUNT(*) AS `count` FROM `rights_conference` WHERE `rc_room` = '. $conf_id .' AND `rc_group` = '. $group_id, 'count');
        if ($res > 0) 
        {
            return false;
        }    
            
        $result = $this->db->Insert('rights_conference', $inputAr);
        
        return $result;        
        
    }
    
    
    #=========================
    #       ОБЪЯВЛЕНИЯ
    #=========================     
    
    # Получить список объявление - группы
    public function getAGroups() {
    
        $query = 'SELECT DISTINCT `ann_title` AS `field1`, `ann_id` as `id` FROM `announcement` ORDER BY `ann_id`';

        $result = $this->db->query($query);
        
        if ($result)
        {
            foreach ($result as &$v)
            {
                $ann_id = (int)$v['id'];
                 
                $q = 'SELECT `group_code` AS `title` FROM `rights_announ` JOIN `group` ON ra_group = group_id WHERE `ra_announ` = ' . $ann_id . ' ORDER BY `group_id`';
                $v['field2'] = $this->db->query($q); 
            }    
        }
        return $result;        
    }
        
    # Получить список группа - объявления
    public function getGroupAnnouns() {
        
        $query = 'SELECT DISTINCT `group_code` AS `field1`, `group_id` as `id` FROM `group` ORDER BY `group_id`';

        $result = $this->db->query($query);
        
        if ($result)
        {
            foreach ($result as &$v)
            {
                $group_id = (int)$v['id'];
                 
                $q = 'SELECT `ann_title` AS `title` FROM `rights_announ` JOIN `announcement` ON ra_announ = ann_id WHERE `ra_group` = ' . $group_id . ' ORDER BY `ann_id`';
                $v['field2'] = $this->db->query($q); 
            }    
        }
        return $result;
            
    }
    
    
    # Открываем объявление
    public function OpenAnn($ann_id) {
        
        $query = 'SELECT `ann_title` AS `ann_title`, `ann_id` AS `ann_id` FROM `announcement` WHERE `ann_id` = '. (int)$ann_id;
        
        $result = $this->db->query($query);
        $result = $result[0];
               
        $qOK = 'SELECT `group_code` AS `group_name`, `group_id` FROM `rights_announ` JOIN `group` ON group_id = ra_group WHERE `ra_announ` = '. $ann_id;    
        $result['accessok'] = (array)$this->db->query($qOK);
        
        $str = '';
        // cформируем список айдишников
        if (sizeof($result['accessok']) != 0)
        {
            $str = ' WHERE `group_id` NOT IN (';
            foreach ($result['accessok'] as $v)
            {
                $str = $str . '' . $v['group_id'] . ', ';    
            } 
            $str = trim($str);
            $str = trim($str, ',');
            
            $str = $str . ')';                  
        }
        
        $qERR = 'SELECT DISTINCT `group_code` AS `group_name`, `group_id` FROM `group`'. $str;    
        $result['accesserr'] = $this->db->query($qERR);
        
        return $result;
        
    }
    
    # Открываем группу 
    public function OpenAGroup($group_id) {
        
        $query = 'SELECT `group_code` AS `group_title`, `group_id` FROM `group` WHERE `group_id` = '. (int)$group_id;
        
        $result = $this->db->query($query);
        $result = $result[0];
               
        $qOK = 'SELECT `ann_title` AS `ann_name`, `ann_id` FROM `rights_announ` JOIN `announcement` ON ann_id = ra_announ WHERE `ra_group` = '. $group_id;    
        $result['accessok'] = (array)$this->db->query($qOK);
        
        $str = '';
        // cформируем список айдишников
        if (sizeof($result['accessok']) != 0)
        {
            $str = ' WHERE `ann_id` NOT IN (';
            foreach ($result['accessok'] as $v)
            {
                $str = $str . '' . $v['ann_id'] . ', ';    
            } 
            $str = trim($str);
            $str = trim($str, ',');
            
            $str = $str . ')';                  
        }
        
        $qERR = 'SELECT DISTINCT `ann_title` AS `ann_name`, `ann_id` FROM `announcement`'. $str;    
        $result['accesserr'] = $this->db->query($qERR);
        
        return $result;
        
    }    
       
    # Удаляем ДОСТУП
    public function deleteAR($ann_id, $group_id) {
        
        $query = 'DELETE FROM `rights_announ` WHERE `ra_announ` = '. $ann_id .' AND `ra_group` = '. $group_id;
        $result = $this->db->query($query);
        
        return $result;        
        
    }
    
    # Добавляем ДОСТУП
    public function addAR($ann_id, $group_id) {
        
        $inputAr = array(
           'ra_announ' => $ann_id,
           'ra_group' => $group_id
        );
        
        // проверим, нет ли такого доступа уже
        $res = $this->db->Pick('SELECT COUNT(*) AS `count` FROM `rights_announ` WHERE `ra_announ` = '. $ann_id .' AND `ra_group` = '. $group_id, 'count');
        if ($res > 0) 
        {
            return false;
        }    
            
        $result = $this->db->Insert('rights_announ', $inputAr);
        
        return $result;        
        
    }
    
}

?>