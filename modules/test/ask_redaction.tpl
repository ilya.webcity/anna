{* Админская часть - редактирование вопроса для теста *}
    <link type="text/css" href="/css/datapicker/jquery-ui-1.8.18.custom.css" rel="stylesheet" /> 
    <script type="text/javascript" src="/js/test/script.js"></script>    
    
    {* Плавающее меню *}
    <script type="text/javascript" src="/js/docmenufloater/jquery.hoverintent.minified.js"></script>
    <script type="text/javascript" src="/js/docmenufloater/jquery.floater.1.2.js"></script>
    
    {* ВИЗУАЛЬНЫЙ РЕДАКТОР *}
    <script type="text/javascript" src="/js/tiny_mce/jquery.tinymce.js"></script>
    <script src="/js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php" type="text/javascript"></script>
            
{* /Админская часть - редактирование вопроса для теста *}
<div id = "errmessage"></div>

<p>
    <label>&nbsp;</label>
    
    {if $ask.testask_id}
        <input type="button" class="btn" value="Сохранить" onClick="saveAsk();" />        
        <input type="button" class="btn" value="Удалить" onClick="delAsk();" />
    {else}
        <input type="button" class="btn" value="Создать" onClick="newAsk();" />          
    {/if}
    <input type="button" class="btn" value="Назад" onClick="javascript:history.back();"/>
</p>
<br/>
<hr>
<br/> 
<div class="wrapper">          
    <form id="form" class="blocks"> 
    <input type="hidden" id="testask_id" name="testask_id" value="{$ask.testask_id}"/>      
        <p>
            <label>Тип вопроса:</label>            
            <select name="testask_type" class="select" id="testask_type" disabled>                
                <option value="simple" class="line" {if $ask.testask_type == 'simple' || $type == 'simple'}selected{/if}>Обычный</option>
                <option value="pair" class="line" {if $ask.testask_type == 'pair' || $type == 'pair'}selected{/if}>Вопрос - соответствие</option>                
            </select>
        </p>        
        <p>
            <label>Принадлежит тесту:</label>            
            <select name="testask_test" class="select" id="testask_test" style="width: 220px;">                
                
                
                {foreach $ask.testsList as $t}
                    <option value="{$t.test_id}" class="line" {if $ask.testask_test == $t.test_id}selected{/if}>{$t.test_title}</option>     
                {/foreach}
                
                {if !$ask.testask_id}                
                    {* Добавление нового вопроса *}
                    <option value="{$test_id}" class="line" selected>{$test_title}</option>                     
                {/if}
                      
            </select>
        </p>                 
        <p>            
            <textarea name="testask_text" id="testask_text" style="height: 350px;">{$ask.testask_text}</textarea>
        </p>
        
        <p>
    </div>
        
        {if $ask.testask_id}
        <p><img src="/img/addtest_ico.png" style="vertical-align:middle; padding-right: 10px;"><a href="/test/addAns/{if $ask.testask_type == 'simple'}simple{else}pair{/if}/{$ask.testask_id}" title="Добавить ответ">Добавить ответ</a></p>
            <label>Ответы:</label>
            <div name="answers">
                
                {* простые вопросы *}                   
                    <ol class="docTree">
                    {foreach $ask.answers as $a}                                
                        <li class="docTree">
                            {if $ask.testask_type == 'simple'}                    
                            <a href="/test/redactionAns/simple/{$a.testans_id}"><img src="/img/{if $a.testans_right == 1}right_ans_ico{else}wrong_ans_ico{/if}.png" title="{if $a.testans_right == 1}Правильный ответ{else}Неправильный ответ{/if}" style="vertical-align:middle; padding-right: 10px;">{$a.testans_text|truncate:50:'..':true}</a>        
                            {else}
                            <a href="/test/redactionAns/pair/{$a.testans_id}">{$a.testans_text|truncate:50:'..':true}</a>       
                            {/if}
                        </li>             
                    {/foreach}
                    </ol>            
                
            </div>            
        </p> 
        {/if}       
          
        <br/>
        <div id = "errmessage2"></div>
        <hr>
        <p>
            <label>&nbsp;</label>     
            {if $ask.testask_id}
                <input type="button" class="btn" value="Сохранить" onClick="saveAsk();" />        
                <input type="button" class="btn" value="Удалить" onClick="delAsk();" />
            {else}
                <input type="button" class="btn" value="Создать" onClick="newAsk();" />          
            {/if}
            <input type="button" class="btn" value="Назад" onClick="javascript:history.back();"/>
        </p>
    </form>    

    {if $fglkf}
        <div id="chaptermenu">         
            <ul class="nav-main">
            {if $ask.closeasks}
                {* Дополнительное меню. Отображает другие вопросы текущего теста *}
                {foreach $ask.closeasks as $a} 
                    <li>
                        <a href="/test/redactionAsk/{$a.testask_id}" title="{$a.testask_text}">
                            <span>                                
                                {$a.testask_text|truncate:20:'..':true}                                    
                            </span>
                        </a>
                    </li>
                {/foreach}
                {*/ Дополнительное меню *}
            
                    <li><hr></li>
            {/if}
                    <li><a href="/test/redactionTest/{$ask.testask_test}"><span><b><u>К тесту</u></b></span></a></li>
            </ul>
            <div class="dc-corner"><span></span></div>
        </div>
     {/if}   
    

<div class="clear"></div>
</div>
