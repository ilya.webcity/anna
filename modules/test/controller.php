<?php

class controller_test extends controller {

    /*public $access = array(

        '_default' => array('student' => true, 'admin' => true),
                    

    );*/
    
    public function action_index() {
        
        $this->redirect('/');
    
    }  
    
	# Вывести список всех тестов
	public function action_list() {
		
		$model = LoadModel('test');
		$this->view = 'test/list.tpl';
		#$this->view = 'docs/doctree.tpl';
		
		$this->data['title'] = 'Список всех тестов';
		$this->data['title_page'] = $this->data['title'];
		
		$this->data['tests'] = $model->getalltests();
		#$this->data['docTree'] = $model->getalltests();
		$this->render();
	
	}
	
    # Начало прохождения теста
    public function action_go() {
        
        $model = LoadModel('test');        
        $test_id = (int)$this->path[0];
        
        if ($test_id <= 0)
            $this->redirect('/');
        
        # Начинаем сессию и запоминаем инфу по тесту
        session_start();
                
        $_SESSION['test'] = $model->getTestById($test_id);    # Вся инфа по тесту
        $_SESSION['curNumber'] = 0;                           # Текущий номер вопроса
        $_SESSION['askList'] = array();                       # Список отвеченных вопросов
        $_SESSION['goodAns'] = 0;                             # Количество правильный ответов
            
        $this->view = 'test/go.tpl';
        $this->data['test'] = $model->getTestById($test_id);
        $this->data['title'] = 'Тест: ' . $this->data['test']['test_title'];
        $this->data['title_page'] = 'Тест: ' . $this->data['test']['test_title']; 
        
        $this->render();    
        
    }  
    
    # Начать тест / следующий вопрос
    public function action_next() {
        
        session_start();
        if (!isset($_SESSION['test']))
            $this->redirect('/');       
        
        # Завершаем тест
        if ($_SESSION['curNumber'] >= $_SESSION['test']['test_cask']) {            
            $this->catchAnswer($_POST); 
            $this->finishTest();
            
            //debug($_SESSION);
            //$this->redirect('/test/go/2');
            
            return;            
        }    
                
        $_SESSION['curNumber'] ++;
        
        # Проверяем предыдущий вопрос
        if ((isset($_POST['testask_type']) && ((isset($_POST['answers']) || isset($_POST['left']))))) 
        {
            //debug($_POST);
            $this->catchAnswer($_POST);    
        }
        
        //echo "Правильных ответов " . $_SESSION['goodAns'];
        
        # Выводим след вопрос
        $model = LoadModel('test');
        $this->view = 'test/next.tpl';
        $this->data['test'] = $_SESSION['test'];
        $this->data['title_page'] = 'Вопрос № ' .  $_SESSION['curNumber'] . ' | Тест: ' . $_SESSION['test']['test_title'];  

        $this->data['ask'] = $model->getNextAsk($_SESSION['askList'], $_SESSION['test']);
        $this->data['ask']['curNumber'] = $_SESSION['curNumber'];
        
        # Помечаем вопрос как пройденный
        $_SESSION['askList'][] = $this->data['ask']['testask_id']; 
        
        //debug($this->data['ask']);
        
        $this->render();     
        
    }
    
    # Проверяем текущий ответ
    private function catchAnswer($answer) {
        
        session_start();
        $model = LoadModel('test');
        if ($answer['testask_type'] == 'pair') {
            # Вопрос - соответствие
            $pairAr = array();
            foreach ($answer['left'] as $k => $v)
            {
                $pairAr[$k] = $answer['right'][$v-1];        
            }
            
            //echo "ПАРНЫЙ МАССИВ<br>";
            //debug($pairAr);
            
            if ($model->catchPair($pairAr)) 
            {
                # Защитываем ответ как верный
                $_SESSION['goodAns'] ++;
            }
        } 
        elseif ($answer['testask_type'] == 'simple') 
        {
            //debug($answer);
			# Обычный вопрос
            if (!isset($answer['answers'])) return;
            //echo "ОК до " . $_SESSION['goodAns'] . "<br>";
            if ($model->catchSimple($answer['answers'])) 
            {
                # Защитываем ответ как верный
                $_SESSION['goodAns'] ++;    
            }
			//echo "ОК после " . $_SESSION['goodAns'] . "<br>";
        }                
        
    }
    
    private function finishTest() {
        
        session_start();
        //debug($_SESSION);
        $model = LoadModel('test');
        $this->view = 'test/finish.tpl';
        $mark = ceil($_SESSION['goodAns'] * 10 / $_SESSION['test']['test_cask']);# оценка
            
        if ($_SESSION['goodAns'] >= $_SESSION['test']['test_ctrue'])
        {
            $this->data['title'] = 'Тест успешно пройден!';             
            $model->testFinished($_SESSION['test']['test_id'], 1, $mark);              
        }
        else
        {
            $this->data['title'] = 'Тест не пройден!';
            $model->testFinished($_SESSION['test']['test_id'], 0, $mark); 
        } 
        $this->data['title_page'] = $this->data['title'];
        
        # Очищаем данные сессии
        $_SESSION['test'] = false;    
        $_SESSION['curNumber'] = 0;                           
        $_SESSION['askList'] = array();                       
        $_SESSION['goodAns'] = 0;
         
        $this->render();  
        
    }
    #============================
    # АДМИНСКИЕ
    #============================
    
    
    #============================== 
    # ТЕСТЫ
    #============================== 
    
    # Получить список всех тестов
    public function action_admin() {
     
		$logged = $this->core->global['auth']->logged;
		
		if ($logged) 
        {
			$model = LoadModel('test');
			$this->data['title'] = 'Список тестов';
			$this->data['title_page'] = 'Список тестов';
        
			$this->view = 'test/admin.tpl';
			$this->data['testsList'] = $model->getalltests();
            
        } else {
            $this->data['title'] = 'Ошибка авторизации!';
            $this->view = 'notauthpage.tpl';
		}      
        
        $this->render();         
    }
	
    # Открыть тест на редактирование
    public function action_redactionTest() {
        
        $model = LoadModel('test');    
        $this->data['title'] = 'Редактирование теста';
        $this->data['title_page'] = 'Редактирование теста';
        
        $this->view = 'test/test_redaction.tpl';
        $this->data['test'] = $model->redactionTest((int)$this->path[0]);
        
        $this->render();        
    }
    
    # Добавление теста
    public function action_addTest() {
            
        $this->data['title'] = 'Создание нового теста';
        $this->data['title_page'] = 'Создание нового теста';
        
        $this->view = 'test/test_redaction.tpl';        
                
        $this->render();        
    }    
     
    # Сохранение отредактированного теста
    public function action_saveTest() {
        
        $model = LoadModel('test');
        $inputArr = array(
            'test_title' => $this->req['test_title'],
            'test_about' => $this->req['test_about'],
            'test_ctrue' => (int)$this->req['test_ctrue'],
            'test_cask' => $this->req['test_cask']             
        );
        
        return $model->saveTest($inputArr, (int)$this->req['test_id']);
    }
    
    # Удаление теста
    public function action_deleteTest() {
        
        $model = LoadModel('test');
        return $model->deleteTest((int)$this->req['test_id']);
        
    }
    
    # Сохранение нового теста в БД
    public function action_saveNewTest() {
        
        $model = LoadModel('test');
        $inputArr = array(
            'test_title' => $this->req['test_title'],
            'test_about' => $this->req['test_about'],
            'test_ctrue' => (int)$this->req['test_ctrue'],
            'test_cask' => $this->req['test_cask']             
        );
        
        return $model->addTest($inputArr);
    }                 
    
    
    #============================== 
    # ВОПРОСЫ
    #==============================
    
    # Открыть страницу добавления вопроса
    public function action_addAsk() {
        
        $model = LoadModel('test');
        
        $this->data['title'] = 'Добавление вопроса';
        $this->data['title_page'] = 'Редактирование теста';
        
        $this->view = 'test/ask_redaction.tpl';
        $ask = array (
            
        );
        $this->data['type'] = $this->path[0];
        $test = $model->getTestById((int)$this->path[1]);
        
        $this->data['test_id'] = (int)$this->path[1];
        $this->data['test_title'] = $test['test_title'];       
        
        $this->render();   
        
    }
     
    # Открыть вопрос на редактирование
    public function action_redactionAsk() {
            
        $model = LoadModel('test');
        $this->data['title'] = 'Редактирование вопроса';
        $this->data['title_page'] = 'Редактирование вопроса';
        
        $this->view = 'test/ask_redaction.tpl';
        $this->data['ask'] = $model->redactionAsk((int)$this->path[0]);
        
        $this->render();        
    }
    
    # Сохранить вопрос
    public function action_saveAsk() {
        
        $model = LoadModel('test');       
        
        $inputAr = array (
            'testask_type' => $this->req['testask_type'],
            'testask_test' => $this->req['testask_test'],
            'testask_text' => $this->req['testask_text']        
        );
        
        return $model->saveAsk($inputAr, $this->req['testask_id']);
        
    }
    
    # Новый вопрос
    public function action_newAsk() {
        
        $model = LoadModel('test');       
        
        $inputAr = array (
            'testask_type' => $this->req['testask_type'],
            'testask_test' => $this->req['testask_test'],
            'testask_text' => $this->req['testask_text']        
        );
        
        return $model->newAsk($inputAr);
        
    }
    
    # Удалить вопрос
    public function action_delAsk() {
        
        $model = LoadModel('test');
        return $model->delAsk((int)$this->req['testask_id']);
        
    }
    
    #==============================
    # ОТВЕТЫ
    #============================== 
    
    # Редактировать ответ
    public function action_redactionAns() {
        
        $model = LoadModel('test');
        $this->data['title'] = 'Редактирование ответа';
        $this->data['title_page'] = 'Редактирование ответа';
        
        $this->view = 'test/ans_redaction.tpl';        
        $this->data['ans'] = $model->redactionAns((int)$this->path[1], $this->path[0]);  // id, type        
        $this->data['type'] = $this->path[0];
        
        $this->render();
            
    }

    # Открыть страницу добавления ответа
    public function action_addAns() {
        
        $model = LoadModel('test');
        $this->data['title'] = 'Добавление ответа';
        $this->data['title_page'] = 'Добавление ответа';
        
        $this->view = 'test/ans_redaction.tpl';        
        $this->data['type'] = $this->path[0];
        $ans = array (
            'testans_ask' => (int)$this->path[1]
        );
        $this->data['ans'] = $ans;
        
        $this->render();          
        
    }
    
    # Простые ответы
    
    # Сохранить простой ответ
    public function action_saveSimpleAns() {
        
        $model = LoadModel('test');
        $inputAr = array (
            'testans_text' => $this->req['testans_text'],
            'testans_right' => $this->req['testans_right']
        );
        return $model->saveSimpleAns($this->req['testans_id'], $inputAr);
        
    }
    
    # Удалить простой ответ
    public function action_delSimpleAns() {
        
        $model = LoadModel('test');        
        return $model->delSimpleAns($this->req['testans_id']);
        
    }     
    
    # Создать новый ответ
    public function action_newSimpleAns() {
        
        $model = LoadModel('test');
        $inputAr = array (
            'testans_ask' => (int)$this->req['testans_ask'],
            'testans_text' => $this->req['testans_text'],
            'testans_right' => $this->req['testans_right']
        );
        return $model->newSimpleAns($inputAr);
        
    }
    
    # Ответы - пары
    
    # Сохранить ответ - пару
    public function action_savePairAns() {
        
        $model = LoadModel('test');
        $leftAr = array (
            'testans_text' => $this->req['left_testans_text']            
        );
        $rightAr = array (
            'testans_text' => $this->req['right_testans_text']            
        );
        
        return $model->savePairAns($this->req['left_testans_id'], $leftAr, $this->req['right_testans_id'], $rightAr);
        
    }
    
    # Удалить ответ - пару
    public function action_delPairAns() {
        
        $model = LoadModel('test');       
        return $model->delPairAns($this->req['left_testans_id'], $this->req['right_testans_id']);
        
    }
    
    # Создать новый ответ - пару
    public function action_newPairAns() {
        
        $model = LoadModel('test');
        $leftAr = array (
            'testans_text' => $this->req['left_testans_text'],
            'testans_ask' => $this->req['testans_ask']
                        
        );
        $rightAr = array (
            'testans_text' => $this->req['right_testans_text'],
            'testans_ask' => $this->req['testans_ask']            
        );
        
        return $model->newPairAns($leftAr, $rightAr);
        
    }
    
    
    

}   

                    

?>