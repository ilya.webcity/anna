{* Админская часть - редактирование ответа для теста *}
    <link type="text/css" href="/css/datapicker/jquery-ui-1.8.18.custom.css" rel="stylesheet" /> 
    <script type="text/javascript" src="/js/test/script.js"></script>  
    
    {* ВИЗУАЛЬНЫЙ РЕДАКТОР *}
    <script type="text/javascript" src="/js/tiny_mce/jquery.tinymce.js"></script>
    <script src="/js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php" type="text/javascript"></script>
            
{* /Админская часть - редактирование вопроса для теста *}
<div id = "errmessage"></div>

<p>
    <label>&nbsp;</label>
    
    {if $ans.testans_id}
        <input type="button" class="btn" value="Сохранить" onClick="{if $type == 'simple'}saveSimpleAns(){else}savePairAns(){/if};" />        
        <input type="button" class="btn" value="Удалить" onClick="{if $type == 'simple'}delSimpleAns(){else}delPairAns(){/if};" />
    {else}
        <input type="button" class="btn" value="Создать" onClick="{if $type == 'simple'}newSimpleAns(){else}newPairAns(){/if};" />          
    {/if}
    <input type="button" class="btn" value="Назад" onClick="javascript:history.back();"/>
</p>
<br/>
<hr>
<br/> 
<div class="wrapper">          
    <form id="form" class="blocks"> 
                 
        {if $type == 'simple'}                 
        {* Простой ответ *}
        <input type="hidden" id="testans_id" name="testans_id" value="{$ans.testans_id}"/>
        <input type="hidden" id="testans_ask" name="testans_ask" value="{$ans.testans_ask}"/> 
        <p> <label>Ответ</label>           
            <textarea name="testans_text" id="testans_text" style="height: 50px; width: 370px;">{$ans.testans_text}</textarea>
        </p>
        <p>
        <label>Этот ответ является</label>            
            <select name="testans_right" class="select" id="testans_right">                
                <option value="1" class="line" {if $ans.testans_right == 1}selected{/if}>Верным</option>
                <option value="0" class="line" {if $ans.testans_right == 0}selected{/if}>Неверным</option>                
            </select>
        </p>
        {else}
        <input type="hidden" id="testans_ask" name="testans_ask" value="{$ans.testans_ask}"/>
        <input type="hidden" id="left_testans_id" name="left_testans_id" value="{$ans.left.testans_id}"/>
        <input type="hidden" id="right_testans_id" name="right_testans_id" value="{$ans.right.testans_id}"/>
        <p>
            <label>Левая часть</label>
            <p>            
                <textarea name="left_testans_text" id="left_testans_text" style="height: 50px; width: 300px;">{$ans.left.testans_text}</textarea>
            </p>
        </p> 
        <p>
            <label>Правая часть</label>
            <p>            
                <textarea name="right_testans_text" id="right_testans_text" style="height: 50px; width: 300px;">{$ans.right.testans_text}</textarea>
            </p>
        </p>
        {* Ответ соответствие *} 
         
        {/if}        
    </div>            
          
        <br/>
        <div id = "errmessage2"></div>
        <hr>
        <p>
            <label>&nbsp;</label>     
            {if $ans.testans_id}
                <input type="button" class="btn" value="Сохранить" onClick="{if $type == 'simple'}saveSimpleAns(){else}savePairAns(){/if};" />        
                <input type="button" class="btn" value="Удалить" onClick="{if $type == 'simple'}delSimpleAns(){else}delPairAns(){/if};" />
            {else}
                <input type="button" class="btn" value="Создать" onClick="{if $type == 'simple'}newSimpleAns(){else}newPairAns(){/if};" />          
            {/if}
            <input type="button" class="btn" value="Назад" onClick="javascript:history.back();"/>
        </p>
    </form>     

<div class="clear"></div>
</div>
