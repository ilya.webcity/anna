<script>
$(document).ready(function(){

    $("#back-top").hide();
    
    $(function () {

        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#back-top').fadeIn();
            } else {
                $('#back-top').fadeOut();
            }
        });

        $('#back-top a').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });

});
</script>

<p><h3>        
        <style="vertical-align:middle; padding-right: 10px;"><a href ="/test/addtest">Добавить тест</a>              
    </h3>
</p>

<ol class="docTree">
{foreach $testsList as $q}
    <li class="docTree">
        <a href="/test/redactionTest/{$q.test_id}">{$q.test_title}</a>        
    </li>             
{/foreach}
</ol>

<p id="back-top">
        <a href="#top"><span></span>Наверх</a>
</p>

