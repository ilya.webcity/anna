{* Админская часть - редактирование теста *}
    <link type="text/css" href="/css/datapicker/jquery-ui-1.8.18.custom.css" rel="stylesheet" /> 
    <script type="text/javascript" src="/js/test/script.js"></script>    
    
    {* Плавающее меню *}
    <script type="text/javascript" src="/js/docmenufloater/jquery.hoverintent.minified.js"></script>
    <script type="text/javascript" src="/js/docmenufloater/jquery.floater.1.2.js"></script>
    
    {* ВИЗУАЛЬНЫЙ РЕДАКТОР *}
    <script type="text/javascript" src="/js/tiny_mce/jquery.tinymce.js"></script>
    <script src="/js/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php" type="text/javascript"></script>
            
{* /Админская часть - редактирование теста *}
<div id = "errmessage"></div>

<p>
    <label>&nbsp;</label>
    
    {if $test.test_id}
        <input type="button" class="btn" value="Сохранить" onClick="saveTest();" />        
        <input type="button" class="btn" value="Удалить" onClick="delTest();" />
    {else}
        <input type="button" class="btn" value="Создать" onClick="newTest();" />         
    {/if}
    <input type="button" class="btn" value="Назад" onClick="javascript:history.back();"/>
</p>
<br/>
<hr>
<br/> 
<div class="wrapper">          
    <form id="form" class="blocks"> 
        <input type="hidden" id="test_id" name="test_id" value="{$test.test_id}"/>
        <p>
            <label>Название теста *:</label>
            <input type="text" class="text" name="test_title" id="test_title" value="{$test.test_title}" style="width: 400px;"/>
        </p>
        <p>
            <label>Количество верных ответов:</label>
            <input type="text" class="text" name="test_ctrue" id="test_ctrue" value="{$test.test_ctrue}" style="width: 50px;"/>
        </p>
        <p>
            <label>Количество вопросов *:</label>
            <input type="text" class="text" name="test_cask" id="test_cask" value="{$test.test_cask}" style="width: 50px;"/>
        </p>
        <p>            
            <textarea name="test_about" id="test_about" style="height: 150px;">{$test.test_about}</textarea>
        </p>        
        
</div>          
        <br/>
        
		 {if $test.test_id}
        <p><img src = "/img/addtest_ico.png" style="vertical-align:middle; padding-right: 10px;"><a href="/test/addAsk/sipmle/{$test.test_id}" title="Добавить обычный вопрос">Добавить обычный вопрос</a> | <a href="/test/addAsk/pair/{$test.test_id}" title="Добавить вопрос-соответствие">Добавить вопрос - соответствие</a> </p>
        <p>База вопросов:</p>
        {* Список вопросов *}
        <div id = "ask_list">
            <ol class="docTree">
            {foreach $test.asksList as $a}
                 
                <li class="docTree">                    
                    <a href="/test/redactionAsk/{$a.testask_id}"><img src="/img/{if $a.testask_type == 'simple'}simple_ask_ico{else}pair_ask_ico{/if}.png" title="{if $a.testask_type == 'simple'}Обычный вопрос{else}Вопрос - соответствие{/if}" style="vertical-align:middle; padding-right: 10px;">{$a.testask_text|truncate:50:'..':true}</a>        
                </li>             
            {/foreach}
            </ol>       
        </div>
		{/if}
        
        <div id = "errmessage2"></div>
        <hr>
        <p>
            <label>&nbsp;</label>     
            {if $test.test_id}
                <input type="button" class="btn" value="Сохранить" onClick="saveTest();" />        
                <input type="button" class="btn" value="Удалить" onClick="delTest();" />
            {else}
                <input type="button" class="btn" value="Создать" onClick="newTest();" />                 
            {/if}
            <input type="button" class="btn" value="Назад" onClick="javascript:history.back();"/>
        </p>
    </form>    

     
        <div id="chaptermenu">
            <ul class="nav-main">
            {if $test.closetests}
                {* Дополнительное меню. Отображает другие тесты *}
                {foreach $test.closetests as $p}
                    <li>
                        <a href="/test/redactionTest/{$p.test_id}" title="{$p.test_about}">
                            <span>                                
                                {$p.test_title|truncate:20:'..':true}                                    
                            </span>
                        </a>
                    </li>
                {/foreach}
                    <li><hr></li>  
                {*/ Дополнительное меню *}
            {/if}
                    <li><a href="/test/admin"><span><b><u>К списку тестов</u></b></span></a></li>
            </ul>
            <div class="dc-corner"><span></span></div>
        </div>
   

<div class="clear"></div>
</div>
