<?php

class model_test extends model {

    public function get_user_info() {         
        
        return $this->core->global['auth']->user;   
        
    }
    
    #================
    # ?????
    #================ 
    
    public function getNextAsk($askList, $test) {		

        $ask = $this->db->pick('SELECT * FROM `test_ask` WHERE `testask_test` = ' . $test['test_id'] . ($askList ? ' AND `testask_id` NOT IN (' . implode(', ', $askList) . ') ' : '') .  ' ORDER BY RAND() LIMIT 1');                 
        
        if ($ask['testask_type'] == 'simple')
        {
            # ??????? ??????
            $ask['answers'] = $this->db->query('SELECT * FROM `test_answer` WHERE `testans_ask` = '. $ask['testask_id'] . ' ORDER BY RAND()');
        }
        else
        {
            # ?????? - ????
            $ask['answersLeft'] = $this->db->query('SELECT * FROM `test_answer` WHERE `testans_ask` = '. $ask['testask_id'] . ' AND `testans_left` = 1 ORDER BY RAND()');
            $ask['answersRight'] = $this->db->query('SELECT * FROM `test_answer` WHERE `testans_ask` = '. $ask['testask_id'] . ' AND `testans_left` = 0 ORDER BY RAND()');            
        }
        
        return $ask;
                
    }    
    
    public function catchSimple($answersAr) {
        
        foreach ($answersAr as $v)
        {
            if ($this->db->pick('SELECT `testans_right` FROM `test_answer` WHERE `testans_id` = '. (int)$v, 'testans_right') == 0) return false;
        }
               
        return true;   
        
    }
    
    public function catchPair($pairsAr) {
        
        foreach ($pairsAr as $k => $v) {
            
            //echo "ПАРКА " . $this->db->pick('SELECT `testans_id` FROM `test_answer` WHERE `testans_id` = '. (int)$k . ' AND `testans_pair` = ' . (int)$v, 'testans_id') . "<br>";       
         
            if (!$this->db->pick('SELECT `testans_id` FROM `test_answer` WHERE `testans_id` = '. (int)$k . ' AND `testans_pair` = ' . (int)$v, 'testans_id')) return false;       
            
        }
        
        return true;
        
    }
    
    public function testFinished($test_id, $isDone, $mark) {
        session_start();
        $inputData = array(
                        'study_test' => $test_id, 
                        'study_done' => $isDone,
                        'study_date' => date('Y-m-d'),
                        'study_user' => $_SESSION['user']['user_id'],
                        'study_mark' => $mark                                    
        );
        
        if (!$this->db->pick('SELECT `study_id` FROM `study` WHERE `study_test` = ' . (int)$test_id . ' AND `study_user` = ' . (int)$_SESSION['user']['user_id'], 'study_id'))
        {            
            $this->db->insert('study', $inputData);
        }
        else
        {
            $this->db->update('study', $inputData, 'study_test', $test_id);
        }
        
    }
    
    # ???????? ?????? ??????    
    public function getalltests() {
        
        return $this->db->query('SELECT `test_id`, `test_title`, `test_about` FROM `test`');
        
    }
    
    # ?????????????? ?????
    public function redactionTest($test_id) {
        
        $res = $this->db->query('SELECT * FROM `test` WHERE `test_id` = ' . (int)$test_id . ' LIMIT 1');
        $res = $res[0];
        
        $res['closetests'] = $this->db->query('SELECT `test_id`, `test_title`, `test_about` FROM `test` WHERE `test_id` != ' . (int)$test_id);        
        $res['asksList'] = $this->db->query('SELECT `testask_id`, `testask_text`, `testask_type` FROM `test_ask` WHERE `testask_test` = ' . (int)$test_id);
        return $res;
    }     

    # ?????????? ?????????????????? ?????
    public function saveTest($inputAr, $test_id) {
        
        return $this->db->Update('test', $inputAr, 'test_id', $test_id);
        
    }
    
    # ?????????? ?????? ????? ? ??
    public function addTest($inputAr) {
        
        return $this->db->Insert('test', $inputAr);
        
    }    
    
    # ??????? ????
    public function deleteTest($test_id) {
        
        $q = $this->db->query('SELECT `testask_id` FROM `testask` WHERE `testask_test` = '. (int)$test_id);
        foreach ($q as $v) {            
            # ??????? ?????? ???????
            $this->db->query('DELETE FROM `test_ans` WHERE `testans_ask` = '. (int)$v['testask_id']);
            
            # ??????? ??? ??????
            $this->db->query('DELETE FROM `test_ask` WHERE `testask_id` = '. (int)$v['testask_id']);                        
        }
        
        # ??????? ????
        return $this->db->query('DELETE FROM `test` WHERE `test_id` = '. $test_id);        
        
    }
    
    # ???????? ??? ???? ? ????? ?? ID
    public function getTestById($test_id) {
        
        return $this->db->Pick('SELECT * FROM `test` WHERE `test_id` = '.(int)$test_id);
        
    }
    
    #================
    # ???????
    #================
    
    # ?????????????? ?????
    public function redactionAsk($testask_id) {
        
        $res = $this->db->query('SELECT * FROM `test_ask` WHERE `testask_id` = ' . (int)$testask_id . ' LIMIT 1');
        $res = $res[0];
        
        $res['closeasks'] = $this->db->query('SELECT `testask_id`, `testask_text`, `testask_type` FROM `test_ask` WHERE `testask_id` != ' . (int)$testask_id . ' AND `testask_test` = ' . (int)$res['testask_test']);        
        $res['testsList'] = $this->db->query('SELECT `test_id`, `test_title` FROM `test`');
        
        if ($res['testask_type'] == 'simple') {
            $res['answers'] = $this->db->query('SELECT `testans_id`, `testans_text`, `testans_right` FROM `test_answer` WHERE `testans_ask` = ' . (int)$testask_id); 
        }
        else
        {
            $left_pairs = $this->db->query('SELECT `testans_id`, `testans_text` FROM `test_answer` WHERE `testans_left` = 1 AND `testans_ask` = '.(int)$testask_id);
            foreach ($left_pairs as $k => &$v) 
            {
                $m = $this->db->Pick('SELECT `testans_text` FROM `test_answer` WHERE `testans_pair` = '.(int)$v['testans_id'].' LIMIT 1');
                $v['testans_text'] = $v['testans_text'] . ' -> ' . $m['testans_text'];
            }             
            
            $res['answers'] = $left_pairs;
        }
        
        //debug($res);
        
        return $res;
    }
    
    # ????????? ??????
    public function saveAsk($inputAr, $testask_id) {
        
        return $this->db->Update('test_ask', $inputAr, 'testask_id', $testask_id); 
        
    }
    
    # ???????? ????? ???????
    public function newAsk($inputAr) {
        
        return $this->db->Insert('test_ask', $inputAr);
        
    }
    
    # ??????? ??????
    public function delAsk($testask_id) {
        
        // ??????? ??? ??????
        $this->db->query('DELETE FROM `test_answer` WHERE `testans_ask` = '. (int)$testask_id);
        
        return $this->db->query('DELETE FROM `test_ask` WHERE `testask_id` = '. (int)$testask_id);
        
        
    }
    
    #================
    # ??????
    #================
    
    # ?????????????? ??????
    public function redactionAns($testans_id, $type) {
        
        if ($type == 'simple') {
            $res = $this->db->query('SELECT * FROM `test_answer` WHERE `testans_id` = ' . (int)$testans_id . ' LIMIT 1'); 
            $res = $res[0];          
        }
        else  {
            $left_ans = $this->db->query('SELECT * FROM `test_answer` WHERE `testans_id` = ' . (int)$testans_id . ' LIMIT 1');
            $right_ans = $this->db->query('SELECT * FROM `test_answer` WHERE `testans_pair` = ' . $left_ans[0]['testans_id'] . ' LIMIT 1');
            
            $res = array(
                'left' => $left_ans[0],
                'right' => $right_ans[0],
                'testans_id' => $left_ans[0]['testans_id']           
            ); 
            
            
        }
        debug($res);
        return $res;  
    }     
        
    # ??????? ??????
    
    # ????????? ??????? ?????
    public function saveSimpleAns($testans_id, $inputAr) {
    
       return $this->db->Update('test_answer', $inputAr, 'testans_id', $testans_id);       
        
    }
    
    # ??????? ??????? ?????
    public function delSimpleAns($testans_id) {
        
        return $this->db->query('DELETE FROM `test_answer` WHERE `testans_id` = '. (int)$testans_id);
        
    }
    
    # ???????? ????? ??????? ?????
    public function newSimpleAns($inputAr) {
        
        return $this->db->Insert('test_answer', $inputAr);
        
    }
    
    # ?????? - ????
    
    # ?????????
    public function savePairAns($left_id, $leftAr, $right_id, $rightAr) {
        
        $this->db->Update('test_answer', $leftAr, 'testans_id', $left_id);
        return $this->db->Update('test_answer', $rightAr, 'testans_id', $right_id);                  
        
    } 
    
    # ???????
    public function delPairAns($left_id, $right_id) {
        
        $this->db->query('DELETE FROM `test_answer` WHERE `testans_id` = '. (int)$left_id);
        return $this->db->query('DELETE FROM `test_answer` WHERE `testans_id` = '. (int)$right_id);             
    }
    
    # ???????? ?????
    public function newPairAns($leftAr, $rightAr) {       
        
        $leftAr['testans_left'] = 1;        
        $this->db->Insert('test_answer', $leftAr);
        
        // ???????? ID
        $last_id = $this->db->query('SELECT MAX(testans_id) As `last_id` FROM `test_answer`');
        $left_last_id = $last_id[0]['last_id'];
        $rightAr['testans_pair'] = $left_last_id;            
        
        $ar = array(
            'testans_pair' => $left_last_id + 1
        );
        
        // ????????? ????? ?????
        $this->db->Update('test_answer', $ar, 'testans_id', $left_last_id);
        
               
        return $this->db->Insert('test_answer', $rightAr);
        
    }
}

?>