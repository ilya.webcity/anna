<p><b>Описание</b>: {$test.test_about}<br/></p>
<p><b>Количество вопросов</b>: {$test.test_cask}<br/></p>
<p><b>Правильных ответов для прохождения теста</b>: {$test.test_ctrue}<br/></p>
<hr> 
<p><b>Вопрос №</b>: {$ask.curNumber}<br/></p>
<p>{$ask.testask_text}</p>
<form name="userAnswer" method="post" action="/test/next">
    <input type="hidden" name="testask_type" value="{$ask.testask_type}">
    {if $ask.testask_type == 'simple'}             
        {foreach $ask.answers as $a}
            <p><input type="checkbox" name="answers[]" value="{$a.testans_id}">{$a.testans_text}</p>
        {/foreach}    
    {else}
        <table id="table_mess">
        <tr>
            <th>Понятие</th>
            <th>Соответствие</th>
        </tr>
        {foreach $ask.answersLeft as $k => $v}
            <tr {if $k % 2 == 0} class="alt"{/if}>
                <td><p>{$k + 1}. {$ask.answersLeft[$k].testans_text}<input type="text" name="left[{$ask.answersLeft[$k].testans_id}]" style="float: right; width: 25px;" value=""></p></td>
                <td><p>{$k + 1}. {$ask.answersRight[$k].testans_text} <input type="hidden" name="right[]" value="{$ask.answersRight[$k].testans_id}"></p></td> 
            </tr>
        {/foreach}
        </table>        
    {/if}
    <p><input type="submit" value="Следующий вопрос >>"></p>    
</form>

