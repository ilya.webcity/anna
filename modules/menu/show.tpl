{if $logged}
{if $err}<div class="errmessage">{$err}</div>{/if} 
{if $sendok}<div class="okmessage">{$sendok}</div>{/if}
<script type="text/javascript" src="/js/menu_list.js"></script>

<div id='items_div'>
<table class='item_table'>
		<tr>
			<th>№</th>
			<th>Название</th>
			<th>Доступ</th>
			<th>Действия</th>
		</tr>
{foreach $menu_item as $item}
		<tr id_itm='{$item.menu_id}'>
			<td>{$item.menu_id}</td>
			<td><a href='{$item.menu_url}'>{$item.menu_title}</a></td>
			<td>{$item.menu_type}</td>
			<td><span class='del' >Удалить</span></td>
		</tr>
{/foreach}
</table>
</div>

<table class='item_add' align=center>
	<tr>
		<th align=right>Параметр</th>
		<th align=left>Значение</th>
	</tr>
	<tr id='tr_id'>
		<td align=right>Id</td>
		<td align=left ></td>
	</tr>
	<tr >
		<td align=right>Название *</td>
		<td align=left><input name='menu_title' type='text' size=10></td>
	</tr>
	<tr >
		<td align=right>URL *</td>
		<td align=left><input name='menu_url' type='text' size=10></td>
	</tr>
	<tr >
		<td align=right>Родитель *</td>
		<td align=left>
			<select name='menu_pid'>
				<option value=0>Нету</option>
				{foreach $menu_item as $item}
				<option value={$item.menu_id}>{$item.menu_title}</option>
				{/foreach}
			</select>
		</td>
	</tr>
	<tr >
		<td align=right>Номер *</td>
		<td align=left><input name='menu_number' type='text' size=10></td>
	</tr>
	<tr >
		<td align=right>Доступен </td>
		<td align=left>
			<select name='menu_type' size=2 multiple>
				<option value=guest>Гость</option>
				<option value=student>Студент</option>
				<option value=admin>Админ</option>
			</select>
		</td>
	</tr>
	<tr >
		<td align=right>Цвет </td>
		<td align=left id='color_td'>
			<img src='/img/blue.png' id='blue' width=20px height=20px class='active'>
			<img src='/img/green.png' id='green' width=20px height=20px>
			<img src='/img/orange.png' id='orange' width=20px height=20px>
			<img src='/img/red.png' id='red' width=20px height=20px>
			<br>
			{*<select name='color'>
				<option value=blue>Синий</option>
				<option value=green>Зеленый</option>
				<option value=orange>Оранжевый</option>
				<option value=red>Красный</option>
			</select>*}
		</td>
	</tr>
	<tr id='tr_usr_subit'>
		<td align=right><input name='user_add' value='Новый' type='submit' size=3></td>
		<td align=left><input name='user_edit' value='Сохранить' type='submit' size=3><input name='user_del' value='Удалить' type='submit' size=3></td>
	</tr>
	
</table>

<div class="clear"></div>
</div>


{else}
    <p class="NotAuthorised">Вы не авторизованы!!!</p>
{/if}
