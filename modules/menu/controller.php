﻿<?php

class controller_menu extends controller {

	public function action_list() {

		$model = LoadModel('menu');
        
        $this->view = 'menu/show.tpl';
		
		$logged = $this->core->global['auth']->logged;
        
		$this->data['title'] = "Меню";
		
        if ($logged) 
        {
            $this->data['err'] = $this->req['err'];
            $this->data['sendok'] = $this->req['sendok'];
            
            $this->data['menu_item'] = $model->get_menu_list_id('0');
            //$this->data['teachers']	    = $model->get_teachers_list(); 
            //$this->data['user']         = $this->core->global['auth']->user;
        }

		$this->render();   

	}
	
	public function action_delete() {

		$model = LoadModel('menu');
		$del = $model->del_menu($_POST['id']);
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		print $_POST['id'];
		die();
	}
	
	public function action_get() {

		$model = LoadModel('menu');
		//$_GET['id'] = 5;
		$usr = $model->get_menu($_GET['id']);
		$usr = $usr[0];
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		//print_r($usr);
		//print_r($usr['menu_type']);
		$usr['menu_type'] = explode(',', $usr['menu_type']);
		//print_r($usr['menu_type']);
		echo json_encode($usr);
		die();
		print "{\r\n";
		foreach ($usr as $key => $value)
			if ($key == 'menu_type')
				print "	\"".$key."\": \"".$value."\",\r\n";
			else
				print "	\"".$key."\": \"".$value."\",\r\n";
		
		print "	\"error\": \"0\"\r\n";
/*		print "	\"user_type\": \"".$usr['user_type']."\",\r\n";
		print "	\"user_name\": \"".$usr['user_name']."\",\r\n";
		print "	\"user_fname\": \"".$usr['user_fname']."\",\r\n";
		print "	\"user_lname\": \"".$usr['user_lname']."\",\r\n";
		print "	\"user_group\": \"".$usr['user_group']."\",\r\n";
		print "	\"user_number\": \"".$usr['user_number']."\",\r\n";
		print "	\"user_dregistration\": \"".$usr['user_dregistration']."\",\r\n";
		print "	\"user_ldate\": \"".$usr['user_ldate']."\"\r\n";*/
		print "}\r\n";
		die();
	}
	
	public function action_add() {

		$model = LoadModel('menu');
		print $_GET['menu_id'].'--<br>';
		$iferror = false;
		$item=array();
		print '{';
		print '"error": {';
		$item['menu_pid'] = $_GET['menu_pid'];
		$item['menu_number'] = $_GET['menu_number'];
		$item['color'] = $_GET['color'];
		$item['menu_title'] = $_GET['menu_title'];
		$item['menu_url'] = $_GET['menu_url'];
		$item['menu_type'] = '';
		foreach($_GET['menu_type'] as $type)
			if ($item['menu_type'] == '')
				$item['menu_type'] .=$type;
			else
				$item['menu_type'] .=','.$type;
		
		//print_r( $item['menu_type']);
		if (empty($_GET['menu_id']))
		{
			/*if ($model->valid_num($_GET['group_code']) > 0)
			{
				print '"num": "1"';
				$iferror = true;
			}
			else
				print '"num": "0"';
			*/	
		}
		else
		{
			$item['menu_id'] = $_GET['menu_id'];
		}
		
		print '}';
		print '}';
		//print_r($item);
		
		if (!$iferror)
		{
			if (empty($_GET['menu_id']))
			{
				$model->set_menu($item);
				print 'ok';
			}
			else
			{
				$model->edit_menu($item);
			}
		}
		//if (empty($_POST['id']))
			//print 'add';
		//$del = $model->del_user($_POST['id']);
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		//print $_POST['id'];
		die();
	}
	
	public function action_getlist() {

		$model = LoadModel('menu');
		//$_POST['id'] = 2;
		$items = $model->get_menu_list_id('0');
		//$usr = $usr[0];
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		//print_r($items);
		print "<table class='item_table'>
		<tr>
			<th>Название</th>
			<th>Доступ</th>
			<th>Действия</th>
		</tr>";
		foreach ($items as $item)
		{
			print "			
		<tr id_itm='".$item['menu_id']."'>
			<td>".$item['menu_number'].". <a href='".$item['menu_url']."'>".$item['menu_title']."</a></td>
			<td>".$item['menu_type']."</td>
			<td><span class='del' >Удалить</span></td>
		</tr>";
			foreach ($model->get_menu_list_id($item['menu_id']) as $subitem)
			{
				print "			
			<tr id_itm='".$subitem['menu_id']."'>
				<td>&nbsp;&nbsp;&nbsp;".$item['menu_number'].".".$subitem['menu_number'].". <a href='".$subitem['menu_url']."'>".$subitem['menu_title']."</a></td>
				<td>".$subitem['menu_type']."</td>
				<td><span class='del' >Удалить</span></td>
			</tr>";
			
			}
		}
		print "</table>";
		die();
	}
	public function action_getselect() {

		$model = LoadModel('menu');
		//$_POST['id'] = 2;
		$items = $model->get_menu_list_id('0');
		//$usr = $usr[0];
		/*if (!empty($del))
			print 'OK ';
		else
			print "NO";*/
		//print_r($usr);
		
		print "	<select name='menu_pid'>
				<option value=0>Нету</option>";
			
		foreach ($items as $item)
		{
			print "	
				<option value=".$item['menu_id'].">".$item['menu_title']."</option>";
		}
		print "</select>";
		die();
	}




}


?>