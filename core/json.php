<?php

function ke_json_encode($obj) {
	if(is_string($obj)) return ke_json_str($obj) ;
	if(count($obj) == 0) return '[]';
	$is_obj = isset($obj[count($obj) - 1]) ? false : true;
	$str = $is_obj ? '{' : '[';
	foreach($obj as $key  => $value) {
	   $str .= $is_obj ? ke_json_str($key) . ':' : '';
	   if(is_array($value)) $str .= ke_json_encode($value);
	   elseif(is_string($value)) $str .= ke_json_str($value);
	   elseif(is_null($value)) $str .= 'null';
	   elseif(is_bool($value)) $str .= $value ? 'true' : 'false';
	   elseif(is_numeric($value)) $str .= $value;
	   else $str .= ke_json_str($value);
	   $str .= ',';
	}
	return  substr_replace($str, $is_obj ? '}' : ']', -1);
}

function ke_json_str($str) {
	return '"' . addcslashes($str, "\n\r\t\"\\/") . '"';
}

function ke_json_out($obj) {
	header('Content-Type: application/json');
	echo ke_json_encode($obj);
}

if(!function_exists('json_encode'))	{
	function json_encode($obj) { return ke_json_encode($obj); }
}

?>