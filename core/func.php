<?php

function LoadModel($name) {
    $file = MODULES_DIR . "/{$name}/model.php";
    if(!file_exists($file)) Error_404(); //die("Unknown model `{$name}` ({$file})");
    $classname = "model_{$name}";
    require_once $file;
    return new $classname();
}

function LoadController($name) {
    $file = MODULES_DIR . "/{$name}/controller.php";
    if(!file_exists($file)) Error_404(); //die("Unknown controller `{$name}` ({$file})");
    $classname = "controller_{$name}";
    require_once $file;
    return new $classname();
}

function go($url) {
	header('Location: ' . $url);
	exit;
}

function debug($ar) {
    echo "<pre>";
    print_r($ar);
    echo "</pre>";
}

function Error_404() {

	readfile('404.html');
    //Header('Location: /404.html');
	//print '404!';
    exit;
}

function json($data) {
    header('Content-Type: application/json');
    echo json_encode($data);
    exit;
}

function GetFormatedDate($DateTime, $DateSeparate = ' ', $TimeSeparate = ':', $mode = 'date') {
    $date_time_array = explode(' ', $DateTime);
    $date = explode('-', $date_time_array[0]);
    $time = explode(':', $date_time_array[1]);

    $day = $date[2];
    $month = $date[1];
    //-- Обрабатываем месяц
    switch($month)
    {
        case 1:
            $month = 'янв.';
            break;
        case 2:
            $month = 'февр.';
            break;
        case 3:
            $month = 'март';
            break;
        case 4:
            $month = 'апр.';
            break;
        case 5:
            $month = 'май';
            break;
        case 6:
            $month = 'июнь.';
            break;
        case 7:
            $month = 'июль';
            break;
        case 8:
            $month = 'авг.';
            break;
        case 9:
            $month = 'сент.';
            break;
        case 10:
            $month = 'окт.';
            break;
        case 11:
            $month = 'нояб.';
            break;
        case 12:
            $month = 'дек.';
            break;
        default: $month = 'неопределено';
    }

    $year = $date[0];

    $hours = $time[0];
    $minutes = $time[1];
    $seconds = $time[2];

    if ($mode == 'date')
        return $day . $DateSeparate . $month . $DateSeparate . $year;
    elseif ($mode == 'datetime')
        return $day . $DateSeparate . $month . $DateSeparate . $year . " " . $hours . $TimeSeparate . $minutes . $TimeSeparate . $seconds;
    elseif ($mode == 'time')
        return $hours . $TimeSeparate . $minutes . $TimeSeparate . $seconds;
}

?>