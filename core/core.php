<?php

class Core {

	var $req = array();
	var $path = array();
	var $mod = '';
	var $act = '';
	var $global = array();

	// -- Наш шаблонизатор
	var $sm = NULL;

	// -- Нада База Данных
	var $db = NULL;

	public function Run() {

		// -- Подключаем Смарти и конфигурируем его
		require_once CORE_DIR . '/sm3/Smarty.class.php';
		$this->sm = new Smarty;
		$this->sm->compile_dir = MAIN_DIR . '/cache';
		$this->sm->template_dir = MAIN_DIR . '/modules';
		$this->sm->assignbyref('req', $this->req);
		$this->sm->assignbyref('path', $this->path);

		// -- Подключаем БД
		require_once CORE_DIR . '/db.php';
		$this->db = new DB(DB_HOST, DB_USER, DB_PASS, DB_BASE);

		// -- Парсим запрос
		
	// -- Костыль под php-fpm
			list($null, $r) = explode('?', $_SERVER['REQUEST_URI'] , 2);
			parse_str($r, $_GET) ;
			$this->req = array_merge((array)$_GET, (array)$_POST);

		$uri = preg_replace('/^\/index\.php/i', NULL, getenv('REQUEST_URI'));
		$uri = preg_replace('/\?.+$/', NULL, $uri);
		$this->path = explode('/', $uri); unset($this->path[0]);

		$this->mod = array_shift($this->path);
		$this->act = array_shift($this->path);

		if(!$this->mod) $this->mod = DEFAULT_MODULE;
		if(!$this->act) $this->act = DEFAULT_ACTION;

		// -- Загружаем контроллер
		$ctrl = LoadController($this->mod);

		require MODULES_DIR . '/bootstrap.php';
		bootstrap($this, $ctrl);

		// -- Вызываем действие
		if(method_exists($ctrl, "action_{$this->act}")) {
			call_user_func(array($ctrl, "action_{$this->act}"));
		} else {
			Error_404();
		}

	}

}

abstract class model {

	protected $core = NULL;
	protected $db = NULL;

	public function __construct() {

		global $core;
		$this->core = & $core;
		$this->db = & $core->db;
	}

}

abstract class controller {

	// -- Наш каркасный шаблон
	public $template = 'main_template.tpl';

	// -- Суб-шаблон
	public $view = 'main/main.tpl';

	// -- Набор данных для передачи их в шаблон
	public $data = array();

	// -- Наше ядро + пару ссылок для удобства
	protected $core = NULL;
	protected $req = array();
	protected $path = array();
	protected $sm = NULL;
	protected $db = NULL;

	// -- Конструктор контроллера
	public function __construct() {
		global $core;
		$this->core = & $core;
		$this->req = & $core->req;
		$this->path = & $core->path;
		$this->sm = & $core->sm;
		$this->db = & $core->db;

	}

	public function redirect($url = '/') {
		header('Location: ' . $url);
		exit;
	}

	public function render() {

		// -- Передаем в Смарти данные из контроллера
		foreach($this->data as $key => $value) $this->sm->assign($key, $value);

		// -- Формируем контентную часть
		$content = $this->sm->fetch($this->view);

		// -- Передаем в смарти контентную часть
		$this->sm->assign('content', $content);

		// -- Формируем и выводим окончательный HTML
		$this->sm->display($this->template);

	}

}

?>