<?php

class DB {

	private $link = null;

	public function __construct($host, $user, $pass, $base) {
		$this->link = @mysql_connect($host, $user, $pass, true);
		$this->SelectDB($base);
		$this->Query('SET names "utf8", character_set_results="utf8", character_set_client="utf8", collation_connection="utf8_general_ci"');
	}

	public function __destruct() {
		@mysql_close($this->link);
	}

	public function SelectDB($base) {
        return mysql_select_db($base, $this->link);
	}

	public function Query($sql) {
		$return = array();
		$result = @mysql_query($sql, $this->link);
		if($result) {
			while($data = @mysql_fetch_array($result, MYSQL_ASSOC)) $return[] = $data;
			@mysql_free_result($result);
			return $return;
		} else {
			return false;
			//Error(MySQL_Error($this->link), MySQL_ErrNo($this->link));
		}
	}
    
    public function ConsoleQuery($sql) {
        
        $return_table = array();
        $result = @mysql_query($sql, $this->link);
        if (!$result) {
			// Ошибка
			$return = array (
				'error' => 1,
				'text' => 'Ошибка: ' . mysql_errno () .' '. mysql_error()
			);
           return $return;
		}
        
        while($data = @mysql_fetch_array($result, MYSQL_ASSOC)) $return_table[] = $data;
            @mysql_free_result($result);
        
		if (sizeOf($return_table) > 0) {		
			$return = array (
				'error' => 0,
			);
			foreach ($return_table as $key => $val)
			{
				$return['table'][] = $val;				
			}
		}
		else {
			$return = array (
				'error' => 0,
				'text' => 'Запрос вернул пустой ответ!'				
			);
		}
		
        return $return;        
           
    }

	public function Pick($sql, $field = '') {
		$r = $this->Query($sql);
		if($field) return $r[0][$field];
		if(isSet($r[0])) return $r[0];
		return false;
	}

	public function PickBy($table, $field, $value) {
		$res = $this->pick('SELECT * FROM `' . $table . '` WHERE `' . $field . '` = "' . mysql_escape_string($value) . '"');
		return $res;
	}

	public function GetList($sql, $v) {
		$r = $this->Query($sql);
		$result = array();
		foreach($r as $item) $result[] = $item[$v];
		return $result;
	}

	public function GetKeyList($sql, $k, $v) {
		$r = $this->Query($sql);
		$result = array();
		foreach($r as $item) $result[$item[$k]] = $item[$v];
		return $result;
	}

	public function Insert($table, $data, $return = '') {
		$keys = array_keys($data);
		$values = array_values($data);
		foreach($values as $k => $v) $values[$k] = mysql_escape_string($v);
		$sql = 'INSERT INTO `' . $table . '` (`' . implode('`, `', $keys) . '`) VALUES ("' . implode('", "', $values) . '")';
		$r = $this->Query($sql);
		return $this->InsertId();
	}

	public function InsertId() {
		return @mysql_insert_id($this->link);
	}

	public function AffectedRows() {
		return @mysql_affected_rows($this->link);
	}

	public function Update($table, $data, $where_key = '', $where_value = '') {
		$keys = array_keys($data);
		$values = array_values($data);
		$sql = 'UPDATE `' . $table . '` SET ';
		$cnt = count($data);
		for($i = 0; $i < $cnt; $i++) {
			$sql .= '`' . $keys[$i] . '` = "' . mysql_escape_string($values[$i]) . '"';
			if($i < $cnt - 1) $sql .= ', ';
		}
		if($where_key) $sql .= ' WHERE `' . $where_key . '` = "' . mysql_escape_string($where_value)  . '" ';
		$r = $this->Query($sql);
		return $this->AffectedRows();
	}

}

?>